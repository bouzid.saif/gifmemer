/**
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

/// Delegate method for the morse keyboard view that will allow it to perform
/// actions on whatever text entry you want to use it with. It does not assume
/// any type e.g. UITextField vs UITextView.
protocol MorseKeyboardViewDelegate: class {
  func insertCharacter(_ newCharacter: String)
  func deleteCharacterBeforeCursor()
  func characterBeforeCursor() -> String?
}

/// Contains all of the logic for handling button taps and translating that into
/// specific actions on the text entry associated with it
class MorseKeyboardView: UIView {
  @IBOutlet var nextKeyboardButton: UIButton!
  @IBOutlet var deleteButton: UIButton!
  @IBOutlet var spaceButtonToParentConstraint: NSLayoutConstraint!
  @IBOutlet var spaceButtonToNextKeyboardConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nofavoritesView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var gifsBTN: UIButton!
    @IBOutlet weak var memesBTN: UIButton!
    @IBOutlet weak var selectorContainerCustom : UIView!
    var isGif : Bool = true
    var isMemeLoaded : Bool = false
    @IBOutlet weak var labelNoGifs : UILabel!
    
       @IBOutlet weak var MemesContainer: UIView!
       
       @IBOutlet weak var GifsContainer: UIView!
       
       var selectorContainer: UIView!
    @IBOutlet weak var fullAccessGif : UIView!
       @IBOutlet weak var gifsLabel: UILabel!
       var oneTimeWork = true
    var frameCustom : CGRect = CGRect.zero
       @IBOutlet weak var memesLabel: UILabel!
    
    @IBOutlet weak var keyboardView: UIView!
    
    var keys: [UIButton] = []
       var paddingViews: [UIButton] = []
       var backspaceTimer: Timer?
       enum KeyboardState{
           case letters
           case numbers
           case symbols
       }
       
       enum ShiftButtonState {
           case normal
           case shift
           case caps
       }
       var keyboardState: KeyboardState = .letters
       var shiftButtonState:ShiftButtonState = .normal
    @IBOutlet weak var stackView1: UIStackView!
    @IBOutlet weak var stackView2: UIStackView!
    @IBOutlet weak var stackView3: UIStackView!
    @IBOutlet weak var stackView4: UIStackView!
    func initSelector(){
        
        memesLabel.font = UIFont.init(name: "SegoeUI-Regular", size: 18)
        memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)
        gifsLabel.font = UIFont.init(name: "SegoeUI-Bold", size: 18)
        gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
        selectorContainer = UIView(frame: self.GifsContainer.frame)
        selectorContainer.backgroundColor = .white
        self.selectorContainerCustom.addSubview(selectorContainer)
        //self.bringSubviewToFront(self.selectorContainerCustom)
        self.selectorContainerCustom.bringSubviewToFront(self.GifsContainer)
        self.selectorContainerCustom.bringSubviewToFront(self.MemesContainer)
        self.selectorContainerCustom.bringSubviewToFront(self.gifsLabel)
        self.selectorContainerCustom.bringSubviewToFront(self.memesLabel)
        self.selectorContainerCustom.bringSubviewToFront(self.gifsBTN)
        self.selectorContainerCustom.bringSubviewToFront(self.memesBTN)
        //selectorContainer.layoutIfNeeded()
        let tapgestureGifs = UITapGestureRecognizer(target: self, action: #selector(self.goToGifs))
        let tapgestureMimes = UITapGestureRecognizer(target: self, action: #selector(self.goToMimes))
        MemesContainer.isUserInteractionEnabled = true
        MemesContainer.addGestureRecognizer(tapgestureMimes)
        GifsContainer.isUserInteractionEnabled = true
        GifsContainer.addGestureRecognizer(tapgestureGifs)
        frameCustom = self.GifsContainer.frame
    }
    @IBAction func goGifsAction(_ sender: UIButton) {
        if isGif == false  {
            self.goToGifs()
        }
    }
    @IBAction func goMemesAction(_ sender: UIButton) {
        if isGif && isMemeLoaded {
            self.goToMimes()
        }
    }
    @objc func goToMimes(){
        isGif = false
    

        UIView.performWithoutAnimation {
        self.gifsLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
        self.memesLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
        }
       
        UIView.animate(withDuration: 0.2, animations: {
            
            self.gifsLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

            self.memesLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
          self.selectorContainer.transform = CGAffineTransform(translationX: (self.MemesContainer.frame.origin.x), y: 0)
            
        
         }, completion: nil)
        self.collectionView.reloadData()
        
    }
    @objc func goToGifs(){
      isGif = true
       
        UIView.performWithoutAnimation {
            self.memesLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
            self.gifsLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
        }
        UIView.animate(withDuration: 0.2, animations: {
           
                self.memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

                self.gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
                self.selectorContainer.transform = CGAffineTransform.identity
              // self.selectorContainer.layoutIfNeeded()
               }, completion: {complete in
                
                
        })
        
        self.collectionView.reloadData()
    }
    /// end selector
  weak var delegate: MorseKeyboardViewDelegate?

  /// Cache of signal inputs
  var signalCache: [MorseData.Signal] = [] {
    didSet {
      var text = ""
      if signalCache.count > 0 {
        text = signalCache.reduce("") {
          return $0 + $1.rawValue
        }
        text += " = \(cacheLetter)"
      }
    
    }
  }

  /// The letter represented by the current signalCache
  var cacheLetter: String {
    return MorseData.letter(fromSignals: signalCache) ?? "?"
  }


  override init(frame: CGRect) {
    super.init(frame: frame)
    setColorScheme(.light)
    setNextKeyboardVisible(false)
    initSelector()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setColorScheme(.light)
    setNextKeyboardVisible(false)
    initSelector()
  }

  func setNextKeyboardVisible(_ visible: Bool) {
    //spaceButtonToNextKeyboardConstraint.isActive = visible
    //spaceButtonToParentConstraint.isActive = !visible
    nextKeyboardButton.isHidden = !visible
  }

  func setColorScheme(_ colorScheme: MorseColorScheme) {
    let colorScheme = MorseColors(colorScheme: colorScheme)
   
    backgroundColor = colorScheme.backgroundColor
    collectionView.backgroundColor = .black
    for view in subviews {
      if let button = view as? KeyboardButton {
        button.setTitleColor(colorScheme.buttonTextColor, for: [])
        button.tintColor = colorScheme.buttonTextColor

        if button == nextKeyboardButton || button == deleteButton {
          button.defaultBackgroundColor = colorScheme.buttonHighlightColor
          button.highlightBackgroundColor = colorScheme.buttonBackgroundColor
        } else {
          button.defaultBackgroundColor = colorScheme.buttonBackgroundColor
          button.highlightBackgroundColor = colorScheme.buttonHighlightColor
        }
      }
    }
  }
    
}

// MARK: - Actions
extension MorseKeyboardView {
  @IBAction func dotPressed(button: UIButton) {
    addSignal(.dot)
  }

  @IBAction func dashPressed() {
    addSignal(.dash)
  }

  @IBAction func deletePressed() {
    if signalCache.count > 0 {
      // Remove last signal
      signalCache.removeLast()
    } else {
      // Already didn't have a signal
      if let previousCharacter = delegate?.characterBeforeCursor() {
        if let previousSignals = MorseData.code["\(previousCharacter)"] {
          signalCache = previousSignals
        }
      }
    }

    if signalCache.count == 0 {
      // Delete because no more signal
      delegate?.deleteCharacterBeforeCursor()
    } else {
      // Building on existing letter by deleting current
      delegate?.deleteCharacterBeforeCursor()
      delegate?.insertCharacter(cacheLetter)
    }
  }

  @IBAction func spacePressed() {
    if signalCache.count > 0 {
      // Clear our the signal cache
      signalCache = []
    } else {
      delegate?.insertCharacter(" ")
    }
  }
}

// MARK: - Private Methods
private extension MorseKeyboardView {
  func addSignal(_ signal: MorseData.Signal) {
    if signalCache.count == 0 {
      // Have an empty cache
      signalCache.append(signal)
      delegate?.insertCharacter(cacheLetter)
    } else {
      // Building on existing letter
      signalCache.append(signal)
      delegate?.deleteCharacterBeforeCursor()
      delegate?.insertCharacter(cacheLetter)
    }
  }
}
