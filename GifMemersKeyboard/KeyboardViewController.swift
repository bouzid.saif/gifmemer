//
//  KeyboardViewController.swift
//  GifMemersKeyboard
//
//  Created by macbook on 2019-12-09.
//  Copyright © 2019 52inc. All rights reserved.
//

import UIKit
var proxy : UITextDocumentProxy!
class KeyboardViewController: UIInputViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet var nextKeyboardButton: UIButton!
    var morseKeyboardView: MorseKeyboardView!
     var userLexicon: UILexicon?
    var dataModel: [String] = []
    var dataModelMeme: [String] = []
   
var isLoading = false
    var index = 0
    var currentWord: String? {
      var lastWord: String?
      if let stringBeforeCursor = textDocumentProxy.documentContextBeforeInput {
        stringBeforeCursor.enumerateSubstrings(in: stringBeforeCursor.startIndex...,
                                               options: .byWords)
        { word, _, _, _ in
          if let word = word {
            lastWord = word
          }
        }
      }
      return lastWord
    }
    
   override var hasFullAccess: Bool {
      if #available(iOS 11.0, *) {
        return super.hasFullAccess// super is UIInputViewController.
      }
      if #available(iOS 10.0, *) {
        let original: String? = UIPasteboard.general.string
        UIPasteboard.general.string = " "
        let val: Bool = UIPasteboard.general.hasStrings
        if let str = original {
          UIPasteboard.general.string = str
        }
        return val
      }
      return UIPasteboard.general.isKind(of: UIPasteboard.self)
    }
    override func updateViewConstraints() {
        super.updateViewConstraints()
        self.morseKeyboardView.keyboardView.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 53)
        // Add custom view sizing constraints here
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
      let nib = UINib(nibName: "MorseKeyboardView", bundle: nil)
      let objects = nib.instantiate(withOwner: nil, options: nil)
      morseKeyboardView = objects.first as! MorseKeyboardView
      morseKeyboardView.delegate = self
      proxy = textDocumentProxy as UITextDocumentProxy
      guard let inputView = inputView else { return }
      inputView.addSubview(morseKeyboardView)
      morseKeyboardView.translatesAutoresizingMaskIntoConstraints = false
      
      NSLayoutConstraint.activate([
        morseKeyboardView.leftAnchor.constraint(equalTo: inputView.leftAnchor),
        morseKeyboardView.topAnchor.constraint(equalTo: inputView.topAnchor),
        morseKeyboardView.rightAnchor.constraint(equalTo: inputView.rightAnchor),
        morseKeyboardView.bottomAnchor.constraint(equalTo: inputView.bottomAnchor)
        ])
        
      morseKeyboardView.setNextKeyboardVisible(needsInputModeSwitchKey)
        morseKeyboardView.nextKeyboardButton.addTarget(self, action: #selector(self.goToNextKeyboard(_:)), for: .allTouchEvents)

      requestSupplementaryLexicon { lexicon in
        self.userLexicon = lexicon
      }
      registerNib()
        if hasFullAccess {
            self.morseKeyboardView.collectionView.isHidden = false
            self.morseKeyboardView.fullAccessGif.alpha = 0
            self.morseKeyboardView.keyboardView.alpha = 0
            self.morseKeyboardView.fullAccessGif.isHidden = true
            self.morseKeyboardView.keyboardView.isHidden = true
      fetchGifs()
      fetchMemes()
            
    }else{
            loadKeys()
            self.nextKeyboardButton.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
            self.morseKeyboardView.collectionView.isHidden = true
            self.morseKeyboardView.fullAccessGif.alpha = 1
             self.morseKeyboardView.keyboardView.alpha = 1
            self.morseKeyboardView.fullAccessGif.isHidden = false
            self.morseKeyboardView.keyboardView.isHidden = false
        }
    }
    @objc func goToNextKeyboard(_ sender: Any) {
        print("Will Go")
        self.advanceToNextInputMode()
    }
    func loadKeys(){
        self.morseKeyboardView.keys.forEach{$0.removeFromSuperview()}
        self.morseKeyboardView.paddingViews.forEach{$0.removeFromSuperview()}
        
        let buttonWidth = (UIScreen.main.bounds.width - 6) / CGFloat(Constants.letterKeys[0].count)
        
        var keyboard: [[String]]
        
        //start padding
        switch self.morseKeyboardView.keyboardState {
        case .letters:
            keyboard = Constants.letterKeys
            addPadding(to: self.morseKeyboardView.stackView2, width: buttonWidth/2, key: "a")
        case .numbers:
            keyboard = Constants.numberKeys
        case .symbols:
            keyboard = Constants.symbolKeys
        }
        
        let numRows = keyboard.count
        for row in 0...numRows - 1{
            for col in 0...keyboard[row].count - 1{
                let button = UIButton(type: .custom)
                button.backgroundColor = Constants.keyNormalColour
                button.setTitleColor(.black, for: .normal)
                let key = keyboard[row][col]
                let capsKey = keyboard[row][col].capitalized
                let keyToDisplay = self.morseKeyboardView.shiftButtonState == .normal ? key : capsKey
                button.layer.setValue(key, forKey: "original")
                button.layer.setValue(keyToDisplay, forKey: "keyToDisplay")
                button.layer.setValue(false, forKey: "isSpecial")
                button.setTitle(keyToDisplay, for: .normal)
                button.layer.borderColor = self.morseKeyboardView.keyboardView.backgroundColor?.cgColor
                button.layer.borderWidth = 4
                button.addTarget(self, action: #selector(keyPressedTouchUp), for: .touchUpInside)
                button.addTarget(self, action: #selector(keyTouchDown), for: .touchDown)
                button.addTarget(self, action: #selector(keyUntouched), for: .touchDragExit)
                button.addTarget(self, action: #selector(keyMultiPress(_:event:)), for: .touchDownRepeat)

                if key == "⌫"{
                    let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(keyLongPressed(_:)))
                    button.addGestureRecognizer(longPressRecognizer)
                }
                
                button.layer.cornerRadius = buttonWidth/4
                self.morseKeyboardView.keys.append(button)
                switch row{
                case 0: self.morseKeyboardView.stackView1.addArrangedSubview(button)
                case 1: self.morseKeyboardView.stackView2.addArrangedSubview(button)
                case 2: self.morseKeyboardView.stackView3.addArrangedSubview(button)
                case 3: self.morseKeyboardView.stackView4.addArrangedSubview(button)
                default:
                    break
                }
                if key == "🌐"{
                    nextKeyboardButton = button
                }
                
                //top row is longest row so it should decide button width
                print("button width: ", buttonWidth)
                if key == "⌫" || key == "↩" || key == "#+=" || key == "ABC" || key == "123" || key == "⬆️" || key == "🌐"{
                    button.widthAnchor.constraint(equalToConstant: buttonWidth + buttonWidth/2).isActive = true
                    button.layer.setValue(true, forKey: "isSpecial")
                    button.backgroundColor = Constants.specialKeyNormalColour
                    if key == "⬆️" {
                        if self.morseKeyboardView.shiftButtonState != .normal{
                            button.backgroundColor = Constants.keyPressedColour
                        }
                        if self.morseKeyboardView.shiftButtonState == .caps{
                            button.setTitle("⏫", for: .normal)
                        }
                    }
                }else if (self.morseKeyboardView.keyboardState == .numbers || self.morseKeyboardView.keyboardState == .symbols) && row == 2{
                    button.widthAnchor.constraint(equalToConstant: buttonWidth * 1.4).isActive = true
                }else if key != "space"{
                    button.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
                }else{
                    button.layer.setValue(key, forKey: "original")
                    button.setTitle(key, for: .normal)
                }
            }
        }
        
        
        //end padding
        switch self.morseKeyboardView.keyboardState {
        case .letters:
            addPadding(to: self.morseKeyboardView.stackView2, width: buttonWidth/2, key: "l")
        case .numbers:
            break
        case .symbols: break
        }
        
    }
    func addPadding(to stackView: UIStackView, width: CGFloat, key: String){
        let padding = UIButton(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
        padding.setTitleColor(.clear, for: .normal)
        padding.alpha = 0.02
        padding.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //if we want to use this padding as a key, for example the a and l buttons
        let keyToDisplay = self.morseKeyboardView.shiftButtonState == .normal ? key : key.capitalized
        padding.layer.setValue(key, forKey: "original")
        padding.layer.setValue(keyToDisplay, forKey: "keyToDisplay")
        padding.layer.setValue(false, forKey: "isSpecial")
        padding.addTarget(self, action: #selector(keyPressedTouchUp), for: .touchUpInside)
        padding.addTarget(self, action: #selector(keyTouchDown), for: .touchDown)
        padding.addTarget(self, action: #selector(keyUntouched), for: .touchDragExit)
        
        self.morseKeyboardView.paddingViews.append(padding)
        stackView.addArrangedSubview(padding)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.morseKeyboardView.isGif {
            if dataModel.count == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if !self.morseKeyboardView.spinner.isAnimating {
                                  
                                       self.morseKeyboardView.collectionView.isHidden = true
                                       self.morseKeyboardView.nofavoritesView.isHidden = false
                                   }
                }
               
                
                
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                 if !self.morseKeyboardView.spinner.isAnimating {
               
                self.morseKeyboardView.collectionView.isHidden = false
                self.morseKeyboardView.nofavoritesView.isHidden = true
                }
                }
            }
           return dataModel.count
        }else{
            if dataModelMeme.count == 0 {
               DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                  if !self.morseKeyboardView.spinner.isAnimating {
                
                self.morseKeyboardView.collectionView.isHidden = true
                self.morseKeyboardView.nofavoritesView.isHidden = false
                }
                }
            }else{
               DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                  if !self.morseKeyboardView.spinner.isAnimating {
                
                self.morseKeyboardView.collectionView.isHidden = false
                               self.morseKeyboardView.nofavoritesView.isHidden = true
                }
                }
            }
            return dataModelMeme.count
                
        }
       }

       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GifCollectionViewCell", for: indexPath) as? GifCollectionViewCell else {
               return UICollectionViewCell()
           }
         print(dataModel)
        if self.morseKeyboardView.isGif {
         var urlGifTemp = dataModel[indexPath.row]
         urlGifTemp.removeLast()
         if urlGifTemp.starts(with: "192.168.1.25") {
             urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
         }
            
                 cell.setGif(urlString: "http://gifmemer.com/" + urlGifTemp + ".gif")
            
        
            }else{
             var urlGifTemp = dataModel[indexPath.row]
                if urlGifTemp.starts(with: "192.168.1.25") {
                    urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                }
            
                cell.setGif(urlString: "http://gifmemer.com/" + urlGifTemp + ".jpg")
            
            }
        if !self.morseKeyboardView.spinner.isHidden {
            self.morseKeyboardView.spinner.stopAnimating()
               collectionView.backgroundColor = UIColor.black
           }
        
           return cell
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let numberOfCellsInOneRow: CGFloat = 3
           let spacing: CGFloat = 2
           let heightWidth = (UIScreen.main.bounds.width - numberOfCellsInOneRow * spacing)/numberOfCellsInOneRow
         return CGSize(width: (collectionView.frame.height * 0.1) + collectionView.frame.height, height: collectionView.frame.height)
          // return CGSize(width: heightWidth, height: heightWidth)
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 2
       }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GifCollectionViewCell", for: indexPath) as? GifCollectionViewCell else {
            return
        }
        cell.imageView.removeFromSuperview()
    }
    private func registerNib() {
        morseKeyboardView.collectionView.register(UINib(nibName: "GifCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GifCollectionViewCell")
        morseKeyboardView.collectionView.delegate = self
        morseKeyboardView.collectionView.dataSource = self
       }
    func fetchGifs() {
        
       // guard let giphyURL = URL(string: "http://gifmemer.ddns.net:3030/gifs/MyGifs") else { return }
        //imagesViewHeightConstraint.constant = isShowingGif ? 300: 0
        morseKeyboardView.spinner.startAnimating()
        morseKeyboardView.collectionView.backgroundColor = UIColor.white
          let defaultsToKeyboard = UserDefaults(suiteName: "group.com.regystone.gifmemer.keyboard")
      let uid = defaultsToKeyboard?.object(forKey: "userUID")
        if let firstLaunch = uid as? String {
     //var firstLaunch = "5dca835f9290a97849f39799"
    
   /*  let parameters : Parameters = [
                   "userId" : firstLaunch
               ]
    print(parameters)
      let headers : HTTPHeaders = [
          "Content-Type" : "application/x-www-form-urlencoded"
      
      ] */
      guard let url = URL(string:"http://gifmemer.com/gifs/MyGifsKeyboard") else { print("Invalid URL: http://gifmemer.com/gifs/MyGifsKeyboard")
          return
      }
      var urlRequest = URLRequest(url: url)
      let parametersU: [String: Any] = [
            "userId" : firstLaunch
      ]
     // urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

      urlRequest.httpMethod = "POST"
      urlRequest.httpBody = parametersU.percentEscaped().data(using: .utf8)
        
          let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
              print("task done")
              print(error?.localizedDescription)
              if error == nil {
                do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
              print(json)
                    if let object = json as? [Any] {
                        for item in object as! [Dictionary<String,Any>] {
                            self.dataModel.append(item["url"] as! String)
                        }
             
              DispatchQueue.main.async {
                  self.morseKeyboardView.collectionView.reloadData()
              }
                    }
                }catch {
                    print("KeyboardExtension : ",error.localizedDescription)
                }
              }else{
                
                self.morseKeyboardView.spinner.stopAnimating()
                 self.morseKeyboardView.collectionView.isHidden = true
                self.morseKeyboardView.labelNoGifs.text = "\(error!.localizedDescription)"
            }
              
          }
      task.resume()
        }
    }
    func fetchMemes() {
         
        // guard let giphyURL = URL(string: "http://gifmemer.ddns.net:3030/gifs/MyGifs") else { return }
         //imagesViewHeightConstraint.constant = isShowingGif ? 300: 0
         morseKeyboardView.spinner.startAnimating()
         morseKeyboardView.collectionView.backgroundColor = UIColor.white
           let defaultsToKeyboard = UserDefaults(suiteName: "group.com.regystone.gifmemer.keyboard")
       let uid = defaultsToKeyboard?.object(forKey: "userUID")
         if let firstLaunch = uid as? String {
      //var firstLaunch = "5dca835f9290a97849f39799"
     
    /*  let parameters : Parameters = [
                    "userId" : firstLaunch
                ]
     print(parameters)
       let headers : HTTPHeaders = [
           "Content-Type" : "application/x-www-form-urlencoded"
       
       ] */
       guard let url = URL(string:"https://gifmemer.ddns.net:3031/mimes/MyMemesKeyboard") else { print("Invalid URL: https://gifmemer.ddns.net:3031/mimes/MyMemesKeyboard")
           return
       }
       var urlRequest = URLRequest(url: url)
       let parametersU: [String: Any] = [
             "userId" : firstLaunch
       ]
      // urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

       urlRequest.httpMethod = "POST"
       urlRequest.httpBody = parametersU.percentEscaped().data(using: .utf8)
         
           let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
               print("task done")
            DispatchQueue.main.async {
                self.morseKeyboardView.isMemeLoaded = true
            }
               print(error?.localizedDescription)
               if error == nil {
                 do {
                 let json = try JSONSerialization.jsonObject(with: data!, options: [])
               print(json)
                     if let object = json as? [Any] {
                         for item in object as! [Dictionary<String,Any>] {
                             self.dataModelMeme.append(item["url"] as! String)
                         }
              
               
                     }
                 }catch {
                     print("KeyboardExtension : ",error.localizedDescription)
                 }
               }else{
                 self.morseKeyboardView.spinner.stopAnimating()
                  self.morseKeyboardView.collectionView.isHidden = true
                 self.morseKeyboardView.labelNoGifs.text = "\(error!.localizedDescription)"
             }
               
           }
       task.resume()
         }
     }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.morseKeyboardView.oneTimeWork {
                self.morseKeyboardView.oneTimeWork = false
               DispatchQueue.main.async {
                   self.morseKeyboardView.selectorContainer.frame = self.morseKeyboardView.frameCustom
                    
                   
                   
               }
               

           }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
    }
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        
        let colorScheme: MorseColorScheme
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
            colorScheme = .dark
        } else {
            colorScheme = .light
        }
         morseKeyboardView.setColorScheme(colorScheme)
    }

}
// MARK: - MorseKeyboardViewDelegate
extension KeyboardViewController: MorseKeyboardViewDelegate {
  func insertCharacter(_ newCharacter: String) {
  
    textDocumentProxy.insertText(newCharacter)
  }

  func deleteCharacterBeforeCursor() {
    textDocumentProxy.deleteBackward()
  }

  func characterBeforeCursor() -> String? {
    guard let character = textDocumentProxy.documentContextBeforeInput?.last else {
      return nil
    }
    return String(character)
  }
}

// MARK: - Private methods
private extension KeyboardViewController {
  func attemptToReplaceCurrentWord() {
    guard let entries = userLexicon?.entries,
      let currentWord = currentWord?.lowercased() else {
        return
    }

    let replacementEntries = entries.filter {
      $0.userInput.lowercased() == currentWord
    }

    if let replacement = replacementEntries.first {
      for _ in 0..<currentWord.count {
        textDocumentProxy.deleteBackward()
      }

      textDocumentProxy.insertText(replacement.documentText)
    }
  }
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    func stringByAddingPercentEncodingForRFC3986() -> String? {
       let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return self.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
     }
    public func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String? {
        let unreserved = "*-._"
        let allowedCharacterSet = NSMutableCharacterSet.alphanumeric()
        allowedCharacterSet.addCharacters(in: unreserved)
        
        if plusForSpace {
            allowedCharacterSet.addCharacters(in: " ")
        }
        
        var encoded =  self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet as CharacterSet)
        if plusForSpace {
            
          encoded = encoded?.replacingOccurrences(of: " ", with: "+")
        }
        return encoded
      }
}
enum Constants{
    
    static let keyNormalColour: UIColor = .white
    static let keyPressedColour: UIColor = .lightText
    static let specialKeyNormalColour: UIColor = .gray

    static let letterKeys = [
        ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
        ["a", "s", "d", "f", "g","h", "j", "k", "l"],
        ["⬆️", "z", "x", "c", "v", "b", "n", "m", "⌫"],
        ["123", "🌐", "space", "↩"]
    ]
    static let numberKeys = [
        ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",],
        ["-", "/", ":", ";", "(", ")" ,",", "$", "&", "@", "\""],
        ["#+=",".", ",", "?", "!", "\'", "⌫"],
        ["ABC", "🌐", "space", "↩"]
    ]
    
    static let symbolKeys = [
        ["[", "]", "{", "}", "#", "%", "^", "*", "+", "="],
        ["_", "\\", "|", "~", "<", ">", "€", "£", "¥", "·"],
        ["123",".", ",", "?", "!", "\'", "⌫"],
        ["ABC", "🌐", "space", "↩"]
    ]
}
extension KeyboardViewController {
    func handlDeleteButtonPressed(){
        proxy.deleteBackward()
    }
    func changeKeyboardToNumberKeys(){
        self.morseKeyboardView.keyboardState = .numbers
        self.morseKeyboardView.shiftButtonState = .normal
        loadKeys()
    }
    func changeKeyboardToLetterKeys(){
        self.morseKeyboardView.keyboardState = .letters
        loadKeys()
    }
    func changeKeyboardToSymbolKeys(){
        self.morseKeyboardView.keyboardState = .symbols
        loadKeys()
    }
    @IBAction func keyPressedTouchUp(_ sender: UIButton) {
        guard let originalKey = sender.layer.value(forKey: "original") as? String, let keyToDisplay = sender.layer.value(forKey: "keyToDisplay") as? String else {return}
        
        guard let isSpecial = sender.layer.value(forKey: "isSpecial") as? Bool else {return}
        sender.backgroundColor = isSpecial ? Constants.specialKeyNormalColour : Constants.keyNormalColour

        switch originalKey {
        case "⌫":
            if self.morseKeyboardView.shiftButtonState == .shift {
                self.morseKeyboardView.shiftButtonState = .normal
                loadKeys()
            }
           handlDeleteButtonPressed()
        case "space":
            proxy.insertText(" ")
        case "🌐":
            break
        case "↩":
            proxy.insertText("\n")
        case "123":
            changeKeyboardToNumberKeys()
        case "ABC":
            changeKeyboardToLetterKeys()
        case "#+=":
            changeKeyboardToSymbolKeys()
        case "⬆️":
            self.morseKeyboardView.shiftButtonState = self.morseKeyboardView.shiftButtonState == .normal ? .shift : .normal
            loadKeys()
        default:
            if self.morseKeyboardView.shiftButtonState == .shift {
                self.morseKeyboardView.shiftButtonState = .normal
                loadKeys()
            }
            proxy.insertText(keyToDisplay)
        }
    }
    
    @objc func keyMultiPress(_ sender: UIButton, event: UIEvent){
        guard let originalKey = sender.layer.value(forKey: "original") as? String else {return}

        let touch: UITouch = event.allTouches!.first!
        if (touch.tapCount == 2 && originalKey == "⬆️") {
            self.morseKeyboardView.shiftButtonState = .caps
            loadKeys()
        }
    }
    
    @objc func keyLongPressed(_ gesture: UIGestureRecognizer){
        if gesture.state == .began {
            self.morseKeyboardView.backspaceTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
                self.handlDeleteButtonPressed()
            }
        } else if gesture.state == .ended || gesture.state == .cancelled {
            self.morseKeyboardView.backspaceTimer?.invalidate()
            self.morseKeyboardView.backspaceTimer = nil
            (gesture.view as! UIButton).backgroundColor = Constants.specialKeyNormalColour
        }
    }
    
    @objc func keyUntouched(_ sender: UIButton){
        guard let isSpecial = sender.layer.value(forKey: "isSpecial") as? Bool else {return}
        sender.backgroundColor = isSpecial ? Constants.specialKeyNormalColour : Constants.keyNormalColour
    }
    
    @objc func keyTouchDown(_ sender: UIButton){
        sender.backgroundColor = Constants.keyPressedColour
    }
    
    
    
}
