//
//  GifCollectionViewCell.swift
//  Giffy Keyboard
//
//  Created by Nisha  on 04/11/18.
//  Copyright © 2018 Nisha . All rights reserved.
//

import UIKit
import FLAnimatedImage
class GifCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: FLAnimatedImageView!
    @IBOutlet weak var activityIndic : UIActivityIndicatorView!
    var urlGif : String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .black
    }
    
    func setGif(urlString: String) {
        do {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let strongSelf = self else {return }
                strongSelf.downloadImageFrom(urlString: urlString) { (data) in
                    if let _data = data {
                        DispatchQueue.main.async {
                           let gifImage = FLAnimatedImage(gifData: _data)
                            strongSelf.imageView.animatedImage = gifImage
                                       strongSelf.urlGif = urlString
                            strongSelf.activityIndic.stopAnimating()
                                       let tapgesture = UITapGestureRecognizer(target: strongSelf, action: #selector(strongSelf.celldidTapped(_:)))
                            strongSelf.imageView.isUserInteractionEnabled = true
                                       strongSelf.addGestureRecognizer(tapgesture)
                        }
                    }
                }
                
            }
       /* let gifImage = FLAnimatedImage(gifData: try Data(contentsOf: URL(string: urlString)!))
        imageView.animatedImage = gifImage
            self.urlGif = urlString
            let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.celldidTapped(_:)))
            imageView.isUserInteractionEnabled = true
            self.addGestureRecognizer(tapgesture) */
            
        }catch{
            print("GifCollectionViewCell : ",error.localizedDescription)
        }
    }
    func downloadImageFrom(urlString:String, completion:@escaping(Data?)->()) {
        guard let url = URL(string:urlString) else { return }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { (data, _, err) in
            if err != nil {
                // handle error if any
            }
            // you should check the reponse status
            // if data is a json object/dictionary so decode it
            // if data is regular data then pass it to your callback
            completion(data)
        }.resume()
    }
    @objc func celldidTapped(_ sender: UITapGestureRecognizer) {
        if !self.subviews.contains(where: { (view) -> Bool in
            if view .isKind(of: UIButton.self) {
                return true
            }
            return false
        }) {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "send"), for: .normal)
            button.layer.cornerRadius = 64 / 2
            button.layer.masksToBounds = true
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor.white.cgColor
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(self.willSendGif(_:)), for: .touchUpInside)
        self.addSubview(button)
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalToSystemSpacingAfter: self.centerXAnchor, multiplier: 1.0),
            button.centerYAnchor.constraint(equalToSystemSpacingBelow: self.centerYAnchor, multiplier: 1.0),
            button.widthAnchor.constraint(equalToConstant: 64),
            button.heightAnchor.constraint(equalToConstant: 64)
        
        ])
        }else{
            for v in self.subviews {
                if v .isKind(of: UIButton.self) {
                    v.removeFromSuperview()
                }
            }
        }
        
    }
    
    @objc func willSendGif(_ sender: UIButton) {
        if let data = NSData(contentsOf: URL(string: self.urlGif)!) {
          
            UIPasteboard.general.setData(data as Data, forPasteboardType: "com.compuserve.gif")
           
            //self.textFieldPrimaray.insertText(String(decoding: data as Data, as: UTF8.self))
            let copiedView = UIView()
            self.addSubview(copiedView)
            DispatchQueue.main.async {
                sender.isHidden = true
            }
            copiedView.translatesAutoresizingMaskIntoConstraints = false
            copiedView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            copiedView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            copiedView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
            copiedView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
            copiedView.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
            copiedView.alpha = 0
            let copiedImage = UIImageView(image: UIImage(named: "copied")?.withRenderingMode(.alwaysTemplate))
            copiedImage.tintColor = .white
            copiedView.addSubview(copiedImage)
            copiedImage.contentMode = .scaleAspectFit
            copiedImage.translatesAutoresizingMaskIntoConstraints = false
            copiedImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            copiedImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            copiedImage.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5).isActive = true
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                copiedView.alpha = 1
            }) { (_) -> Void in
                UIView.animate(withDuration: 0.5, animations: { () -> Void in
                    copiedView.alpha = 0
                    sender.removeFromSuperview()
                })
        }
        }

    }
}
