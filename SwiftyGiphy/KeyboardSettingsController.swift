//
//  KeyboardSettingsController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2020-01-02.
//  Copyright © 2020 52inc. All rights reserved.
//

import Foundation
import UIKit
class KeyboardSettingsController: UIViewController {
    
    @IBOutlet weak var settingsUpLBL: UILabel!
    @IBOutlet weak var toSetUpLBL: UILabel!
    @IBOutlet weak var setupOneLBL: UILabel!
    @IBOutlet weak var setupTwoLBL: UILabel!
    @IBOutlet weak var setupThreeLBL: UILabel!
    @IBOutlet weak var goToSettingsBTN: UIButton!
    @IBAction func goToSettingsAction(_ sender: UIButton){
   UIApplication.openAppSettings()
    }
    @IBAction func closeView(_ sender : Any ) {
        self.dismiss(animated: true, completion: nil)
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsUpLBL.text = Localization("settingsUpLBL")
        toSetUpLBL.text = Localization("toSetUpLBL")
        setupOneLBL.text = Localization("setupOneLBL")
        setupTwoLBL.text = Localization("setupTwoLBL")
        setupThreeLBL.text = Localization("setupThreeLBL")
        goToSettingsBTN.setTitle(Localization("goToSettingsBTN"), for: .normal)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
