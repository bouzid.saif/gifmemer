//
//  CustomToast.swift
//  NotificationBanner_Example
//
//  Created by macbook on 2019-01-16.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
class CustomToast: UIView {
    static let shared = CustomToast()
     fileprivate var containerView: UIView!
     fileprivate let nibName = "CustomToast"
    @IBOutlet weak var LabelN : UILabel!
    @IBOutlet weak var ImageV : UIImageView!
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth]
        addSubview(containerView)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.frame.size = CGSize(width: self.containerView.frame.width, height: 80)
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle.main
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
    
}
