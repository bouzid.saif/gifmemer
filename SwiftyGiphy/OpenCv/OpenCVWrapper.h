//
//  OpenCVWrapper.h
//  YLYL
//
//  Created by macbook on 2019-11-08.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface OpenCVWrapper : NSObject
- (size_t) isThisWorking:(UIImage*) image;
@end

NS_ASSUME_NONNULL_END
