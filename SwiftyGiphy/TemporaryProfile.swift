//
//  TemporaryProfile.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-13.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SwiftyGiphy
import AVKit
import TLPhotoPicker
import Photos
import MapleBacon
import AssetsLibrary
import OneSignal
import iOSPhotoEditor
import MobileCoreServices
class TemporaryProfile: appleDetection,UICollectionViewDelegate,UICollectionViewDataSource,PhotoEditorDelegate {
    
    var photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
    
    @IBOutlet weak var downloadShareView: BorderView!
    
    @IBOutlet weak var downloadShareLBL: UILabel!
    
    @IBOutlet weak var mpFourBTN: UIButton!
    
    @IBOutlet weak var gifBTN: UIButton!
    
    @IBOutlet weak var backDownloadShareBTN: BorderButton!
    var isDownload = true
   
    @IBAction func mpFourAction(_ sender: UIButton) {
        if isDownload {
                   SwiftSpinner.show(Localization("Downloading..."))
                   PHPhotoLibrary.shared().performChanges({
                let request = PHAssetCreationRequest.forAsset()
                       if self.isGif {
                        
                       // request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(string:self.globalURlVideo)!) as! PHAssetCreationRequest
                       
                        request.creationDate = Date()
                      
                           do {
                               let data = try Data(contentsOf: URL(string:self.globalURlVideo)!)
                           let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                            let pathToMovie = documentsPath + "/stream.mp4"
                                               
                            
                            try data.write(to: URL(fileURLWithPath: pathToMovie))
                           //print("savable :", UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(pathToMovie))
                            request.addResource(with: .video, fileURL: URL(fileURLWithPath: pathToMovie), options: nil)
                           //request.addResource(with: .pairedVideo, data: data, options: nil)
                           }catch{
                       print("unable to download the video : ",error.localizedDescription)
                           }
                        
                      
                       }else{
                           
                        
                         guard let shareData: Data = self.fullGifImageView.image?.pngData() else {
                             return
                         }
                           request.addResource(with: .photo, data: shareData, options: nil)
                       }
                      }) { (success, error) in
                          if let error = error {
                            DispatchQueue.main.async {
                                
                          
                           UIView.animate(withDuration: 0.4, animations: {
                               self.downloadShareView.alpha = 0
                           }) { (complete) in
                               self.downloadShareView.isHidden = true
                           }
                                  }
                           print("png or video can't be saved : ",error.localizedDescription)
                              //completion(.failure(error))
                            DispatchQueue.main.async {
                           SwiftSpinner.show(duration: 2.0, title: Localization("Not_saved"))
                           }
                          } else {
                              DispatchQueue.main.async {
                           UIView.animate(withDuration: 0.4, animations: {
                                                  self.downloadShareView.alpha = 0
                                              }) { (complete) in
                                                  self.downloadShareView.isHidden = true
                                              }
                            }
                             print("Video saved")
                           DispatchQueue.main.async {
                                SwiftSpinner.show(duration: 2.0, title: Localization("Saved"))
                               
                           }
                          
                          
                             // completion(.success(true))
                          }
                      }
        }else{
            SwiftSpinner.show(Localization("Downloading..."))
                   do {
                    let data = try Data(contentsOf: URL(string:self.globalURlVideo)!)
                                 // now you have the data
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                    let pathToMovie = documentsPath + "/gifmemer.mp4"
                                       
                    
                    try data.write(to: URL(fileURLWithPath: pathToMovie))
                        
                            
                        let firstActivityItem: Array = [URL(fileURLWithPath: pathToMovie)]
                        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)
                        activityViewController.excludedActivityTypes = [.addToReadingList, .assignToContact]

                                                     
                        DispatchQueue.main.async {
                                                // display your imageView with the data
                        SwiftSpinner.hide()
                            self.present(activityViewController, animated: true) {
                          UIView.animate(withDuration: 0.4, animations: {
                          self.downloadShareView.alpha = 0
                                  }) { (completed) in
                          self.downloadShareView.isHidden = true
                                                 }
                                               }
                                            }
                       //request.addResource(with: .pairedVideo, data: data, options: nil)
                       }catch{
                   print("unable to share the video : ",error.localizedDescription)
                       }
           
        }
    }
    
    @IBAction func backDownloadShareAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4, animations: {
            self.downloadShareView.alpha = 0
        }) { (complete) in
            self.downloadShareView.isHidden = true
        }
    }
    
    @IBAction func gifAction(_ sender: UIButton) {
        if isDownload {
            SwiftSpinner.show(Localization("Downloading..."))
            PHPhotoLibrary.shared().performChanges({
                   let request = PHAssetCreationRequest.forAsset()
                if self.isGif {
              
                    do {
                        let data = try Data(contentsOf: URL(string:self.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".gif"))!)
                    request.addResource(with: .photo, data: data, options: nil)
                    }catch{
                print("unable to download the gif : ",error.localizedDescription)
                    }
               
                }else{
                     
                  guard let shareData: Data = self.fullGifImageView.image?.pngData() else {
                      return
                  }
                    request.addResource(with: .photo, data: shareData, options: nil)
                }
               }) { (success, error) in
                   if let error = error {
                    DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.downloadShareView.alpha = 0
                    }) { (complete) in
                        self.downloadShareView.isHidden = true
                    }
                    }
                    print("Gif can't be saved : ",error.localizedDescription)
                       //completion(.failure(error))
                     DispatchQueue.main.async {
                    SwiftSpinner.show(duration: 2.0, title: Localization("Not_saved"))
                    }
                   } else {
                    DispatchQueue.main.async {
                        
                   
                    UIView.animate(withDuration: 0.4, animations: {
                                           self.downloadShareView.alpha = 0
                                       }) { (complete) in
                                           self.downloadShareView.isHidden = true
                                       }
                         }
                      print("Gif saved")
                    DispatchQueue.main.async {
                         SwiftSpinner.show(duration: 2.0, title: Localization("Saved"))
                        
                    }
                   
                   
                      // completion(.success(true))
                   }
               }
        }else{
              SwiftSpinner.show(Localization("Downloading..."))
            
            DispatchQueue.global(qos: .background).async { [weak self] in
                   guard let strongSelf = self else { return } // to avoid reference cycle
              strongSelf.downloadImageFrom(urlString: strongSelf.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".gif"), completion: { (data) in
                       if let _data = data {
                           // now you have the data
                          let shareData: Data = _data
                                          
                                let firstActivityItem: Array = [shareData]
                                let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)
                                    
                           DispatchQueue.main.async {
                               // display your imageView with the data
                              SwiftSpinner.hide()
                         
                    strongSelf.present(activityViewController, animated: true) {
                                UIView.animate(withDuration: 0.4, animations: {
                                    strongSelf.downloadShareView.alpha = 0
                                }) { (completed) in
                                    strongSelf.downloadShareView.isHidden = true
                                }
                              }
                           }
                       }
                   })
               }

        }
    }
    @IBAction func modifyGifMemeAction(_ sender: UIButton) {
        photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
        photoEditor.photoEditorDelegate = self
        if isGif {
            
            let imageData = try! Data(contentsOf: URL(string: self.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".jpg"))!)
            let image = UIImage(data: imageData)
            photoEditor.image = image
            
        }else{
             photoEditor.image = fullGifImageView.image
        }
       m
        for i in 17...64 {
            photoEditor.stickers.append(UIImage(named: i.description )!)
        }
        //if isGif {
        photoEditor.hiddenControls = [.save,.share]
        //}
         photoEditor.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        present(photoEditor, animated: true, completion: nil)
    }
    func doneEditing(image: UIImage,source: UIImage) {
        print("Done Editing")
        if isGif {
           
            SwiftSpinner.show(Localization("creating"),animated: true)
               
               let sourceImage = source
                var newGifURL : URL? = nil
                DispatchQueue.background(background: {
                    // do something in background
                    newGifURL = Handler(encoder: Encoder(),decoder: Decoder()).modify(gifUrl: URL(string: self.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".gif"))!) { image -> UIImage in
                        let imagetoParse = self.resizeImage(image: image, targetSize: sourceImage.size)
                         let finalImage =  UIImage.imageByCombiningImage(firstImage: imagetoParse, withImage: sourceImage)
                       
                        return finalImage!
                    }
                }, completion:{
                    // when background job finished, do something in main thread
                    PHPhotoLibrary.shared().performChanges({
                                let request = PHAssetCreationRequest.forAsset()
                          
                           
                                 do {
                                    let data = try Data(contentsOf: newGifURL!)
                                 request.addResource(with: .photo, data: data, options: nil)
                                 }catch{
                             print("unable to download the gif : ",error.localizedDescription)
                                 }
                            
                      
                            }) { (success, error) in
                                if let error = error {
                                 
                                 print("Gif can't be saved : ",error.localizedDescription)
                                    //completion(.failure(error))
                                  DispatchQueue.main.async {
                                 SwiftSpinner.show(duration: 2.0, title: Localization("Not_saved"))
                                 }
                                } else {
                                   print("Gif saved")
                                 DispatchQueue.main.async {
                                      SwiftSpinner.show(duration: 2.0, title: Localization("Saved"))
                                    self.photoEditor.dismiss(animated: true, completion: nil)
                                 }
                                
                                
                                   // completion(.success(true))
                                }
                            }
                })
                
                
               
          /*  var imagesToTreat : [UIImage] = []
            
                let result = Decoder().decode(gifUrl: self.fullGifImageView.sd_imageURL()!)
                Encoder().enc
            for i in 0...((result?.images.count)! - 1) {
                print("Starting : ", i)
                uploadProgress = Int(i / result!.images.count)
                DispatchQueue.main.async {
                SwiftSpinner.show(progress: Double(uploadProgress), title: "Creating  \n  \(Int(uploadProgress * 100))%")
                }
              let imageToTreat = result!.images[i]
                
               let finalImage =  UIImage.imageByCombiningImage(firstImage: imageToTreat, withImage: sourceImage)
                if finalImage != nil {
                    print("Appended : ", i)
                    imagesToTreat.append(finalImage!)
                }
            }
            DispatchQueue.main.async {
                 print("Merging")
            SwiftSpinner.show("Merging...")
            }
             print("Creating Gif")
           let urlCreated  =  UIImage.createanimatedGif(from: imagesToTreat)
            print("End Creating Gif : ", urlCreated?.absoluteString)

            if urlCreated != nil {
                 DispatchQueue.main.async {
                SwiftSpinner.hide()
                }
                fullGifImageView.setGifFromURL(urlCreated)
            } */
            
        }else{
          self.fullGifImageView.image = image
              self.photoEditor.dismiss(animated: true, completion: nil)
        }
    }
     func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    func canceledEditing() {
        
    }
    
    
    
    //MARK: Settings View
    
    @IBOutlet weak var settingsView: UIView!
    
    @IBOutlet weak var settingsBackgroundView: UIView!
    
    @IBAction func CloseSettingsAction(_ sender: UITapGestureRecognizer) {
       
        UIView.animate(withDuration: 0.3, animations: {
            self.settingsView.alpha = 0
        }, completion: { (verified) in
            self.settingsView.isHidden = true
            self.settingsView.alpha  = 1
            self.settingsBackgroundView.isHidden = true
        
    })
        
    }
    @IBOutlet weak var gifmemerKeybView: UIView!
    
    @IBOutlet weak var gimemerKeybLBL: UILabel!
    
    @IBOutlet weak var termsView: UIView!
    
    @IBOutlet weak var termsLBL: UILabel!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var logoutLBL: UILabel!
    @IBAction func goToSettings(_ sender: Any) {
        self.settingsView.alpha  = 0
        self.settingsView.isHidden = false
        self.settingsBackgroundView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.settingsView.alpha = 1
        }, completion: nil)
    }
    func initSettingsViews(){
        let tapgestureKeyboard = UITapGestureRecognizer(target: self, action: #selector(self.goToKeyboard(_:)))
         let tapgestureTerms = UITapGestureRecognizer(target: self, action: #selector(self.goToTerms(_:)))
         let tapgestureLogout = UITapGestureRecognizer(target: self, action: #selector(self.goToLogout(_:)))
        
        gifmemerKeybView.isUserInteractionEnabled = true
        termsView.isUserInteractionEnabled = true
        logoutView.isUserInteractionEnabled = true
        gifmemerKeybView.addGestureRecognizer(tapgestureKeyboard)
        termsView.addGestureRecognizer(tapgestureTerms)
        logoutView.addGestureRecognizer(tapgestureLogout)


    }
    @objc func goToKeyboard(_ sender : UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "KeyboardSettingsController") as! KeyboardSettingsController
        vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true) {
                   self.settingsView.isHidden = true
        }
    }
    @objc func goToTerms(_ sender : UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsProfileController") as! TermsProfileController
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true) {
            self.settingsView.isHidden = true
        }
       
    }
    @objc func goToLogout(_ sender : UITapGestureRecognizer) {
        let yesAction = UIAlertAction(title: Localization("Yes"), style: .destructive) { (alert) in
            //do the work
            //self.signOut()
            UIView.animate(withDuration: 0.3, animations: {
                           self.settingsView.alpha = 0
                       }) { (comp) in
                           self.settingsView.isHidden  = true
                        let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            OneSignal.setSubscription(false)
                    
                            UserDefaults.standard.synchronize()
                            HHTabBarView.shared.selectTabAtIndex(withIndex: 2)
                       }
           
            
        }
        let noAction = UIAlertAction(title: Localization("No"), style: .cancel){ (alert) in
            //do the work
            UIView.animate(withDuration: 0.3, animations: {
                self.settingsView.alpha = 0
            }) { (comp) in
                self.settingsView.isHidden  = true
            }
        }
        let alert = UIAlertController(title: Localization("Disconnect"), message: Localization("AYS"), preferredStyle: .alert)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    //
    func getUser(callback: @escaping (_ messageInfo : Bool) -> Void) {
             guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
               return }
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            do {
                   let a = try JSON(data: dataFromString!)
           let parameters : Parameters = [
               "userId" : a["_id"].stringValue
           ]
           let headers : HTTPHeaders = [
           "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
           ]
           Alamofire.request(URL(string: ScriptBase.sharedInstance.getZonz)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                let data = JSON(response.data ?? Data())
               if data["user"].exists() {
               //UserDefaults.standard.setValue(data["user"].rawString(), forKey: "UserZonzay")
               //UserDefaults.standard.synchronize()
                   
                   let profilePic = a["userImageURL"].stringValue
                                       if profilePic != ""{
                                           self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                                           
                                       }
                   callback(true)
                   
               }else{
                   callback(false)
               }
               }
                               
                            }catch {
                               print("GfyCatTrendingController :",error.localizedDescription)
           }
       }
    
    //MARK: Show Unpayed Gifs
    
    @IBOutlet weak var unpayedView: UIView!
    
    @IBOutlet weak var unpayedGifImageView: UIImageView!
    
    @IBOutlet weak var unpayedGifNameLBL: UILabel!
    
    @IBOutlet weak var zonzUserLBL: UILabel!
        
    @IBOutlet weak var lastNameLBL: UILabel!
    @IBOutlet weak var backLBL: UILabel!
    var indexPathToSend : Int = 0
    @IBOutlet weak var zonzContainer : BuyZonz!
    @IBOutlet weak var zonzBackground: UIImageView!
    @IBAction func closeBuyZonz(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: {
            self.zonzContainer.alpha = 0
            self.zonzBackground.alpha = 0
        }) { (complete) in
            self.zonzContainer.isHidden = true
            self.zonzBackground.isHidden = true
        }
    }
    @IBAction func showBuyZonz(_ sender: UIButton) {
        self.zonzContainer.alpha = 0
        self.zonzBackground.alpha = 0
        self.zonzContainer.isHidden = false
        self.zonzBackground.isHidden = false
        self.zonzContainer.MyZonzLBL.text = Localization("MyGimees")
        self.zonzContainer.zonzTextLBL.text = Localization("BuyGimees")
        let sum = self.zonzUserLBL.text!
        self.zonzContainer.zonzPointsLBLTOP.text = sum
        self.zonzContainer.zonzPointsLBLTOP.font = sum.font(named: "Roboto-Medium", toFit: self.zonzContainer.zonzPointsLBLTOP.frame.size, noSmallerThan: 14.0, noLargerThan: 33.0)
        self.zonzContainer.navigation = self.navigationController
        UIView.animate(withDuration: 0.3, animations: {
            self.zonzContainer.alpha = 1
            self.zonzBackground.alpha = 1
        }) { (complete) in
            
        }
    }
    @IBAction func backAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3, animations: {
                   self.unpayedView.alpha = 0
               }) { (complete) in
                self.unpayedView.isHidden = true
                self.unpayedGifImageView.stopAnimatingGIF()
                NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: self.videoLayer.player?.currentItem)
                self.videoLayer.player?.pause()
                self.videoLayer.player = nil
                self.unpayedGifImageView.layer.sublayers?.forEach({ (layer) in
                    layer.removeFromSuperlayer()
                })
                self.fullGifImageView.layer.sublayers?.forEach({ (layer) in
                    layer.removeFromSuperlayer()
                })
                self.unpayedGifImageView.image = nil
        }
    }
    @IBOutlet weak var coinsLBL: UILabel!
    
    @IBOutlet weak var backgroundCoinsImageView: UIImageView!
    
    @IBOutlet weak var backgroundBGFull: UIImageView!
    @IBOutlet weak var buyView : UIView!
    var gifUser : GifUser!
    var mimeUser : GifUser!
    @IBAction func buyGifAction(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        self.buyView.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
           
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            
            
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
            let params : Parameters = [
                "gifId" : self.gifsModel[self.indexPathToSend].id,
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
                Alamofire.request(URL(string: ScriptBase.sharedInstance.payGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                    if response.error == nil {
                        let data = JSON(response.data)
                        if data["status"].boolValue {
                            UIView.animate(withDuration: 0.2, animations: {
                                           self.unpayedGifImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                                   
                                       }) { (completeScaleOne) in
                                           self.backgroundBGFull.alpha = 0
                                           self.backgroundBGFull.isHidden = false
                                           UIView.animate(withDuration: 0.2, animations: {
                                               self.unpayedGifImageView.transform = CGAffineTransform.identity
                                               self.backgroundBGFull.alpha = 1
                                           }) { (completeScaleIdentity) in
                                               UIView.animate(withDuration: 0.4, delay: 1, options: [.curveEaseInOut], animations: {
                                                   self.backgroundBGFull.alpha = 0
                                               }) { (completeHideBackgroundFull) in
                                                   self.backgroundBGFull.isHidden = true
                                                   self.backgroundCoinsImageView.alpha = 0
                                                   self.backgroundCoinsImageView.isHidden = false
                                                   UIView.animate(withDuration: 0.4, animations: {
                                                       self.backgroundCoinsImageView.alpha = 1
                                                       self.coinsLBL.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                                                   }) { (completeBackgroundCoins) in
                                                       self.coinsLBL.text = String(Int(self.coinsLBL.text!)! - 50 )
                                                    self.zonzUserLBL.text =  self.coinsLBL.text!
                                                       UIView.animate(withDuration: 0.4, delay: 0.2, options: [.curveEaseInOut], animations: {
                                                           self.backgroundCoinsImageView.alpha = 0
                                                           self.coinsLBL.transform = CGAffineTransform.identity
                                                       }) { (completeBackgroundCoins) in
                                                           ///MARK: Play Gif
                                                           if self.isGif {
                                                           self.getGifsFromDataBase()
                                                           }else{
                                                           self.getMemesFromDataBase()
                                                           }
                                                           DispatchQueue.main.asyncAfter(deadline: .now()  + 1) {
                                                               
                                                               UIView.animate(withDuration: 0.3, animations: {
                                                                          self.unpayedView.alpha = 0
                                                                      }) { (complete) in
                                                                       self.unpayedView.isHidden = true
                                                                       self.unpayedGifImageView.stopAnimatingGIF()
                                                                       self.unpayedGifImageView.image = nil
                                                                        self.buyView.isHidden = false
                                                                       sender.isUserInteractionEnabled = true
                                                               }
                                                           }
                                                       }
                                                       
                                                   }
                                               }
                                           }
                                       }
                        }else{
                            ///Unkown error
                            let alert = UIAlertController(title: "Gif", message: data["message"].stringValue, preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default) { (alertC) in
                                UIView.animate(withDuration: 0.3, animations: {
                                           self.unpayedView.alpha = 0
                                       }) { (complete) in
                                        self.unpayedView.isHidden = true
                                        self.unpayedGifImageView.stopAnimatingGIF()
                                        self.unpayedGifImageView.image = nil
                                         self.buyView.isHidden = false
                                        sender.isUserInteractionEnabled = true
                                }
                            }
                            alert.addAction(action)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }catch{
                print(error.localizedDescription)
            }
            
            
        }
        
        
    }
    //
    //MARK: Show Full Gif
    
    @IBOutlet weak var showFullGifView: UIView!
    
    @IBOutlet weak var fullGifImageView: UIImageView!
    
    @IBOutlet weak var fullGifNameLBL: UILabel!
    
    @IBOutlet weak var backFullGifLBL: UILabel!
    
    @IBAction func backFullGifAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.showFullGifView.alpha = 0
        }) { (complete) in
            self.showFullGifView.isHidden = true
            self.fullGifImageView.stopAnimatingGIF()
            self.fullGifImageView.image = nil
        }
    }
    
    @IBAction func remakeGifAction(_ sender: UIButton) {
        
    }
    @IBAction func downloadGifAction(_ sender: UIButton) {
        if isGif {
         self.downloadShareView.alpha = 0
         self.downloadShareView.isHidden = false
        self.downloadShareLBL.text = Localization("downloadMedia")
        self.gifBTN.backgroundColor = UIColor(red: 212 / 255, green: 65 / 255, blue: 19 / 255, alpha: 1)
        self.mpFourBTN.backgroundColor = UIColor(red: 212 / 255, green: 65 / 255, blue: 19 / 255, alpha: 1)
        self.backDownloadShareBTN.setTitle(Localization("back"), for: .normal)
        UIView.animate(withDuration: 0.4, animations: {
            self.downloadShareView.alpha = 1
        }) { (complete) in
            self.isDownload = true
        }
        }else{
            
        
        SwiftSpinner.show(Localization("Downloading..."))
        PHPhotoLibrary.shared().performChanges({
               let request = PHAssetCreationRequest.forAsset()
            if self.isGif {
          
                do {
                    let data = try Data(contentsOf: URL(string:self.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".gif"))!)
                request.addResource(with: .photo, data: data, options: nil)
                }catch{
            print("unable to download the gif : ",error.localizedDescription)
                }
           
            }else{
                 
              guard let shareData: Data = self.fullGifImageView.image?.pngData() else {
                  return
              }
                request.addResource(with: .photo, data: shareData, options: nil)
            }
           }) { (success, error) in
               if let error = error {
                
                print("Gif can't be saved : ",error.localizedDescription)
                   //completion(.failure(error))
                 DispatchQueue.main.async {
                SwiftSpinner.show(duration: 2.0, title: Localization("Not_saved"))
                }
               } else {
                  print("Gif saved")
                DispatchQueue.main.async {
                     SwiftSpinner.show(duration: 2.0, title: Localization("Saved"))
                }
               
               
                  // completion(.success(true))
               }
           }
       }
    }
    func downloadImageFrom(urlString:String, completion:@escaping(Data?)->()) {
        guard let url = URL(string:urlString) else { return }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { (data, _, err) in
            if err != nil {
                // handle error if any
            }
            // you should check the reponse status
            // if data is a json object/dictionary so decode it
            // if data is regular data then pass it to your callback
            completion(data)
        }.resume()
    }
    @IBAction func shareGifAction(_ sender: UIButton) {
        if isGif {
            self.downloadShareView.alpha = 0
             self.downloadShareView.isHidden = false
            self.downloadShareLBL.text = Localization("shareMedia")
            self.gifBTN.backgroundColor = UIColor(red: 0, green: 163 / 255, blue: 217 / 255, alpha: 1)
            self.mpFourBTN.backgroundColor = UIColor(red: 0, green: 163 / 255, blue: 217 / 255, alpha: 1)
            self.backDownloadShareBTN.setTitle(Localization("back"), for: .normal)
            UIView.animate(withDuration: 0.4, animations: {
                self.downloadShareView.alpha = 1
            }) { (complete) in
                self.isDownload = false
            }
        /*SwiftSpinner.show(Localization("Downloading..."))
          
          DispatchQueue.global(qos: .background).async { [weak self] in
                 guard let strongSelf = self else { return } // to avoid reference cycle
            strongSelf.downloadImageFrom(urlString: strongSelf.globalURlVideo.replacingOccurrences(of: ".mp4", with: ".gif"), completion: { (data) in
                     if let _data = data {
                         // now you have the data
                        let shareData: Data = _data
                                        
                              let firstActivityItem: Array = [shareData]
                              let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)
                                  
                         DispatchQueue.main.async {
                             // display your imageView with the data
                            SwiftSpinner.hide()
                            strongSelf.present(activityViewController, animated: true, completion: nil)
                         }
                     }
                 })
             } */
        }else{
            guard let shareData: Data = self.fullGifImageView.image?.pngData() else {
                return
            }
            //let shareData: Data = self.fullGifImageView.image!.imageData!
         
               let firstActivityItem: Array = [shareData]
               let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)
                   self.present(activityViewController, animated: true, completion: nil)
               }
            
    }
    @IBOutlet weak var favoriteBTN: UIButton!
    @IBAction func favoriteGifAction(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        if sender.image(for: .normal) == UIImage(named: "favoriteProfile"){
            
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
        let a = try JSON(data: dataFromString!)
            if self.isGif {
            let params : Parameters = [
                "userId" : a["_id"].stringValue,
                "gifId" : self.gifUser.id
            ]
            let headers : HTTPHeaders = [
                 "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            //print(params)
            Alamofire.request(URL(string: ScriptBase.sharedInstance.addLikeGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                if response.error == nil {
                    let data = JSON(response.data)
                    if data["status"].exists() {
                        if data["status"].boolValue {
                            self.getGifsFromDataBase()
                            sender.setImage(UIImage(named: "favoriteFull"), for: .normal)
                        }else{
                          
                        }
                    }
                      sender.isUserInteractionEnabled = true
                }else{
                    sender.isUserInteractionEnabled = true
                }
            }
            }else{
                let params : Parameters = [
                            "userId" : a["_id"].stringValue,
                            "mimeId" : self.gifUser.id
                        ]
                        let headers : HTTPHeaders = [
                             "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        ]
                       // print(params)
                        Alamofire.request(URL(string: ScriptBase.sharedInstance.addLikeMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                            if response.error == nil {
                                let data = JSON(response.data)
                                if data["status"].exists() {
                                    if data["status"].boolValue {
                                        self.getMemesFromDataBase()
                                        sender.setImage(UIImage(named: "favoriteFull"), for: .normal)
                                    }else{
                                      
                                    }
                                }
                                  sender.isUserInteractionEnabled = true
                            }else{
                                sender.isUserInteractionEnabled = true
                            }
                        }
            }
        }catch {
                    
        }
        }else{
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                if self.isGif {
            let a = try JSON(data: dataFromString!)
                let params : Parameters = [
                    "userId" : a["_id"].stringValue,
                    "gifId" : self.gifUser.id
                ]
                let headers : HTTPHeaders = [
                     "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                Alamofire.request(URL(string: ScriptBase.sharedInstance.removeLikeGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                    if response.error == nil {
                        let data = JSON(response.data)
                        if data["status"].exists() {
                            if data["status"].boolValue {
                               sender.isUserInteractionEnabled = true
                                sender.setImage(UIImage(named: "favoriteProfile"), for: .normal)
                                self.getGifsFromDataBase()
                            }else{
                                
                            }
                        }
                    }else{
                        
                    }
                }
                }else{
                    let a = try JSON(data: dataFromString!)
                                   let params : Parameters = [
                                       "userId" : a["_id"].stringValue,
                                       "mimeId" : self.gifUser.id
                                   ]
                                   let headers : HTTPHeaders = [
                                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                   ]
                                   Alamofire.request(URL(string: ScriptBase.sharedInstance.removeLikeMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                       if response.error == nil {
                                           let data = JSON(response.data)
                                           if data["status"].exists() {
                                               if data["status"].boolValue {
                                                  sender.isUserInteractionEnabled = true
                                                   sender.setImage(UIImage(named: "favoriteProfile"), for: .normal)
                                                   self.getMemesFromDataBase()
                                               }else{
                                                   
                                               }
                                           }
                                       }else{
                                           
                                       }
                                   }
                }
            }catch {
                        
            }
        }
    }
    //
    //MARK: Meme Collection
    @IBOutlet weak var collectionMeme: UICollectionView!
    
    @IBOutlet weak var collectionMemeViewLayout: SwiftyGiphyGridLayout!
    //
    
    @IBOutlet weak var userProfileImageView: ExtensionProfile!
    
    @IBOutlet weak var firstNameLBL: UILabel!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    var isGif = true
    fileprivate var buffer:NSMutableData = NSMutableData()
    func pausePlayeVideos(who:String){
        if who == "One"{
            ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(collectionView: self.collection)
                  
        }else{
             ASVideoPlayerControllerOwned.sharedVideoPlayer.pausePlayeVideosFor(collectionView: self.collectionMyGifs)
        }
       
          
       }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos(who: "One")
        pausePlayeVideos(who: "Two")
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       // print("didEndDecelerating : ", scrollView.frame.origin.y)
        if scrollView.frame.origin.y <= (self.view.frame.height / 2) {
               pausePlayeVideos(who: "One")
        }else{
             pausePlayeVideos(who: "Two")
        }
      
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
       // print("willDecelerate")
        if !decelerate {
             if scrollView.frame.origin.y <= (self.view.frame.height / 2) {
                          pausePlayeVideos(who: "One")
                   }else{
                        pausePlayeVideos(who: "Two")
                   }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collection {
           
            if gifsModel.count != 0 {
                       //self.defaultLBL.isHidden = true
                      
                   return gifsModel.count
                   }else{
                       //self.defaultLBL.isHidden = false
                     return 0
                   }
            
              
        }else if collectionView != self.collectionMeme {
           
            if myGifsModel.count != 0 {
                    //self.defaultLBL.isHidden = true
                   
                return myGifsModel.count
                }else{
                    //self.defaultLBL.isHidden = false
                     return 0
                }
            
           
        }else{
            return self.myMimesModel.count
        }
    }
     var videoLayer: AVPlayerLayer!
    var globalURlVideo : String = ""
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collection {
     if let videoCell = collectionView.cellForItem(at: indexPath) as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
         self.unpayedView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.unpayedView.alpha = 1
        }) { (complete) in
            
        //self.unpayedGifImageView.sd_cacheFLAnimatedImage = false
       // self.unpayedGifImageView.contentMode = .scaleAspectFill
       // self.unpayedGifImageView.sd_setShowActivityIndicatorView(true)
       // self.unpayedGifImageView.sd_setIndicatorStyle(.gray)
       // self.unpayedGifImageView.image = cell.imageViewGif.image
          
        self.videoLayer = AVPlayerLayer(player: AVPlayer(url: URL(string: videoCell.videoURL!)!))
            self.globalURlVideo = videoCell.videoURL!
            self.videoLayer.videoGravity = .resize
                       
            self.videoLayer.frame = self.unpayedGifImageView.bounds
            self.unpayedGifImageView.layer.addSublayer(self.videoLayer)
            self.unpayedGifNameLBL.text = self.gifsModel[indexPath.row].gifId?.gifName
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoLayer.player?.currentItem, queue: .main) { [weak self] _ in
                self?.videoLayer.player?.seek(to: CMTime.zero)
                self?.videoLayer.player?.play()
            }
            self.videoLayer.player?.play()
        }
       
            }
            
        }else if collectionView == self.collectionMyGifs {
        if let videoCell = collectionView.cellForItem(at: indexPath) as? ASAutoPlayVideoLayerContainerOwned, let _ = videoCell.videoURL {
    self.showFullGifView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
        self.showFullGifView.alpha = 1
        }) { (complete) in
            
            
           // self.fullGifImageView.image = cell.imageViewGif.image
            self.videoLayer = AVPlayerLayer(player: AVPlayer(url: URL(string: videoCell.videoURL!)!))
            self.globalURlVideo = videoCell.videoURL!
            self.videoLayer.frame = self.fullGifImageView.bounds
             self.videoLayer.videoGravity = .resize
            self.fullGifImageView.layer.addSublayer(self.videoLayer)
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoLayer.player?.currentItem, queue: .main) { [weak self] _ in
                           self?.videoLayer.player?.seek(to: CMTime.zero)
                           self?.videoLayer.player?.play()
                    
                       }
                       self.videoLayer.player?.play()
            self.fullGifNameLBL.text = self.myGifsModel[indexPath.row].gifId?.gifName
            self.gifUser = self.myGifsModel[indexPath.row]
            self.favoriteBTN.setImage(self.myGifsModel[indexPath.row].favorite ? UIImage(named: "favoriteFull") : UIImage(named: "favoriteProfile"), for: .normal)
            
            let urlGif = URL(string: videoCell.videoURL!.replacingOccurrences(of: ".mp4", with: ".gif"))!
            let urlVideo = URL(string: videoCell.videoURL!)!
            self.getDownloadSize(url: urlGif) { (size, error) in
                if error != nil {
                    print("An error occurred when retrieving the download size: \(error?.localizedDescription ?? "no value")")
                    
                }else{
                    DispatchQueue.main.async {
                        
                        
                    self.gifBTN.setTitle("GIF : \(ByteCountFormatter().string(fromByteCount: size))", for: .normal)
                          }
                }
            }
            self.getDownloadSize(url: urlVideo) { (size, error) in
               if error != nil {
                print("An error occurred when retrieving the download size: \(error?.localizedDescription ?? "no value")")

                }else{
                DispatchQueue.main.async {
                    self.mpFourBTN.setTitle("MP4 : \(ByteCountFormatter().string(fromByteCount: size))", for: .normal)
                }
                }
            }
            }
        }
        }else{
            //meme
            if let cell = collectionView.cellForItem(at: indexPath) as? GiphyCollectionViewCell {
                self.view.bringSubviewToFront(self.showFullGifView)
                self.showFullGifView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                self.showFullGifView.alpha = 1
                }) { (complete) in
            
            self.fullGifImageView.image = cell.imageView.image
              
            self.fullGifNameLBL.text = self.myMimesModel[indexPath.row].gifId?.gifName
                       self.gifUser = self.myMimesModel[indexPath.row]
                       self.favoriteBTN.setImage(self.myMimesModel[indexPath.row].favorite ? UIImage(named: "favoriteFull") : UIImage(named: "favoriteProfile"), for: .normal)
                    self.fullGifImageView.setImage(with: cell.gifUrl)
                      /* self.fullGifImageView.sd_setImage(with: cell.gifUrl) { (image, error, cacheType, url) in
                           print(error?.localizedDescription)
                           print(url?.absoluteString)
                        self.fullGifImageView.image = image
                           //self.gif.frame = CGRect(x: 0, y: 167, width: self.viewSource.frame.width, height: 200)
                           //self.watermarketToShow.frame = CGRect(x: 0, y: 167, width: self.viewSource.frame.width, height: 200)
                           self.fullGifImageView.startAnimatingGIF()
                           /* UIView.animate(withDuration: 0.3, animations: {
                                                        self.gif.layoutIfNeeded()
                                                   self.watermarketToShow.layoutIfNeeded()
                                               }) { (complete) in
                                                   print("ok")
                                                   self.gif.startAnimatingGIF()
                           } */
                           } */
                       }
            }
            }
    }
    func getDownloadSize(url: URL, completion: @escaping (Int64, Error?) -> Void) {
        let timeoutInterval = 5.0
        var request = URLRequest(url: url,
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: timeoutInterval)
        request.httpMethod = "HEAD"
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            let contentLength = response?.expectedContentLength ?? NSURLSessionTransferSizeUnknown
            completion(contentLength, error)
        }.resume()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collection {
            
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? OwnGifTableViewCell {
            if isGif {
            var urlGifTemp = gifsModel[indexPath.row].url
                   urlGifTemp.removeLast()
                   if urlGifTemp.starts(with: "192.168.1.25") {
                       urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                   }
            //cell.viewSource = self.view
                   cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + urlGifTemp)!)
               return cell
            }else{
                guard var urlGifTemp = MimesModel[safe: indexPath.row]?.url else {
                    return cell
                }
                                  urlGifTemp.removeLast()
                                  if urlGifTemp.starts(with: "192.168.1.25") {
                                      urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                                  }
                         //  cell.viewSource = self.view
                                  cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + urlGifTemp + ".jpg")!)
                              return cell
            }
               }
               return UICollectionViewCell()
        }else if collectionView == self.collectionMyGifs{
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellGifs", for: indexPath) as? OwnGifPayedTableViewCell {
             
                if isGif {
                var urlGifTemp = myGifsModel[indexPath.row].url
                urlGifTemp.removeLast()
                if urlGifTemp.starts(with: "192.168.1.25") {
                    urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                }
                cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + urlGifTemp )!)
                    
            return cell
                }else{
                    var urlGifTemp = myMimesModel[indexPath.row].url
                                   urlGifTemp.removeLast()
                                   if urlGifTemp.starts(with: "192.168.1.25") {
                                       urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                                   }
                                   cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + urlGifTemp + ".jpg")!)
                                       
                               return cell
                }
            
            }
           
        }else if collectionView == self.collectionGifs{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwiftyGiphyCollectionViewCell", for: indexPath) as? GiphyCollectionViewCell {
        
                    var urlGifTemp = myGifsModel[indexPath.row].url
                    urlGifTemp.removeLast()
                    if urlGifTemp.starts(with: "192.168.1.25") {
                        urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                    }
                    cell.configureForOwnGifs(url: URL(string: ScriptBase.Image_URL + urlGifTemp + ".mp4")!)
               
                
                return cell
                
        }
        }else if collectionView == self.collectionMeme{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwiftyGiphyCollectionViewCell", for: indexPath) as? GiphyCollectionViewCell {
            var urlGifTemp = myMimesModel[indexPath.row].url
                                  urlGifTemp.removeLast()
                              if urlGifTemp.starts(with: "192.168.1.25") {
                                 urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                                                                }
            cell.configureForOwnGifs(url: URL(string: ScriptBase.Image_URL + urlGifTemp + ".jpg")!)
                return cell
            }
        }
         return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       /* if collectionView == self.collectionMyGifs {
            if isGif {
            if let cell = cell as? OwnGifPayedTableViewCell {
                if cell.gifIsReady {
                    cell.gifIsDisplayed = true
                    cell.startAnimating()
                }
            }
            }
        } */
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.collection {
            if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
                ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
            }
           /* if isGif  {
            if let cell = cell as? OwnGifPayedTableViewCell {
              
                    cell.gifIsDisplayed = false
                    cell.stopAnimating()
                
            }
            } */
        }
        if collectionView == self.collectionMyGifs {
            if let videoCell = cell as? ASAutoPlayVideoLayerContainerOwned, let _ = videoCell.videoURL {
                           ASVideoPlayerControllerOwned.sharedVideoPlayer.removeLayerFor(cell: videoCell)
                       }
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                          sessionDataTask.forEach{ $0.cancel()}
                          uploadData.forEach{ $0.cancel()}

                          downloadData.forEach{ $0.cancel()}

                      }
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk(onCompletion: nil)
    }

    @IBOutlet weak var collection:UICollectionViewWithReloadCompletion!
    
    @IBOutlet weak var collectionGifs: UICollectionViewWithReloadCompletion!
    @IBOutlet weak var collectionMyGifs : UICollectionViewWithReloadCompletion!
   
    
    @IBOutlet weak var collectionViewGifsLayout: SwiftyGiphyGridLayout!
    @IBOutlet weak var MycollectionViewLayout : SwiftyGiphyGridLayout!
    @IBOutlet weak var defaultLBL : UILabel!
    @IBOutlet weak var pendingLBL : UILabel!
    @IBOutlet weak var myLBL: UILabel!
    ///Selector code
    
    @IBOutlet weak var MemesContainer: UIView!
    
    @IBOutlet weak var GifsContainer: UIView!
    
    var selectorContainer: UIView!
    
    @IBOutlet weak var gifsLabel: UILabel!
    var oneTimeWork = true
    @IBOutlet weak var memesLabel: UILabel!
    @IBOutlet weak var profilePicture : ExtensionProfile!
    
    //Show All
    var willDispose = false
    @IBOutlet weak var viewShowMore: UIView!
    
    @IBOutlet weak var showLessLBL: UILabel!
    
    
    @IBAction func showMoreAction(_ sender: UITapGestureRecognizer) {
        self.viewShowMore.alpha = 0
        self.viewShowMore.isHidden = false
        self.willDispose = false
        self.collectionGifs.reloadData()
        UIView.animate(withDuration: 0.5, animations: {
            self.viewShowMore.alpha = 1
        }) { (complete) in
            
        }
    }
    
    @IBAction func showLessAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            self.viewShowMore.alpha = 0
        }) { (complete) in
            self.willDispose = true
               self.viewShowMore.isHidden = true
            self.collectionGifs.reloadData()
           
        }
    }
    //
    override func viewDidLayoutSubviews() {
        if oneTimeWork {
            self.oneTimeWork = false
         selectorContainer.frame = self.GifsContainer.frame
        self.view.bringSubviewToFront(self.GifsContainer)
        self.view.bringSubviewToFront(self.gifsLabel)
        self.view.bringSubviewToFront(self.MemesContainer)
        self.view.bringSubviewToFront(self.memesLabel)
        self.view.bringSubviewToFront(self.viewShowMore)
        self.view.bringSubviewToFront(self.unpayedView)
        self.view.bringSubviewToFront(self.showFullGifView)
        self.view.bringSubviewToFront(self.collectionMeme)
        self.view.bringSubviewToFront(self.zonzBackground)
        self.view.bringSubviewToFront(self.zonzContainer)
        self.view.bringSubviewToFront(self.settingsBackgroundView)
        self.view.bringSubviewToFront(self.settingsView)
        }
    }
   
    func initSelector(){
        memesLabel.font = UIFont.init(name: "SegoeUI-Regular", size: 18)
        memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)
        gifsLabel.font = UIFont.init(name: "SegoeUI-Bold", size: 18)
        gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
        selectorContainer = UIView(frame: self.GifsContainer.frame)
        selectorContainer.backgroundColor = .white
        self.view.addSubview(selectorContainer)
       
        //selectorContainer.layoutIfNeeded()
        let tapgestureGifs = UITapGestureRecognizer(target: self, action: #selector(self.goToGifs))
        let tapgestureMimes = UITapGestureRecognizer(target: self, action: #selector(self.goToMimes))
        MemesContainer.isUserInteractionEnabled = true
        MemesContainer.addGestureRecognizer(tapgestureMimes)
        GifsContainer.isUserInteractionEnabled = true
        GifsContainer.addGestureRecognizer(tapgestureGifs)
    }
    @objc func goToMimes(){
        isGif = false
        self.pendingLBL.text = Localization("MyMimes")
        self.myLBL.text =  Localization("MyMimes")
        UIView.performWithoutAnimation {
        self.gifsLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
        self.memesLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
        }
        self.collectionMeme.alpha = 0
        self.collectionMeme.isHidden = false
        UIView.animate(withDuration: 0.2, animations: {
            
            self.gifsLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

            self.memesLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
          self.selectorContainer.transform = CGAffineTransform(translationX: (self.MemesContainer.frame.origin.x), y: 0)
            self.collectionMeme.alpha = 1
        
         }, completion: nil)
        self.collectionMeme.reloadData()
        
    }
    @objc func goToGifs(){
      isGif = true
        self.pendingLBL.text = "Pending Gifs"
        self.myLBL.text = "My Gifs"
        UIView.performWithoutAnimation {
            self.memesLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
            self.gifsLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
        }
        UIView.animate(withDuration: 0.2, animations: {
             self.collectionMeme.alpha = 0
                self.memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

                self.gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
                self.selectorContainer.transform = CGAffineTransform.identity
              // self.selectorContainer.layoutIfNeeded()
               }, completion: {complete in
                
                 self.collectionMeme.isHidden = true
        })
        
        self.collection.reloadData()
        self.collectionMyGifs.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {   self.collection.delegate!.scrollViewDidEndDecelerating?(UIScrollView())
                               self.collection.isScrollEnabled = false
                               self.collection.isScrollEnabled = true
                               
                           }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {   self.collectionMyGifs.delegate!.scrollViewDidEndDecelerating?(UIScrollView())
                                     self.collectionMyGifs.isScrollEnabled = false
                                     self.collectionMyGifs.isScrollEnabled = true
                                     
                                 }
        
    }
    /// end selector
    var selectedAssets = [TLPHAsset]()
    @IBAction func pickerButtonTap(){
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        var configure = TLPhotosPickerConfigure()
        configure.cancelTitle = Localization("Cancel")
        configure.doneTitle = Localization("Done")
        configure.emptyMessage = Localization("NoAlbums")
        configure.allowedVideo = false
        configure.allowedVideoRecording = false
        configure.mediaType = .image
        configure.singleSelectedMode = true
        configure.maxSelectedAssets = 1
        configure.cameraBgColor = .black
        configure.supportedInterfaceOrientations = .portrait
        //configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main) // If you want use your custom cell..
        viewController.configure = configure
        self.present(viewController, animated: true, completion: nil)
    }
    
    var gifsModel : [GifUser] = []
    var MimesModel : [GifUser] = []

    var myGifsModel : [GifUser] = []
     var myMimesModel : [GifUser] = []
    
    @IBOutlet weak var WelcomeLBL: UILabel!
    @IBOutlet weak var userFullName : UILabel!
    @objc func configureViewFromLocalisation(){
           WelcomeLBL.text = Localization("Welcome")
        if isGif == false {
            self.pendingLBL.text = Localization("PendingM")
            self.myLBL.text =  Localization("MyMimes")
        }else{
            self.pendingLBL.text = "Pending Gifs"
            self.myLBL.text = "My Gifs"
        }
       }
       @objc func receiveLanguageChangedNotification(notification:NSNotification) {
           if notification.name == kNotificationLanguageChanged {
               configureViewFromLocalisation()
           }
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        initSelector()
        initSettingsViews()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        collection.delegate = self
        collection.dataSource = self
        
     
        collectionMyGifs.delegate = self
        collectionMyGifs.dataSource = self
       
        
        collectionGifs.delegate = self
        collectionGifs.dataSource = self
        //collectionGifs.register(GiphyCollectionViewCell.self, forCellWithReuseIdentifier: "SwiftyGiphyCollectionViewCell")
        
     
        collectionMeme.delegate = self
        collectionMeme.dataSource = self
         ///Temporray
        
        //collectionViewLayout.delegate = self
        //MycollectionViewLayout.delegate = self
       // collectionViewGifsLayout.delegate = self
        collectionMemeViewLayout.delegate = self
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.appEnteredFromBackground),
        name: UIApplication.willEnterForegroundNotification, object: nil)
     
    }
    @objc func appEnteredFromBackground() {
          
           ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(collectionView: collection, appEnteredFromBackground: true)
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(collectionView: collectionMyGifs, appEnteredFromBackground: true)
       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUser { (valid) in
            if UserDefaults.standard.value(forKey: "UserZonzay") != nil {
                 let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                 let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                     do {
                     let a =  try JSON(data: dataFromString!)
                         self.zonzUserLBL.text = a["zonz"].stringValue
                         self.coinsLBL.text = a["zonz"].stringValue
                         self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + a["userImageURL"].stringValue), placeholder: UIImage(named: "artist"))
                         self.userProfileImageView.setImage(with: URL(string: ScriptBase.Image_URL + a["userImageURL"].stringValue), placeholder: UIImage(named: "artist"))
                         let name = a["userFirstName"].stringValue
                         if name.contains(" ") {
                             
                             self.firstNameLBL.text = name.split(separator: " ")[0] + "\n" + name.split(separator: " ")[1]
                         }else{
                             self.firstNameLBL.text = a["userFirstName"].stringValue + "\n" + a["userLastName"].stringValue
                         }
                     }catch{
                         print("TemporaryProfile : ",error.localizedDescription)
                     }
                     
                 }
        }
       getGifsFromDataBase()
       getMemesFromDataBase()
    }
    func performTouchInView() {
        let touches : Set<UITouch> = []
        self.view.touchesBegan(touches, with: UIEvent())
        self.view.touchesEnded(touches, with: UIEvent())
        
    }
    func getGifsFromDataBase(){
        self.myGifsModel = []
        self.gifsModel = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue
        ]
    let headers : HTTPHeaders = [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue ]
        print(settings)
        print(headers)
                            
            Alamofire.request(ScriptBase.sharedInstance.myGifs, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { (result) in
                if result.response?.statusCode == 200 {
                    
                    let data = JSON(result.data)
                    print("data Gifs : ", data)
                   
                    if data.arrayObject != nil {
                        for i in 0...(data.arrayObject!.count - 1) {
                            let gif = GifUser(id: data[i]["_id"].stringValue, gifId: Gif(id: data[i]["gifId"]["_id"].stringValue, gifName: data[i]["gifId"]["gifName"].stringValue, giphyId: data[i]["gifId"]["giphyId"].stringValue, priceGif: data[i]["gifId"]["priceGif"].stringValue, gifUrl: data[i]["gifId"]["gifUrl"].stringValue, gifHeight: data[i]["gifId"]["gifHeight"].stringValue, gifWidth: data[i]["gifId"]["gifWidth"].stringValue), gifDate: data[i]["gifDate"].stringValue, gifPaid: data[i]["gifPaid"].boolValue, url: data[i]["url"].stringValue, favorite: data[i]["favorite"].boolValue)
                            if data[i]["gifPaid"].boolValue {
                                
                                self.myGifsModel.append(gif)
                              

                            }else{
                                  self.gifsModel.append(gif)
                                 //self.myGifsModel.append(gif)
                            }
                        }
                    }
                    self.collection.reloadDataWithCompletion {
                       // print("reloaded")
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {   self.collection.delegate!.scrollViewDidEndDecelerating?(UIScrollView())
                        self.collection.isScrollEnabled = false
                        self.collection.isScrollEnabled = true
                        self.performTouchInView()
                        /*let pointOnTheScreen = CGPoint(x: 0, y: 0   )
                        self.view.hitTest(pointOnTheScreen, with: nil)
                        self.view.subviews[0].overla
                        self.view.overlapHitTest(pointOnTheScreen, withEvent: nil) */
                      //  print("tap on screen")
                        
                    }
                 
                  /*  self.collection.performBatchUpdates({
                       // self.collection.reloadData()
                    }) { (complete) in
                         print("called selectItem")
                    } */
                  /*  self.collection.reloadDataWithCompletion {
                        self.collection.scrollToPreviousItem()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.collection.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
                            print("called selectItem")
                            if self.collection.visibleCells.count != 0 {
                                //self.collection.hitTest(self.collection.visibleCells[0]., with: <#T##UIEvent?#>)
                            
                            }
                            self.pausePlayeVideos()
                        }
                        
                    } */
                    self.collectionMyGifs.reloadDataWithCompletion {
                      
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {   self.collectionMyGifs.delegate!.scrollViewDidEndDecelerating?(UIScrollView())
                        self.collectionMyGifs.isScrollEnabled = false
                        self.collectionMyGifs.isScrollEnabled = true
                      
                        /*let pointOnTheScreen = CGPoint(x: 0, y: 0   )
                        self.view.hitTest(pointOnTheScreen, with: nil)
                        self.view.subviews[0].overla
                        self.view.overlapHitTest(pointOnTheScreen, withEvent: nil) */
                       // print("tap on screen")
                        
                    }
                    //self.collection.scrollToNextItem()
                    //self.collectionMyGifs.scrollToNextItem()
                    
                    
                }else{
                     let data = JSON(result.data)
                    print("data : ",data)
                    if data["message"].exists() {
                        if data["message"].stringValue == "Gifs is empty"{
                            self.collection.isHidden = true
                            //self.defaultLBL.isHidden = false
                        }
                    }else{
                    /// != 200
                    
                       /* let alert = UIAlertController(title: "Profile", message: "We can't communicate with the server, please try again", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Retry", style: .default, handler: { (ok) in
                            self.getGifsFromDataBase()
                            self.getMemesFromDataBase()
                            
                        })
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil) */
                        self.getGifsFromDataBase()
                        self.getMemesFromDataBase()
                    }
                    
                }
            }
        }catch{
            print("TemporaryProfile getGifsFromDataBase : ",error.localizedDescription)
        }
    }
    func getMemesFromDataBase(){
        self.myMimesModel = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue
        ]
    let headers : HTTPHeaders = [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue ]
        print(settings)
        print(headers)
                            
            Alamofire.request(ScriptBase.sharedInstance.myMimes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { (result) in
                if result.response?.statusCode == 200 {
                    
                    let data = JSON(result.data)
                    print("data Memes : ", data)
                   
                    if data.arrayObject != nil {
                        for i in 0...(data.arrayObject!.count - 1) {
                            let gif = GifUser(id: data[i]["_id"].stringValue, gifId: Gif(id: data[i]["mimeId"]["_id"].stringValue, gifName: data[i]["mimeId"]["mimeName"].stringValue, giphyId: data[i]["mimeId"]["giphyId"].stringValue, priceGif: data[i]["mimeId"]["priceMime"].stringValue, gifUrl: data[i]["mimeId"]["mimeUrl"].stringValue, gifHeight: data[i]["mimeId"]["gifHeight"].stringValue, gifWidth: data[i]["mimeId"]["gifWidth"].stringValue), gifDate: data[i]["mimeDate"].stringValue, gifPaid: data[i]["mimePaid"].boolValue, url: data[i]["url"].stringValue, favorite: data[i]["favorite"].boolValue)
                            self.myMimesModel.append(gif)
                            
                        }
                    }
                    self.collectionMeme.reloadData()
                    
                }else{
                     let data = JSON(result.data)
                    print("data : ",data)
                    if data["message"].exists() {
                        if data["message"].stringValue == "Gifs is empty"{
                            self.collectionMeme.alpha  = 0
                            //self.defaultLBL.isHidden = false
                        }
                    }else{
                    /// != 200
                    
                       /* let alert = UIAlertController(title: "Profile", message: "We can't communicate with the server, please try again", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil) */
                        self.getGifsFromDataBase()
                        self.getMemesFromDataBase()
                    }
                    
                }
            }
        }catch{
            print("TemporaryProfile getGifsFromDataBase : ",error.localizedDescription)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension TemporaryProfile : SwiftyGiphyGridLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        
        if collectionView == self.collectionMeme {
            print(self.myMimesModel)
                      print(self.myMimesModel.count)
                   
                      print(indexPath.row)
                      guard let imageSet = self.myMimesModel[indexPath.row].gifId else {
                           return 0.0
                      }
                      guard let width = NumberFormatter().number(from: imageSet.gifWidth) else {  return 0.0 }
                      guard let height = NumberFormatter().number(from: imageSet.gifHeight) else {  return 0.0 }
                      print(width)
                      print(height)
                      return AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }else{
            return 132
        }
      /*  if collectionView != self.collectionGifs && collectionView != self.collectionMeme {
        return AVMakeRect(aspectRatio: CGSize(width: 119 , height: 87), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }else if collectionView == self.collectionMyGifs || collectionView == self.collectionGifs && collectionView != self.collectionMeme{
            guard let imageSet = self.myGifsModel[indexPath.row].gifId else {
                 return 0.0
            }
            guard let width = NumberFormatter().number(from: imageSet.gifWidth) else {  return 0.0 }
            guard let height = NumberFormatter().number(from: imageSet.gifHeight) else {  return 0.0 }
            return AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }else if self.collectionMeme == collectionView{
               print(self.myMimesModel)
            print(self.myMimesModel.count)
         
            print(indexPath.row)
            guard let imageSet = self.myMimesModel[indexPath.row].gifId else {
                 return 0.0
            }
            guard let width = NumberFormatter().number(from: imageSet.gifWidth) else {  return 0.0 }
            guard let height = NumberFormatter().number(from: imageSet.gifHeight) else {  return 0.0 }
            print(width)
            print(height)
            return AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        } */
       // return 0.0
    }
  
}
extension TemporaryProfile : TLPhotosPickerViewControllerDelegate {
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        print(withTLPHAssets)
        if withTLPHAssets.count != 0 {
        self.selectedAssets = withTLPHAssets
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if let image = withTLPHAssets[0].fullResolutionImage {
                let img = self.fixOrientation(img: image)
            self.profilePicture.image = img
          self.process(image: img) { (resultCode) in
        //let openCvWrapper = OpenCVWrapper()
        //let resultCode = openCvWrapper.isThisWorking(img)
            print("ResultCode : ",resultCode)
        if resultCode == 0 {
        let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
            return
                                         
        }else if resultCode == 2 {
        let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceMore"), preferredStyle: .alert)
        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        }else if resultCode == 3{
        let alert = UIAlertController(title: Localization("Photo"), message: "the face is not proprely recognized or it is not in a portrait mode", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            print("Upload Server")
        self.uploadToServer()

        }
                }
            }
        }
        }
                 
    }
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        print(withPHAssets)
    }
    func photoPickerDidCancel() {
        print("DID cancel photo")
    }
    func dismissComplete() {
         // picker viewcontroller dismiss completion
    }
    func canSelectAsset(phAsset: PHAsset) -> Bool {
        if phAsset.mediaType == .audio || phAsset.mediaType == .video || phAsset.mediaType == .unknown {
            return false
        }
        return true
    }
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        // exceed max selection
    }
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) {
        // handle denied albums permissions case
        picker.dismiss(animated: true) {
            let alert = UIAlertController(title: "", message: "Denied albums permissions granted", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) {
        // handle denied camera permissions case
        let alert = UIAlertController(title: "", message: "Denied camera permissions granted", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        picker.present(alert, animated: true, completion: nil)
    }
}
extension TemporaryProfile : URLSessionDelegate,URLSessionTaskDelegate, URLSessionDataDelegate {
    func randomStringWithLength() -> String{
           let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
           let len = 15
           let date = Date()
           let randomString : NSMutableString = NSMutableString(capacity: len)
           for _ in 0...(len - 1){
               let length = UInt32(letters.length)
               let rand = arc4random_uniform(length)
               randomString.appendFormat("%C", letters.character(at: Int(rand)))
               
           }
           var resultFinal = "ios" + (randomString as String) + date.description
           resultFinal = resultFinal.replacingOccurrences(of: " ", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: ":", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: "+", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: "-", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: ".", with: "")
           return resultFinal
       }
       func uploadToServer(){
           // SwiftSpinner.show("Uploading Picture...")
           let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
           let contentType = "multipart/form-data; boundary=" + boundaryConstant
           
           let mimeType = "image/jpeg"
           
           
           let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
           //?.rotate(radians: Float( -(Double.pi / 2)))
        let image = self.profilePicture.image
           let fileData : Data? = image!.jpegData(compressionQuality: 1)
           let requestBodyData : NSMutableData = NSMutableData()
           requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
           
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
               let a = try JSON(data: dataFromString!)
               let key = "userId"
               requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
               requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
           }catch{
               
           }
           requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
           do {
               let a = try JSON(data: dataFromString!)
               let fieldName = "picture"
               let filename = "zonz" + self.randomStringWithLength() + ".jpg"
               requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
           }catch{
               
           }
           requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
           
           //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
           requestBodyData.append(fileData!)
           // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
           requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
           requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
           var request = URLRequest(url: uploadScriptUrl!)
           
           
           request.httpMethod = "POST"
           request.httpBody = requestBodyData as Data
           request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
           request.setValue(contentType, forHTTPHeaderField: "Content-Type")
           let config = URLSessionConfiguration.default
           let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
           let task = session.uploadTask(withStreamedRequest: request)
           task.resume()
       }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if (error != nil ) {
            SwiftSpinner.show("Unexpected Error", animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                SwiftSpinner.hide()
            }
        }else{
                let q = JSON(buffer)
             SwiftSpinner.hide()
            if q["error"].exists() == false {
                do {
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = try JSON(data: dataFromString!)
                    a["userImageURL"].stringValue = q["userImageURL"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    MapleBacon.shared.cache.clearDisk()
                    MapleBacon.shared.cache.clearMemory()
                }catch{
                    
                }
            }
        }
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
      
        SwiftSpinner.show(progress: Double(uploadProgress), title: "\(Int(uploadProgress * 100))% \n Uploading" )
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        completionHandler(URLSession.ResponseDisposition.allow)
    }
}

extension Collection {
  subscript(safe index: Index) -> Iterator.Element? {
    guard indices.contains(index) else { return nil }
    return self[index]
  }
}
extension String {
    public func font(named name: String,
                     toFit containerSize: CGSize,
                     noSmallerThan lowerBound: CGFloat = 1.0,
                     noLargerThan upperBound: CGFloat = 256.0) -> UIFont? {
        let lowerBound = lowerBound > upperBound ? upperBound : lowerBound
        let mid = lowerBound + (upperBound - lowerBound) / 2
        guard let tempFont = UIFont(name: name, size: mid) else { return nil }
        let difference = containerSize.height -
            self.size(withAttributes:
                [NSAttributedString.Key.font : tempFont]).height
        if mid == lowerBound || mid == upperBound {
            return UIFont(name: name, size: difference < 0 ? mid - 1 : mid)
        }
        return difference < 0 ? font(named: name,
                                     toFit: containerSize,
                                     noSmallerThan: mid,
                                     noLargerThan: mid - 1) :
            (difference > 0 ? font(named: name,
                                   toFit: containerSize,
                                   noSmallerThan: mid,
                                   noLargerThan: mid - 1) :
                UIFont(name: name, size: mid))
    }
    
    /// Returns the system font of the appropriate point size for this string
    /// to fit within a particular container size and constrained to a lower
    /// and upper bound point size.
    /// - parameter containerSize: that this string should fit inside.
    /// - parameter lowerBound: minimum allowable point size of this font.
    /// - parameter upperBound: maximum allowable point size of this font.
    /// - returns: the system font of the appropriate point size for this string
    /// to fit within a particular container size and constrained to a lower
    /// and upper bound point size.
    public func systemFont(toFit containerSize: CGSize,
                           noSmallerThan lowerBound: CGFloat = 1.0,
                           noLargerThan upperBound: CGFloat = 256.0) -> UIFont {
        let lowerBound = lowerBound > upperBound ? upperBound : lowerBound
        let mid = lowerBound + (upperBound - lowerBound) / 2
        let tempFont = UIFont.systemFont(ofSize: mid)
        let difference = containerSize.height -
            self.size(withAttributes:
                [NSAttributedString.Key.font : tempFont]).height
        if mid == lowerBound || mid == upperBound {
            return UIFont.systemFont(ofSize: difference < 0 ? mid - 1 : mid)
        }
        return difference < 0 ? systemFont(toFit: containerSize,
                                           noSmallerThan: mid,
                                           noLargerThan: mid - 1) :
            (difference > 0 ? systemFont(toFit: containerSize,
                                         noSmallerThan: mid,
                                         noLargerThan: mid - 1) :
                UIFont.systemFont(ofSize: mid))
    }
    
}
extension UIView {
    /**
     Convert UIView to UIImage
     */
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        //self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        self.draw(self.bounds)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
}
extension UIImage {
    static func createanimatedGif(from images: [UIImage]) -> URL? {
        let fileProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [kCGImagePropertyGIFLoopCount as String: 0]]  as CFDictionary
        let frameProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [(kCGImagePropertyGIFDelayTime as String): 1.0]] as CFDictionary
        
        let documentsDirectoryURL: URL? = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL: URL? = documentsDirectoryURL?.appendingPathComponent("animated.gif")
        
        if let url = fileURL as CFURL? {
            if let destination = CGImageDestinationCreateWithURL(url, kUTTypeGIF, images.count, nil) {
                CGImageDestinationSetProperties(destination, fileProperties)
                for image in images {
                    if let cgImage = image.cgImage {
                        CGImageDestinationAddImage(destination, cgImage, frameProperties)
                    }
                }
                if !CGImageDestinationFinalize(destination) {
                    print("Failed to finalize the image destination")
                    
                }
                print("Url = \(fileURL)")
                return fileURL
            }
        }
        return nil
    }
    class func imageByCombiningImage(firstImage: UIImage, withImage secondImage: UIImage) -> UIImage? {
        
        let newImageWidth  = secondImage.size.width
        let newImageHeight = secondImage.size.height
        let newImageSize = CGSize(width : newImageWidth, height: newImageHeight)
        

        UIGraphicsBeginImageContextWithOptions(newImageSize, false, 0.0)
        let firstImageDrawX  = 0
        let firstImageDrawY  = 0
        
        let secondImageDrawX = 0
        let secondImageDrawY = 0
        
        firstImage .draw(at: CGPoint(x: firstImageDrawX,  y: firstImageDrawY))
        secondImage.draw(at: CGPoint(x: secondImageDrawX, y: secondImageDrawY))
        //secondImage.draw(at: CGPoint(x: secondImageDrawX, y: secondImageDrawY), blendMode: .overlay, alpha: 0.8)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        
        UIGraphicsEndImageContext()
        
        
        return image
    }
}
extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }

}
extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }

    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }

    func moveToFrame(contentOffset : CGFloat) {
        guard contentOffset <= self.contentSize.width - self.bounds.size.width else { return }
        guard contentOffset >= 0 else { return }
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
       
    }
}
final class UICollectionViewWithReloadCompletion: UICollectionView {
  private var reloadDataCompletionBlock: (() -> Void)?

  override func layoutSubviews() {
    super.layoutSubviews()

    reloadDataCompletionBlock?()
    reloadDataCompletionBlock = nil
  }


  func reloadDataWithCompletion(completion: @escaping () -> Void) {
    reloadDataCompletionBlock = completion
    self.reloadData()
  }
}
extension UIImage {

    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
    
        self.init(data: imageData)
    }

}
