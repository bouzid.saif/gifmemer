//
//  AppDelegate.swift
//  SwiftyGiphy
//
//  Created by Brendan Lee on 3/9/17.
//  Copyright © 2017 52inc. All rights reserved.
//

import UIKit
import SwiftyGiphy
import AVKit
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit
import SwiftyJSON
import IQKeyboardManagerSwift
import OneSignal
import Alamofire
#if targetEnvironment(simulator)
#else
import SwiftyStoreKit
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate{

    var window: UIWindow?
    var timer: DispatchSourceTimer?
    var tempAccessToken = ""
    var alertWaiting : UIAlertController?
    static var countryArray : JSON = []
    var autoBlur = AutoBlurScreen()
    var autoBlurScreenShoots = AutoBlurScreen()
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

    var contient = ""
    func getLangue() -> String {
           var langue : String! = ""
           if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
            if Locale.currentLanguage! == .en {
                  _ =   SetLanguage("English_en")
                   langue = "EN"
               }else if Locale.currentLanguage! == .fr  {
                   langue = "FR"
               _ = SetLanguage("French_fr")

            }else if Locale.currentLanguage! == .es{
                 langue = "ES"
               _ = SetLanguage("Spanish_es")

            }else if Locale.currentLanguage! == .pt {
                 langue = "PT"
               _ = SetLanguage("Portugese_pg")

            }else if Locale.currentLanguage! == .it {
               _ = SetLanguage("Italien_it")
                 langue = "IT"
                }
           }
           else if Localisator.sharedInstance.currentLanguage == arrayLanguages[2]
           {
                print("Fr language")
               langue = "FR"
            _ = SetLanguage("French_fr")

           }
           else if Localisator.sharedInstance.currentLanguage == arrayLanguages[3] {
            print("Es language")

               langue = "ES"
            _ = SetLanguage("Spanish_es")
           }else if Localisator.sharedInstance.currentLanguage == arrayLanguages[4]{
            print("Pt language")

            langue = "PT"
            _ = SetLanguage("Portugese_pg")
            }else if Localisator.sharedInstance.currentLanguage == arrayLanguages[5]{
            print("It language")

            langue = "IT"
            _ = SetLanguage("Italien_it")
           }else{
            print("EN language")

            langue = "EN"
            _ = SetLanguage("English_en")
        }
           return langue
       }
    func setLanguageInput(lang: String) {
        if lang == "EN" {
           _ =   SetLanguage("English_en")
           
        }else if lang == "FR"  {
           
        _ = SetLanguage("French_fr")

        }else if lang == "ES"{
         
        _ = SetLanguage("Spanish_es")

        }else if lang == "PT" {
         
        _ = SetLanguage("Portugese_pg")

        }else if lang == "IT" {
        _ = SetLanguage("Italien_it")
         
         }
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       URLSessionConfiguration.default.multipathServiceType = .handover

       /* if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .dark
        } else {
            // Fallback on earlier versions
            
        } */
        //7bc06854-78d9-4067-97eb-367f5d139e1c
        autoBlur.blurStyle = .light
        autoBlur.isAutoBlur = true
        autoBlurScreenShoots.blurStyle = .prominent
        autoBlurScreenShoots.isAutoBlur = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.preventScreenRecording), name: UIScreen.capturedDidChangeNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.showWaiting(_:)), name: NSNotification.Name.init("VideoSended"), object: nil)
        AppDelegate.countryArray = self.getSupported()
         ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        SwiftyGiphyAPI.shared.apiKey = SwiftyGiphyAPI.publicBetaKey
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enableDebugging = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        connectRemoteNotification(launchOptions: launchOptions)
        SocketIOManager.sharedInstance.establishConnection()
       TWTRTwitter.sharedInstance().start(withConsumerKey: "Mq6MdUcOhDZPX30Zqcq4rCXnL", consumerSecret: "DcR4oJfPr82E1I5SupyBvLPRzcQNXKU8KSImjyk4xANfvg2F6B")
        SetupIAP()
        if UserDefaults.standard.value(forKey: "UserZonzay") != nil {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
               let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
               do {
                _ = SetLanguage("DeviceLanguage")
                   let a = try JSON(data: dataFromString!)
                let defaults = UserDefaults(suiteName: "group.com.regystone.gifmemer.keyboard")
                let defaultValue = ["userUID" : ""]
                defaults!.register(defaults: defaultValue)
                defaults!.set(a["_id"].stringValue, forKey: "userUID")
                defaults!.synchronize()

                if a["language"].exists() {
                    setLanguageInput(lang: a["language"].stringValue)
                }else{
                    _ = self.getLangue()
                    
                }
               }catch{
                print("AppDelegate : ",error.localizedDescription)
        }
        }
        SocketIOManager.sharedInstance.listenToVideoFinsihed { (result) in
            self.alertWaiting?.dismiss(animated: true, completion: nil)
            if result != "" {
                let alert = UIAlertController(title: "Video Finished", message: "Would you like to play it?", preferredStyle: .alert)
                var nameButton = ""
                if (String(result.filter { !" \n\t\r".contains($0) }) == "") || (String(result.filter { !" \n\t\r".contains($0) }) == "videoisnotgood") {
                    nameButton = "ok"
                }else{
                   nameButton = "play"
                }
                let action = UIAlertAction(title: "\(nameButton)", style: .default) { (alertButton) in
                    if nameButton != "ok" {
                        let avplayerItem = AVPlayerItem(url: URL(string: "http://" + String(result.filter { !" \n\t\r".contains($0) }) + ".mp4")!)
                        let avplayer = AVPlayer(playerItem: avplayerItem)
                        let diffuser = AVPlayerViewController()
                        diffuser.player = avplayer
                        self.window?.rootViewController?.present(diffuser, animated: true, completion: {
                            avplayer.play()
                        })
                    }
                }
                
                if nameButton != "ok" {
                    let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
                    alert.addAction(cancel)
                }
                alert.addAction(action)
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
        let rootViewController = HomeTabBarController.shared.getTabBar()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.isNavigationBarHidden = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        Service.default.fetch{ result, error in
            if let result = result {
                
                print("Geo IP result \(result).")
                self.contient = result.timezone!.split(separator: "/")[0].uppercased()
                print(self.contient)
            }else{
                print("Error batch : ", error )
            }
        }
        LaunchScreenManager.instance.animateAfterLaunch(window!.rootViewController!.view)
        
        return true
    }
    func SetupIAP(){
        #if targetEnvironment(simulator)
        #else
    SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
        
        #endif
    }
    @objc func preventScreenRecording() {
        let isCaptured = UIScreen.main.isCaptured

        if isCaptured {
            print("start Capturing")
            autoBlurScreenShoots.createBlurEffect()
        }else {
            print("stoped Capturing")
           autoBlurScreenShoots.removeBlurEffect()

        }
    }
    func renewRefreshToken(){
        stopTimer()
        let queue = DispatchQueue(label: "com.domain.app.timer")  // you can also use `DispatchQueue.main`, if you want
               timer = DispatchSource.makeTimerSource(queue: queue)
               timer!.schedule(deadline: .now(), repeating: .seconds(3500))
               timer!.setEventHandler { [weak self] in
                   
                   self?.renewToken()
               }
               timer!.resume()
    }
    @objc func renewToken() {
    let params : Parameters = [
        "grant_type" : "refresh",
        "client_id" : ScriptBase.client_id,
        "client_secret" : ScriptBase.client_secret
    ]
    Alamofire.request(URL(string: "https://api.gfycat.com/v1/oauth/token")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (data) in
        if data.response?.statusCode == 200 {
            let tempData = JSON(data.data)
            ScriptBase.client_token = tempData["access_token"].stringValue
            ScriptBase.refresh_token = tempData["refresh_token"].stringValue
            self.renewRefreshToken()
        }else{
            self.renewToken()
        }
    }
    }
    func stopTimer() {
           timer?.cancel()
           timer = nil
       }
    func connectRemoteNotification(launchOptions:[UIApplication.LaunchOptionsKey: Any]?){
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false , kOSSettingsKeyInAppLaunchURL: true ]
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.none
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
         UNUserNotificationCenter.current().delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                       print("User accepted notifications: \(accepted)")
                   })
        }
       
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            //  _ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            print("Received Notification: \(notification!.payload.notificationID ?? "")")
            print("launchURL = \(notification?.payload.launchURL ?? "None")")
            print("content_available = \(notification?.payload.contentAvailable ?? false)")
        }
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            //_ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            var fullMessage = payload.body
            print("Message = ",fullMessage ?? "")
            
            if payload.additionalData != nil {
                if payload.title != nil {
                    let messageTitle = payload.title
                    print("Message Title = \(messageTitle!)")
                }
                
                let additionalData = payload.additionalData
                if additionalData?["actionSelected"] != nil {
                    fullMessage = fullMessage! + "\nPressed ButtonID: " + (additionalData!["actionSelected"] as! String)
                }
            }
        }
         OneSignal.initWithLaunchOptions(launchOptions, appId: "7bc06854-78d9-4067-97eb-367f5d139e1c", handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                //self.goToNotfi(launchOptions: launchOptions)
            }
        }
       // OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard let info = userInfo as? [String : AnyObject] else {
                   return
               }
               guard let aps = info["aps"] as? NSDictionary else {
                   return
               }
               guard let Notif = info ["custom"] as? NSDictionary else {
                   return
               }
               guard let NotifFinal = Notif ["a"] as? NSDictionary else {
                   return
               }
               guard let NotifFinalType = NotifFinal ["typeNotif"] as? String else {
                   return
               }
        if NotifFinalType == "gif" {
            generateLocalNotification(userInfo: userInfo, type: .gif)
        }else if NotifFinalType == "mime" {
            generateLocalNotification(userInfo: userInfo, type: .mime)

        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func generateLocalNotification(userInfo : [AnyHashable : Any],type : NotificationType) {
            guard let info = userInfo as? [String : AnyObject] else {
                return
            }
            guard let aps = info["aps"] as? NSDictionary else {
                return
            }
            guard let Notif = info ["custom"] as? NSDictionary else {
                return
            }
            guard let NotifFinal = Notif ["a"] as? NSDictionary else {
                return
            }
            guard let NotifFinalType = NotifFinal ["typeNotif"] as? String else {
                return
            }
        let localNotification = UNMutableNotificationContent()
        localNotification.title = "GifMemer"
        if type == .gif  || type == .mime  {
            print(NotifFinal)
            localNotification.body =  (NotifFinal["gif"]! as! NSDictionary)["message\(getLangueNotif())"] as! String
        
            }else{
            localNotification.body = "didn't worked"
            }
        //localNotification.sound = UNNotificationSound(named: UNNotificationSoundName("friend_request.mp3"))
        if  type == .gif || type == .mime {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

        UIApplication.shared.applicationIconBadgeNumber  = 0

        let request = UNNotificationRequest(identifier: "Time for a run!", content: localNotification, trigger: trigger)

        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: { error in
            print("Notification created")
            if type == .gif || type == .mime{
                DispatchQueue.main.async {
                    HHTabBarView.shared.tabBarTabs[3].badgeValue += 1
                }
               
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadgeGif"), object: nil)
            }
        })
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
                
    }
    let arrayLanguagesNotif = Localisator.sharedInstance.getArrayAvailableLanguages()
    func getLangueNotif() -> String {
        var langue : String! = ""
        if Localisator.sharedInstance.currentLanguage == arrayLanguagesNotif[0] {
         if Locale.currentLanguage! == .en {
               _ =   SetLanguage("English_en")
                langue = "EN"
            }else if Locale.currentLanguage! == .fr  {
                langue = "FR"
            _ = SetLanguage("French_fr")

         }else if Locale.currentLanguage! == .es{
              langue = "ES"
            _ = SetLanguage("Spanish_es")

         }else if Locale.currentLanguage! == .pt {
              langue = "POT"
            _ = SetLanguage("Portugese_pg")

         }else if Locale.currentLanguage! == .it {
            _ = SetLanguage("Italien_it")
              langue = "IT"
             }
        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguagesNotif[2]
        {
             print("Fr language")
            langue = "FR"
         _ = SetLanguage("French_fr")

        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguagesNotif[3] {
         print("Es language")

            langue = "ES"
         _ = SetLanguage("Spanish_es")
        }else if Localisator.sharedInstance.currentLanguage == arrayLanguagesNotif[4]{
         print("Pt language")

         langue = "POT"
         _ = SetLanguage("Portugese_pg")
         }else if Localisator.sharedInstance.currentLanguage == arrayLanguagesNotif[5]{
         print("It language")

         langue = "IT"
         _ = SetLanguage("Italien_it")
        }else{
         print("EN language")

         langue = "EN"
         _ = SetLanguage("English_en")
     }
        return langue
    }
    /*
     * Func to parse the JSON file to Json Array
     */
    func getSupported() -> JSON {
        
        let filePath = Bundle.main.path(forResource: "Country", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
        do {
            let json = try JSON(data: data!)
            return json
            
        }catch{
            
        }
        return JSON()
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        GIDSignIn.sharedInstance()?.handle(url)
         if (ApplicationDelegate.shared.application(app, open: url, options: options)){
                   return true
         }else if (GIDSignIn.sharedInstance()?.handle(url))!{
                   return true
               }else if TWTRTwitter.sharedInstance().application(app, open: url, options: options){
                   return true
               }else if url.absoluteString.contains("itunes.apple.com") {
                   return true
               }
               return false
    }
    @objc func showWaiting(_ notif:Notification){
        alertWaiting = UIAlertController(title: "Video Face", message: "waiting...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        self.alertWaiting!.view.addSubview(loadingIndicator)
        self.window?.rootViewController?.present(self.alertWaiting!, animated: true, completion: nil)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
       /* let domain = Bundle.main.bundleIdentifier!
              UserDefaults.standard.removePersistentDomain(forName: domain)
              UserDefaults.standard.synchronize() */
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func restartApplication(){
           
           let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenController") as! SplashScreenController
           guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController else {
               return
           }
           vc.view.frame = rootViewController.view.frame
           vc.view.layoutIfNeeded()
           UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
               window.rootViewController = vc
           }, completion: nil)
           
       }

}
extension UITabBarController {
    func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
        if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // animation
        UIView.animate(withDuration: animated ? duration : 0.0) {
            
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
    
    func tabBarIsVisible() ->Bool {
        print("UIScreen:",UIScreen.main.bounds.height)
        return self.tabBar.frame.origin.y < (UIScreen.main.bounds.height)
    }
}
extension DateFormatter {
    func date(fromSwapiString dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func date(fromTodayDate dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = NSTimeZone.system
        //self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func date(noChange  dateString : String) -> Date? {
        print(dateString)
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
       
        return self.date(from: dateString)
        
    }
}
extension Locale {
    
    static var enLocale: Locale {
        
        return Locale(identifier: "en-EN")
    } // to use in **currentLanguage** to get the localizedString in English
    
    static var currentLanguage: Language? {
        
        guard let code = preferredLanguages.first?.components(separatedBy: "-").first else {
            
            print("could not detect language code")
            
            return nil
        }
        print("Code Language : ",code)
        if ((code != "en") && (code != "fr") && (code != "es") && (code != "pt") && (code != "it")) {
            return Language(rawValue: "en")
        }else{
            return Language(rawValue: code)
        }
       
    }
}
enum Language: String {
    
    case none = ""
    case en = "en"
    case fr = "fr"
    case it = "it"
    case es = "es"
    case pt = "pt"
    
}
extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        //print("BOTTOM")
        self.setContentOffset(bottomOffset, animated: animated)
    }
    func scrollToTop(animated: Bool) {
        
        self.setContentOffset(.zero, animated: animated)
        self.delegate?.scrollViewDidScrollToTop?(self)
    }
}
extension NSMutableData {
        
        func appendString(string: String) {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            append(data!)
        }
}
extension UIViewController {
 func constructURL( string : String) -> URL {
    
    return URL(string: ScriptBase.Image_URL + string)!
        
    
}
}
enum NotificationType : String {
    case gif  = "gif"
    case mime  = "mime"

    case friendRequest = "friendRequest"
    case chat = "chat"
}
extension CGImage {
    var brightness: Double {
        get {
            let imageData = self.dataProvider?.data
            let ptr = CFDataGetBytePtr(imageData)
            var x = 0
            var result: Double = 0
            for _ in 0..<self.height {
                for _ in 0..<self.width {
                    let r = ptr![0]
                    let g = ptr![1]
                    let b = ptr![2]
                    result += (0.299 * Double(r) + 0.587 * Double(g) + 0.114 * Double(b))
                    x += 1
                }
            }
            let bright = result / Double (x)
            return bright
        }
    }
}
extension UIImage {
    var brightness: Double {
        get {
            return (self.cgImage?.brightness)!
        }
    }
}
extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}
extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    func stringByAddingPercentEncodingForRFC3986() -> String? {
       let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return self.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
     }
    public func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String? {
        let unreserved = "*-._"
        let allowedCharacterSet = NSMutableCharacterSet.alphanumeric()
        allowedCharacterSet.addCharacters(in: unreserved)
        
        if plusForSpace {
            allowedCharacterSet.addCharacters(in: " ")
        }
        
        var encoded =  self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet as CharacterSet)
        if plusForSpace {
            
          encoded = encoded?.replacingOccurrences(of: " ", with: "+")
        }
        return encoded
      }
}
extension UIView {

    var isAnimating: Bool {
        return (self.layer.animationKeys()?.count ?? 0) > 0
    }

}
extension UIApplication {

    @discardableResult
    static func openAppSettings() -> Bool {
        guard
            let settingsURL = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settingsURL)
            else {
                return false
        }

        UIApplication.shared.open(settingsURL)
        return true
    }
}
