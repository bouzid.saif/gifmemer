//
//  CustomCameraPickController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-12-18.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftSpinner
import SwiftyJSON
import MapleBacon
import GfycatKit
import Alamofire
class CustomCameraPickController: appleDetection,AVCapturePhotoCaptureDelegate {
    var captureSesssion: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    fileprivate var buffer:NSMutableData = NSMutableData()

    @IBOutlet weak var photoPreviewImageView: UIImageView!
    @IBOutlet weak var pleaseMakeSureLBL1 : UILabel!
    @IBOutlet weak var pleaseMakeSureLBL2 : UILabel!
    @IBOutlet weak var proceedBTN : UIButton!
    @IBOutlet weak var takePhotoImageView : UIImageView!
    var item : GfycatMediaCustom!
    var isGifOrMime : Bool = true
    @IBAction func proceedAction (_ sender: UIButton) {
        if isGifOrMime {
            self.traitGif(gifId: item.gfyId, gifUrl: item.mpgUrl.absoluteString, nameGif: item.gfyName)
        }else{
            self.traitMime(mimeId: item.gfyId, mimeUrl: item.posterUrl.absoluteString, nameGif: item.gfyName, gifHeight: item.height, gifWidth: item.width)
        }
    }
    @IBAction func closeView( _ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func didTapOnTakePhotoButton(_ sender: UITapGestureRecognizer) {
        if takePhotoImageView.image == UIImage(named: "takePhoto") {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput.capturePhoto(with: settings, delegate: self)
        }else{
            takePhotoImageView.image = UIImage(named: "takePhoto")
            self.pleaseMakeSureLBL1.isHidden = false
            self.pleaseMakeSureLBL2.isHidden = true
            self.proceedBTN.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.photoPreviewImageView.alpha = 0
            }) { (complete) in
                self.photoPreviewImageView.isHidden = true
            }
        }
    }
    func traitGif( gifId:String, gifUrl: String,nameGif : String){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "gifId" : gifId,
                "gifUrl" : gifUrl,
                "name" : nameGif
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addGif, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
             if data["status"].boolValue {
                

                 let alert = UIAlertController(title: "Processing GIF", message: "We are processing your selected gif, we will notify you as soon as it's finished", preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                // self.present(alert, animated: true, completion: nil)
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: NSNotification.Name.init("presentRes"), object: self.item)
                }
                
                 
             }else{
                 

                 let alert = UIAlertController(title: "Processing GIF", message: data["message"].stringValue, preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: {(ok) in
                    self.dismiss(animated: true, completion: nil)
                 })
                 alert.addAction(actionOK)
                 self.present(alert, animated: true, completion: nil)
             }
             
               
            }
            
        }catch {
            
        }
    }
    func traitMime(mimeId:String, mimeUrl: String,nameGif : String,gifHeight: String,gifWidth : String){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "mimeId" : mimeId,
                "mimeUrl" : mimeUrl,
                "name" : nameGif,
                "gifHeight" : gifHeight,
                "gifWidth" : gifWidth
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addMime, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
             if data["status"].boolValue {
                 

                 let alert = UIAlertController(title: "Processing Mime", message: "We are processing your selected mime, we will notify you as soon as it's finished", preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                 //self.present(alert, animated: true, completion: nil)
                 self.dismiss(animated: true) {
                                   NotificationCenter.default.post(name: NSNotification.Name.init("presentRes"), object: self.item)
                    }
             }else{
                

                 let alert = UIAlertController(title: "Processing Mime", message: data["message"].stringValue, preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                 self.present(alert, animated: true, completion: nil)
             }
             
               
            }
            
        }catch {
            
        }
    }
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
                   print("error occure : \(error.localizedDescription)")
               }
        SwiftSpinner.show(Localization("PleaseWait"))
        self.takePhotoImageView.image = UIImage(named: "cancelPhoto")
        let imageData = photo.fileDataRepresentation()
        let dataProvider = CGDataProvider(data: imageData! as CFData)
        let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
        let imageDef = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImage.Orientation.right)
        let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImage.Orientation.leftMirrored)
        self.photoPreviewImageView.image = image
        self.photoPreviewImageView.isHidden = false
        self.pleaseMakeSureLBL1.isHidden = true
        self.pleaseMakeSureLBL2.isHidden = false
        self.proceedBTN.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.photoPreviewImageView.alpha = 1
        }) { (complete) in
              self.process(image: self.fixOrientation(img: imageDef)) { (resultCode) in
           // let openCvWrapper = OpenCVWrapper()
           // let resultCode = openCvWrapper.isThisWorking(self.fixOrientation(img: imageDef))
                                     print("ResultCode : ",resultCode)
                                     if resultCode == 0 {
                                         SwiftSpinner.hide()
                                         let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                                         let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: {ok in
                                            self.takePhotoImageView.image = UIImage(named: "takePhoto")
                                                       self.pleaseMakeSureLBL1.isHidden = false
                                                       self.pleaseMakeSureLBL2.isHidden = true
                                                       self.proceedBTN.isHidden = true
                                                       UIView.animate(withDuration: 0.3, animations: {
                                                           self.photoPreviewImageView.alpha = 0
                                                       }) { (complete) in
                                                           self.photoPreviewImageView.isHidden = true
                                                       }
                                         })
                                         alert.addAction(alertAction)
                                         self.present(alert, animated: true, completion: nil)
                                              return
                                            
                                     }else if resultCode == 2 {
                                         SwiftSpinner.hide()
                                         let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceMore"), preferredStyle: .alert)
                                         let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: { ok in
                                            self.takePhotoImageView.image = UIImage(named: "takePhoto")
                                                       self.pleaseMakeSureLBL1.isHidden = false
                                                       self.pleaseMakeSureLBL2.isHidden = true
                                                       self.proceedBTN.isHidden = true
                                                       UIView.animate(withDuration: 0.3, animations: {
                                                           self.photoPreviewImageView.alpha = 0
                                                       }) { (complete) in
                                                           self.photoPreviewImageView.isHidden = true
                                                       }
                                         })
                                         alert.addAction(alertAction)
                                         self.present(alert, animated: true, completion: nil)
                                     }else if resultCode == 3{
                                         SwiftSpinner.hide()
                                      let alert = UIAlertController(title: Localization("Photo"), message: "the face is not proprely recognized or it is not in a portrait mode", preferredStyle: .alert)
                                                                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: {ok in
                                                                        self.takePhotoImageView.image = UIImage(named: "takePhoto")
                                                                                   self.pleaseMakeSureLBL1.isHidden = false
                                                                                   self.pleaseMakeSureLBL2.isHidden = true
                                                                                   self.proceedBTN.isHidden = true
                                                                                   UIView.animate(withDuration: 0.3, animations: {
                                                                                       self.photoPreviewImageView.alpha = 0
                                                                                   }) { (complete) in
                                                                                       self.photoPreviewImageView.isHidden = true
                                                                                   }
                                                                    })
                                                                    alert.addAction(alertAction)
                                                                    self.present(alert, animated: true, completion: nil)
                                     }else{
                      self.uploadToServer()

                                      }
            }
        }
        
    }
    
    // callBack from take picture
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pleaseMakeSureLBL2.text =  Localization("MessageFacePick")
        self.pleaseMakeSureLBL1.text =  Localization("MessageFacePick")
        captureSesssion = AVCaptureSession()
        captureSesssion.sessionPreset = AVCaptureSession.Preset.photo
              cameraOutput = AVCapturePhotoOutput()
       
        let device =  AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)

        if let input = try? AVCaptureDeviceInput(device: device!) {
                  if captureSesssion.canAddInput(input) {
                      captureSesssion.addInput(input)
                      if captureSesssion.canAddOutput(cameraOutput) {
                          captureSesssion.addOutput(cameraOutput)
                          previewLayer = AVCaptureVideoPreviewLayer(session: captureSesssion)
                        previewLayer.videoGravity = .resizeAspect
                        previewLayer.connection?.videoOrientation = .portrait
                        self.view.layer.insertSublayer(previewLayer, at: 0)
                          captureSesssion.startRunning()
                      }
                  } else {
                      print("issue here : captureSesssion.canAddInput")
                  }
              } else {
                  print("some problem here")
              }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = self.view.bounds

    }
}
extension CustomCameraPickController : URLSessionDelegate,URLSessionTaskDelegate, URLSessionDataDelegate {
    func randomStringWithLength() -> String{
           let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
           let len = 15
           let date = Date()
           let randomString : NSMutableString = NSMutableString(capacity: len)
           for _ in 0...(len - 1){
               let length = UInt32(letters.length)
               let rand = arc4random_uniform(length)
               randomString.appendFormat("%C", letters.character(at: Int(rand)))
               
           }
           var resultFinal = "ios" + (randomString as String) + date.description
           resultFinal = resultFinal.replacingOccurrences(of: " ", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: ":", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: "+", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: "-", with: "")
           resultFinal = resultFinal.replacingOccurrences(of: ".", with: "")
           return resultFinal
       }
    func uploadToServer(){
           // SwiftSpinner.show("Uploading Picture...")
           let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
           let contentType = "multipart/form-data; boundary=" + boundaryConstant
           
           let mimeType = "image/jpeg"
           
           
           let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
           //?.rotate(radians: Float( -(Double.pi / 2)))
           let image = self.photoPreviewImageView.image
           let fileData : Data? = image!.jpegData(compressionQuality: 1)
           let requestBodyData : NSMutableData = NSMutableData()
           requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
           
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
               let a = try JSON(data: dataFromString!)
               let key = "userId"
               requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
               requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
           }catch{
               
           }
           requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
           do {
               let a = try JSON(data: dataFromString!)
               let fieldName = "picture"
               let filename = "zonz" + self.randomStringWithLength() + ".jpg"
               requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
           }catch{
               
           }
           requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
           
           //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
           requestBodyData.append(fileData!)
           // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
           requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
           requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
           var request = URLRequest(url: uploadScriptUrl!)
           
           
           request.httpMethod = "POST"
           request.httpBody = requestBodyData as Data
           request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
           request.setValue(contentType, forHTTPHeaderField: "Content-Type")
           let config = URLSessionConfiguration.default
           let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
           let task = session.uploadTask(withStreamedRequest: request)
           task.resume()
       }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if (error != nil ) {
            SwiftSpinner.show("Unexpected Error", animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                SwiftSpinner.hide()
            }
        }else{
                let q = JSON(buffer)
             SwiftSpinner.hide()
            if q["error"].exists() == false {
                do {
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = try JSON(data: dataFromString!)
                    a["userImageURL"].stringValue = q["userImageURL"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    MapleBacon.shared.cache.clearDisk()
                    MapleBacon.shared.cache.clearMemory()
                    SwiftSpinner.hide()
                
                }catch{
                    
                }
            }
        }
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
      
        SwiftSpinner.show(progress: Double(uploadProgress), title: "\(Int(uploadProgress * 100))% \n Uploading" )
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
}
class GfycatMediaCustom :  GfycatMedia {
    var width : String!
    var height : String!
}
