//
//  BuyZonz.swift
//  YLYL
//
//  Created by macbook on 3/29/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import Alamofire
import SwiftyJSON
import ChameleonFramework
@IBDesignable
class BuyZonz : UIView,SKProductsRequestDelegate,PKIStateDelegate{
    @IBOutlet weak var zonzContainer: UIView!
    
    @IBOutlet weak var MyZonzLBL: UILabel!
    
    @IBOutlet weak var zonzImageTOP: UIImageView!
    
    @IBOutlet weak var zonzPointsLBLTOP: UILabel!
    
    @IBOutlet weak var zonzTextLBL: UILabel!
    
    @IBOutlet weak var zonzHamburgerOne: UIImageView!
    
    @IBOutlet weak var zonzMoneyOne: UILabel!
    @IBOutlet weak var zonzHamburgerTwo: UIImageView!
    
    @IBOutlet weak var zonzMoneyTwo: UILabel!
    @IBOutlet weak var zonzHamburgerThree: UIImageView!
    
    @IBOutlet weak var zonzMoneyThree: UILabel!
    
    @IBOutlet weak var zonzHamburgerFour: UIImageView!
    
    @IBOutlet weak var zonzMoneyFour: UILabel!
    
    var isProductsValidated = false
    
    @IBOutlet weak var pointsLBLOne: UILabel!
    
    @IBOutlet weak var pointsLBLTwo: UILabel!
    
    @IBOutlet weak var pointsLBLThree: UILabel!
    
    @IBOutlet weak var pointsLBLFour: UILabel!
    
    @IBOutlet weak var pointsLBLFive: UILabel!
    
    @IBOutlet weak var lineOne: UIImageView!
    
    @IBOutlet weak var lineTwo: UIImageView!
    
    @IBOutlet weak var lineThree: UIImageView!
    
    @IBOutlet weak var lineFour: UIImageView!
        
    @IBOutlet weak var zonzBroughtContainer: UIView!
    
    @IBOutlet weak var zonzBroughtLBL: UILabel!
    @IBOutlet weak var ZOne : UIImageView!
    @IBOutlet weak var ZTwo : UIImageView!
    @IBOutlet weak var ZThree : UIImageView!
    @IBOutlet weak var ZFour : UIImageView!
    @IBOutlet weak var ZFive : UIImageView!
    var navigation : UINavigationController!
    @IBOutlet weak var animateZonzBroughtConst: NSLayoutConstraint!
    let Zonz200 = "com.regystone.gifmemer.zonz150"
    let Zonz300 = "com.regystone.gifmemer.zonz350"
    let Zonz500 = "com.regystone.gifmemer.zonz500"
    let Zonz900 = "com.regystone.gifmemer.zonz650"
    let Zonz1000 = "com.regystone.zonzay.zonz1000"
    var products : [SKProduct] = []
    fileprivate var containerView: UIView!
    fileprivate let nibName = "BuyZonz"
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
    func hideBuyOne(hide:Bool) {
        self.zonzMoneyOne.isHidden = hide
        self.zonzHamburgerOne.isHidden = hide
        self.lineOne.isHidden = hide
        self.pointsLBLOne.isHidden = hide
        self.ZOne.isHidden = hide
    }
    func hideBuyTwo(hide:Bool) {
        self.zonzMoneyTwo.isHidden = hide
        self.zonzHamburgerTwo.isHidden = hide
        self.lineTwo.isHidden = hide
        self.pointsLBLTwo.isHidden = hide
        self.ZTwo.isHidden = hide
    }
    func hideBuyThree(hide:Bool) {
        self.zonzMoneyThree.isHidden = hide
        self.zonzHamburgerThree.isHidden = hide
        self.lineThree.isHidden = hide
        self.pointsLBLThree.isHidden = hide
        self.ZThree.isHidden = hide
    }
    func hideBuyFour(hide:Bool) {
        self.zonzMoneyFour.isHidden = hide
        self.zonzHamburgerFour.isHidden = hide
        self.lineFour.isHidden = hide
        self.pointsLBLFour.isHidden = hide
        self.ZFour.isHidden = hide
    }
    
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        containerView.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.containerView.frame, colors: [UIColor(red: 208/255, green: 23/255, blue: 130/255, alpha: 1), UIColor(red: 84/255, green: 0/255, blue: 255/255, alpha: 1)], limit: NSNumber(value: 0.8))
        zonzPointsLBLTOP.adjustsFontForContentSizeCategory = true
        zonzPointsLBLTOP.adjustsFontSizeToFitWidth = true
        PKIAPHandler.shared.delegate = self
        zonzTextLBL.text = Localization("BuyGimees")
        PKIAPHandler.shared.setProductIds(ids: [Zonz200,Zonz300,Zonz500,Zonz900])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([Zonz200,Zonz300,Zonz500,Zonz900]))
        
        
        PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            
            for product in skProducts {
                print(product.productIdentifier)
                
            }
            self.products  = skProducts.sorted(by: { (sk1, sk2) -> Bool in
                if sk1.price.compare(sk2.price) == .orderedDescending {
                    return true
                }else{
                    return false
                }
                
            })
            
            print("SK Order: ",skProducts )
            for product in self.products {
                print(product.productIdentifier)
                self.isProductsValidated = true
                DispatchQueue.main.async {
                    if product.productIdentifier == self.Zonz200 {
                        #if targetEnvironment(simulator)
                        #else
                          self.zonzMoneyOne.text = product.localizedPrice!
                        #endif
                      
                    }
                    if product.productIdentifier == self.Zonz300 {
                        #if targetEnvironment(simulator)
                        #else
                        self.zonzMoneyTwo.text = product.localizedPrice!
                        #endif
                    }
                    if product.productIdentifier == self.Zonz500 {
                        #if targetEnvironment(simulator)
                        #else
                        self.zonzMoneyThree.text = product.localizedPrice!
                        #endif
                    }
                    if product.productIdentifier == self.Zonz900 {
                        #if targetEnvironment(simulator)
                        #else
                        self.zonzMoneyFour.text = product.localizedPrice!
                        #endif
                    }
                    if product.productIdentifier == self.Zonz1000 {
                    }
                }
                
            }
        }
        configureZonzContainer()
        createGestures()
        addSubview(containerView)
       // self.zonzBroughtContainer.origin.x = (UIScreen.main.bounds.width / 2) - 87
      //  self.zonzBroughtContainer.origin.y = 0
    }
    func animateWithPrice(view:String) {
        switch view {
        case "One":
            self.zonzBroughtLBL.text = "+150"
        case "Two":
            self.zonzBroughtLBL.text = "+350"
        case "Three":
            self.zonzBroughtLBL.text = "+500"
        case "Four":
            self.zonzBroughtLBL.text = "+650"
        case "Five":
            self.zonzBroughtLBL.text = "+3100"
        default:
            break
        }
     
        
        self.zonzBroughtContainer.autoresizesSubviews = true
        //self.zonzBroughtContainer.size = CGSize.zero
        /*self.zonzBroughtContainer.subviews.forEach { (vi) in
            vi.size = CGSize.zero
            vi.layoutIfNeeded()
        } */
       // self.zonzBroughtContainer.layoutIfNeeded()
        self.zonzBroughtContainer.isHidden = false
       
        self.zonzBroughtContainer.layoutIfNeeded()
        UIView.transition(with: self.zonzBroughtContainer, duration: 1.4, options: [.transitionCrossDissolve], animations: {
              self.animateZonzBroughtConst.constant = 0
            self.zonzBroughtContainer.setNeedsUpdateConstraints()
            self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil)
      /*  UIView.animate(withDuration: 3, delay: 0, options: [.transitionCurlDown], animations: {
           
           self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil) */
       /* UIView.animate(withDuration: 0.8, delay: 0, options: [.curveEaseInOut], animations: {
            self.zonzBroughtContainer.size = CGSize(width: width, height: height)
            self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil) */
    }
    func createGestures(){
        let tapgestureOne = UITapGestureRecognizer(target: self, action: #selector(self.buyTwoHundred(_:)))
        self.zonzHamburgerOne.isUserInteractionEnabled = true
        self.zonzHamburgerOne.addGestureRecognizer(tapgestureOne)
        //
        let tapgestureTwo = UITapGestureRecognizer(target: self, action: #selector(self.buyThreeHundred(_:)))
        self.zonzHamburgerTwo.isUserInteractionEnabled = true
        self.zonzHamburgerTwo.addGestureRecognizer(tapgestureTwo)
        //
        let tapgestureThree = UITapGestureRecognizer(target: self, action: #selector(self.buyFiveHundred(_:)))
        self.zonzHamburgerThree.isUserInteractionEnabled = true
        self.zonzHamburgerThree.addGestureRecognizer(tapgestureThree)
        //
        let tapgestureFour = UITapGestureRecognizer(target: self, action: #selector(self.buyNineHundred(_:)))
        self.zonzHamburgerFour.isUserInteractionEnabled = true
        self.zonzHamburgerFour.addGestureRecognizer(tapgestureFour)
        //
        
        
        
    }
    @objc func buyTwoHundred(_ sender: UITapGestureRecognizer) {
        if self.isProductsValidated {
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
         PKIAPHandler.shared.purchase(product: self.products[3]) { (purchaseAlertType, product, transaction) in
            print(purchaseAlertType.message)
            print("Transaction : ", transaction)
        if transaction != nil {
                self.buyZonzays(zonz: "150", product: product!, completionHandler: { (verif) in
        if verif  {
               
            self.hideBuyOne(hide: true)
            self.zonzPointsLBLTOP.alpha = 0
            let initImageX = self.zonzImageTOP.frame.origin.x
             let initImageY = self.zonzImageTOP.frame.origin.y
            let initImageSize = self.zonzImageTOP.frame.size
             let initLBLX = self.zonzPointsLBLTOP.frame.origin.x
            let initLBLY = self.zonzPointsLBLTOP.frame.origin.y
            let initLBLSize = self.zonzPointsLBLTOP.frame.size
            
            UIView.animate(withDuration: 1.0, animations: {
                self.zonzImageTOP.frame.origin.x = self.zonzBroughtContainer.frame.origin.x + 11
                self.zonzImageTOP.frame.origin.y = self.zonzBroughtContainer.frame.origin.y + 37
                self.zonzImageTOP.frame.size = CGSize(width: 65.28, height: 72.71)
                self.zonzPointsLBLTOP.frame.origin.x = (self.zonzBroughtContainer.frame.origin.x + 11 + 65.28) +  8
                 self.zonzPointsLBLTOP.frame.origin.y = (self.zonzBroughtContainer.frame.origin.y + 37 + 36.355) - 17.5
                self.zonzPointsLBLTOP.frame.size = CGSize(width: 75, height: 39)
                // self.zonzPointsLBLTOP.alpha = 1
            }, completion:{ _ in
                self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                let initVal = Int(self.zonzPointsLBLTOP.text!)!
                _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                    self.zonzPointsLBLTOP.tag += 75
                    self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                    
                    if self.zonzPointsLBLTOP.tag == (initVal + 150) {
                        
                time.invalidate()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                            self.zonzPointsLBLTOP.alpha = 0
                            UIView.animate(withDuration: 0.6, animations: {
                                self.zonzImageTOP.frame.origin.x = initImageX
                                self.zonzImageTOP.frame.origin.y = initImageY
                                self.zonzImageTOP.frame.size = initImageSize
                                self.zonzPointsLBLTOP.frame.origin.x = initLBLX
                                self.zonzPointsLBLTOP.frame.origin.y = initLBLY
                                self.zonzPointsLBLTOP.frame.size = initLBLSize
                            })
                            UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                self.zonzPointsLBLTOP.alpha = 1
                            }, completion: { _ in})
                        })
                        
                    }
                })
                
            })
            UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                self.zonzPointsLBLTOP.alpha = 1
            }, completion: { _ in
              
                
            })
        }else{
            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
                                      let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                      alert.addAction(action)
                                      self.navigation.present(alert, animated: true, completion: nil)
                               self.hideBuyTwo(hide: false)
                                              self.hideBuyThree(hide: false)
                                              self.hideBuyFour(hide: false)
            }
            })
            }else{
                self.hideBuyTwo(hide: false)
                self.hideBuyThree(hide: false)
                self.hideBuyFour(hide: false)
                   
                }
                
       }
        }
        }else{
            /// Do alert no validation apple
            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
            let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(action)
            self.navigation.present(alert, animated: true, completion: nil)
        }
       
    }
    
    @objc func buyThreeHundred(_ sender: UITapGestureRecognizer) {
        if self.isProductsValidated {
        self.hideBuyOne(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[2]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "350", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.frame.origin.x
                    let initImageY = self.zonzImageTOP.frame.origin.y
                    let initImageSize = self.zonzImageTOP.frame.size
                    let initLBLX = self.zonzPointsLBLTOP.frame.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.frame.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.frame.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.frame.origin.x = self.zonzBroughtContainer.frame.origin.x + 11
                        self.zonzImageTOP.frame.origin.y = self.zonzBroughtContainer.frame.origin.y + 37
                        self.zonzImageTOP.frame.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.frame.origin.x = (self.zonzBroughtContainer.frame.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.frame.origin.y = (self.zonzBroughtContainer.frame.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.frame.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 175
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 350) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.frame.origin.x = initImageX
                                        self.zonzImageTOP.frame.origin.y = initImageY
                                        self.zonzImageTOP.frame.size = initImageSize
                                        self.zonzPointsLBLTOP.frame.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.frame.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.frame.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }else{
                            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
                                                      let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                                      alert.addAction(action)
                                                      self.navigation.present(alert, animated: true, completion: nil)
                                                self.hideBuyThree(hide: false)
                                                                 self.hideBuyOne(hide: false)
                                                                 self.hideBuyFour(hide: false)
                        }
                    })
                }else{
                   
                    self.hideBuyThree(hide: false)
                    self.hideBuyOne(hide: false)
                    self.hideBuyFour(hide: false)
                   
                }
                
            }
        }
        }else{
           let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
            let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(action)
            self.navigation.present(alert, animated: true, completion: nil)
        }
    }
    @objc func buyFiveHundred(_ sender: UITapGestureRecognizer) {
        if self.isProductsValidated {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyFour(hide: true)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[1]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "500", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.frame.origin.x
                    let initImageY = self.zonzImageTOP.frame.origin.y
                    let initImageSize = self.zonzImageTOP.frame.size
                    let initLBLX = self.zonzPointsLBLTOP.frame.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.frame.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.frame.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.frame.origin.x = self.zonzBroughtContainer.frame.origin.x + 11
                        self.zonzImageTOP.frame.origin.y = self.zonzBroughtContainer.frame.origin.y + 37
                        self.zonzImageTOP.frame.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.frame.origin.x = (self.zonzBroughtContainer.frame.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.frame.origin.y = (self.zonzBroughtContainer.frame.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.frame.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 250
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 500) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.frame.origin.x = initImageX
                                        self.zonzImageTOP.frame.origin.y = initImageY
                                        self.zonzImageTOP.frame.size = initImageSize
                                        self.zonzPointsLBLTOP.frame.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.frame.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.frame.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }else{
                            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
                                                      let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                                      alert.addAction(action)
                                                      self.navigation.present(alert, animated: true, completion: nil)
                                               self.hideBuyTwo(hide: false)
                                                                  self.hideBuyFour(hide: false)
                                                                  self.hideBuyOne(hide: false)
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyOne(hide: false)
                    
                }
                
            }
        }
        }else{
           let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
            let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(action)
            self.navigation.present(alert, animated: true, completion: nil)
        }
    }
    @objc func buyNineHundred(_ sender: UITapGestureRecognizer) {
        if self.isProductsValidated {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[0]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "650", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.frame.origin.x
                    let initImageY = self.zonzImageTOP.frame.origin.y
                    let initImageSize = self.zonzImageTOP.frame.size
                    let initLBLX = self.zonzPointsLBLTOP.frame.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.frame.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.frame.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.frame.origin.x = self.zonzBroughtContainer.frame.origin.x + 11
                        self.zonzImageTOP.frame.origin.y = self.zonzBroughtContainer.frame.origin.y + 37
                        self.zonzImageTOP.frame.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.frame.origin.x = (self.zonzBroughtContainer.frame.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.frame.origin.y = (self.zonzBroughtContainer.frame.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.frame.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 325
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 650) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.frame.origin.x = initImageX
                                        self.zonzImageTOP.frame.origin.y = initImageY
                                        self.zonzImageTOP.frame.size = initImageSize
                                        self.zonzPointsLBLTOP.frame.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.frame.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.frame.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }else{
                            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
                                   let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                   alert.addAction(action)
                                   self.navigation.present(alert, animated: true, completion: nil)
                            self.hideBuyTwo(hide: false)
                                               self.hideBuyThree(hide: false)
                                               
                                               self.hideBuyOne(hide: false)
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyThree(hide: false)
                    
                    self.hideBuyOne(hide: false)
                }
                
            }
            
        }
        }else{
          
        }
    }
    @objc func buyOneThousend(_ sender: UITapGestureRecognizer) {
        if self.isProductsValidated {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[0]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "3100", product: product!, completionHandler: { (verif) in
                        if verif  {
                   
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.frame.origin.x
                    let initImageY = self.zonzImageTOP.frame.origin.y
                    let initImageSize = self.zonzImageTOP.frame.size
                    let initLBLX = self.zonzPointsLBLTOP.frame.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.frame.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.frame.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.frame.origin.x = self.zonzBroughtContainer.frame.origin.x + 11
                        self.zonzImageTOP.frame.origin.y = self.zonzBroughtContainer.frame.origin.y + 37
                        self.zonzImageTOP.frame.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.frame.origin.x = (self.zonzBroughtContainer.frame.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.frame.origin.y = (self.zonzBroughtContainer.frame.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.frame.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 1550
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 3100) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.frame.origin.x = initImageX
                                        self.zonzImageTOP.frame.origin.y = initImageY
                                        self.zonzImageTOP.frame.size = initImageSize
                                        self.zonzPointsLBLTOP.frame.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.frame.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.frame.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyThree(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyOne(hide: false)
                }
                
            }
        }
        }else{
            let alert = UIAlertController(title: "GifMemer", message: Localization("paymentSoon"), preferredStyle: .alert)
            let action = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
            alert.addAction(action)
            self.navigation.present(alert, animated: true, completion: nil)
        }
    }
    func configureZonzContainer(){
        self.zonzContainer.layer.cornerRadius = 2
        self.zonzContainer.layer.borderColor = UIColor.white.cgColor
        self.zonzContainer.layer.borderWidth = 1.0
        self.zonzContainer.layer.masksToBounds = true
    }
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
    func paymentState(state: SKPaymentTransactionState) {
        if state == .failed {
            self.hideBuyOne(hide: false)
            self.hideBuyTwo(hide: false)
            self.hideBuyThree(hide: false)
            self.hideBuyFour(hide: false)
            
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle.main
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    @objc func buyZonzays(zonz:String,product:SKProduct,completionHandler : @escaping ((Bool) -> Void)){
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    
                    let settings : Parameters = [
                        "userId" : a["_id"].stringValue,
                        "purchaseZonz" : zonz,
                        "purchaseMoney" : product.price.description(withLocale: Locale.current)
                    ]
                    let headers : HTTPHeaders = [
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                    ]
                    print(settings)
                    print(headers)
                    Alamofire.request(ScriptBase.sharedInstance.zonzPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        let data = JSON(response.data)
                        if data != JSON.null {
                            completionHandler(true)
                          
                            
                        }else{
                          completionHandler(false)
                        }
                        
                    }
                    
                    
                }catch {
                 completionHandler(false)
                }
    }
    
}
