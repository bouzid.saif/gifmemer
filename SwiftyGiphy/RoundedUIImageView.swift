//
//  RoundedUIImageView.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 13/07/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import  UIKit
@IBDesignable
class RoundedUIImageView: UIImageView {
    @IBInspectable var round: Bool = true {
        didSet { self.setNeedsLayout() }
    }
    
    @IBInspectable var widthX: CGFloat = 2.5 {
        didSet { self.setNeedsLayout() }
    }
    
    @IBInspectable var color: UIColor = UIColor(red: 208, green: 208, blue: 208, alpha: 1){
        didSet { self.setNeedsLayout() }
    }
    @IBInspectable var colorBackground : UIColor = UIColor.clear {
        didSet { self.setNeedsLayout()}
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
    }
   
}
internal class ClosureWrapper<T> {
    var closure: (T) -> Void
    init(_ closure: @escaping (T) -> Void) {
        self.closure = closure
    }
}

internal protocol Attachable {
    func set(_ attachObj: Any?, forKey key: inout UInt)
    func getAttach(forKey key: inout UInt) -> Any?
}

extension Attachable {

    public func set(_ attachObj: Any?, forKey key: inout UInt) {
        objc_setAssociatedObject(self, &key, attachObj, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    public func getAttach(forKey key: inout UInt) -> Any? {
        return objc_getAssociatedObject(self, &key)
    }

}
private var closureKey: UInt = 0
@IBDesignable
extension RoundedUIImageView: Attachable {
    @IBInspectable
    /// Set this to true if you want to center the image on any detected faces.
    public var focusOnFaces: Bool {
        set {
            let image = self.image
            set(image: image, focusOnFaces: newValue)
        } get {
            return sublayer() != nil ? true : false
        }
    }
    public func set(image: UIImage?, focusOnFaces: Bool) {
        guard focusOnFaces == true else {
            self.removeImageLayer(image: image)
            return
        }
        setImageAndFocusOnFaces(image: image)
    }
    // You can provide a closure here to receive a callback for when all face
    /// detection and image adjustments have been finished.
    public var didFocusOnFaces: (() -> Void)? {
        set {
            set(newValue, forKey: &closureKey)
        } get {
            return getAttach(forKey: &closureKey) as? (() -> Void)
        }
    }
    private func setImageAndFocusOnFaces(image: UIImage?) {
           DispatchQueue.global(qos: .default).async {
               guard let image = image else {
                   return
               }

               guard let ciImage = CIImage(image: image) else {
                   return
               }

               let detector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyLow])
               let features = detector!.features(in: ciImage)

               if features.isEmpty == false {
                   
                   let imgSize = CGSize(width: image.cgImage!.width, height: image.cgImage!.height)
                   self.applyFaceDetection(for: features, size: imgSize, image: image)
               } else {
                  
                   self.removeImageLayer(image: image)
               }
           }
       }
    private func applyFaceDetection(for features: [CIFeature], size: CGSize, image: UIImage) {
        var rect = features[0].bounds
        rect.origin.y = size.height - rect.origin.y - rect.size.height
        var rightBorder = Double(rect.origin.x + rect.size.width)
        var bottomBorder = Double(rect.origin.y + rect.size.height)

        for feature in features[1..<features.count] {
            var oneRect = feature.bounds
            oneRect.origin.y = size.height - oneRect.origin.y - oneRect.size.height
            rect.origin.x = min(oneRect.origin.x, rect.origin.x)
            rect.origin.y = min(oneRect.origin.y, rect.origin.y)

            rightBorder = max(Double(oneRect.origin.x + oneRect.size.width), Double(rightBorder))
            bottomBorder = max(Double(oneRect.origin.y + oneRect.size.height), Double(bottomBorder))
        }

        rect.size.width = CGFloat(rightBorder) - rect.origin.x
        rect.size.height = CGFloat(bottomBorder) - rect.origin.y

        var offset = CGPoint.zero
        var finalSize = size

        DispatchQueue.main.async {
            if size.width / size.height > self.bounds.size.width / self.bounds.size.height {
                var centerX = rect.origin.x + rect.size.width / 2.0

                finalSize.height = self.bounds.size.height
                finalSize.width = size.width/size.height * finalSize.height
                centerX = finalSize.width / size.width * centerX

                offset.x = centerX - self.bounds.size.width * 0.5
                if offset.x < 0 {
                    offset.x = 0
                } else if offset.x + self.bounds.size.width > finalSize.width {
                    offset.x = finalSize.width - self.bounds.size.width
                }
                offset.x = -offset.x
            } else {
                var centerY = rect.origin.y + rect.size.height / 2.0

                finalSize.width = self.bounds.size.width
                finalSize.height = size.height / size.width * finalSize.width
                centerY = finalSize.width / size.width * centerY

                offset.y = centerY - self.bounds.size.height * CGFloat(1-0.618)
                if offset.y < 0 {
                    offset.y = 0
                } else if offset.y + self.bounds.size.height > finalSize.height {
                    finalSize.height = self.bounds.size.height
                    offset.y = finalSize.height
                }
                offset.y = -offset.y
            }
        }

        var newImage: UIImage
       
            newImage = image
        

        DispatchQueue.main.sync {
            self.image = newImage

            let layer = self.imageLayer()
            layer.contents = newImage.cgImage
            layer.frame = CGRect(x: offset.x, y: offset.y, width: finalSize.width, height: finalSize.height)
            self.didFocusOnFaces?()
        }
    }
    private func imageLayer() -> CALayer {
        if let layer = sublayer() {
            return layer
        }

        let subLayer = CALayer()
        subLayer.name = "AspectFillFaceAware"
        subLayer.actions = ["contents": NSNull(), "bounds": NSNull(), "position": NSNull()]
        layer.addSublayer(subLayer)
        return subLayer
    }
    private func removeImageLayer(image: UIImage?) {
        DispatchQueue.main.async {
            // avoid redundant layer when focus on faces for the image of cell specified in UITableView
            self.imageLayer().removeFromSuperlayer()
            self.image = image
        }
    }
    private func sublayer() -> CALayer? {
        if let sublayers = layer.sublayers {
            for layer in sublayers where layer.name == "AspectFillFaceAware" {
                return layer
            }
        }
        return nil
    }
    override open func layoutSubviews() {
           super.layoutSubviews()
        self.clipsToBounds = true
        
        if round {
            self.layer.cornerRadius = self.frame.width / 2
        } else {
            self.layer.cornerRadius = 0
        }
        
        self.layer.borderWidth = self.widthX
        self.layer.borderColor = self.color.cgColor
        self.layer.backgroundColor = self.colorBackground.cgColor
           if focusOnFaces {
               setImageAndFocusOnFaces(image: self.image)
           }
       }
}
