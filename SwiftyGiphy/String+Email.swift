//
//  String+Email.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
