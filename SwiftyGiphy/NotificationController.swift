//
//  NotificationController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-08.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
class NotificationController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var defaultLBL : UILabel!
    var notificationsData : JSON = []
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
              let shareLink = ["http://gifmemer.com/download"]
              let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
              activityVC.popoverPresentationController?.sourceView = self.view
              self.present(activityVC, animated: true, completion: nil)
          }
    @IBOutlet weak var userProfileImg: ExtensionProfile!
    
    @IBOutlet weak var userFirstNameLBL: UILabel!

    
    @IBOutlet weak var zonzLBL: UILabel!
    func numberOfSections(in tableView: UITableView) -> Int {
       return  1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.notificationsData.arrayObject !=  nil {
            if self.notificationsData.arrayObject?.count != 0 {
                return self.notificationsData.arrayObject!.count
            }else{
                return 0
            }
        }else{
            return  0
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      /*  if notificationsData[indexPath.row]["type"].stringValue == "gif" {
            if let cell = cell as? CustomNotificationVideoCell {
                cell.playLayer()
            }
        } */
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if notificationsData[indexPath.row]["type"].stringValue == "gif" {
                   if let cell = cell as? CustomNotificationVideoCell {
                       cell.removeLayer()
                   }
               }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //CustomNotificationVideoCell
    if notificationsData[indexPath.row]["type"].stringValue == "gif" {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellV", for: indexPath) as? CustomNotificationVideoCell {
            
            if notificationsData[indexPath.row]["gifId"].description != "null" {
                 var url = notificationsData[indexPath.row]["gifId"]["url"].stringValue
                        url.removeLast()
                    if url.starts(with: "192.168.1.25") {
                        url = url.deletingPrefix("192.168.1.25/")
                    }
                
               
                cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + url)!)
               
                
            }else{
                cell.gifMemeImageView.image = UIImage(named: "GifChoice")
                }
            cell.clickProfileLBL.text = Localization("checkProfile")
                if notificationsData[indexPath.row]["seen"].boolValue {
                    cell.viewBackground.backgroundColor = UIColor.clear
                }
            
                cell.messageLBL.text = notificationsData[indexPath.row]["message\(getLangue())"].stringValue
                               cell.dateLBL.text = notificationsData[indexPath.row]["gifDate"].stringValue.components(separatedBy: "T")[0]
                return cell
        }
            }else if notificationsData[indexPath.row]["type"].stringValue == "mime" {
         if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomNotificationCell {
                if notificationsData[indexPath.row]["mimeId"].description != "null" {
                    var url = notificationsData[indexPath.row]["mimeId"]["url"].stringValue
                    url.removeLast()
                    if url.starts(with: "192.168.1.25") {
                                url = url.deletingPrefix("192.168.1.25/")
                    }
                   
                    cell.gifMemeImageView.contentMode = .scaleAspectFill
                    cell.gifMemeImageView.setImage(with: URL(string: ScriptBase.Image_URL +  url  + ".jpg")!, placeholder:  UIImage(named: "memeicon"))
                    //cell.configureGif(gifUrl: U)
                    print(ScriptBase.Image_URL +  url  + ".jpg")
                    
                }else{
                    cell.gifMemeImageView.image = UIImage(named: "memeicon")
                }
            cell.clickProfileLBL.text = Localization("checkProfile")
                if notificationsData[indexPath.row]["seen"].boolValue {
                                       cell.viewBackground.backgroundColor = UIColor.clear
                                   }
                cell.messageLBL.text = notificationsData[indexPath.row]["message\(getLangue())"].stringValue
                cell.dateLBL.text = notificationsData[indexPath.row]["gifDate"].stringValue.components(separatedBy: "T")[0]
            return cell
            
            }
        }
         return UITableViewCell()
        }
       
        //return cell
     let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    func getLangue() -> String {
        var langue : String! = ""
        if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
         if Locale.currentLanguage! == .en {
               _ =   SetLanguage("English_en")
                langue = "EN"
            }else if Locale.currentLanguage! == .fr  {
                langue = "FR"
            _ = SetLanguage("French_fr")

         }else if Locale.currentLanguage! == .es{
              langue = "ES"
            _ = SetLanguage("Spanish_es")

         }else if Locale.currentLanguage! == .pt {
              langue = "POT"
            _ = SetLanguage("Portugese_pg")

         }else if Locale.currentLanguage! == .it {
            _ = SetLanguage("Italien_it")
              langue = "IT"
             }
        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguages[2]
        {
             print("Fr language")
            langue = "FR"
         _ = SetLanguage("French_fr")

        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguages[3] {
         print("Es language")

            langue = "ES"
         _ = SetLanguage("Spanish_es")
        }else if Localisator.sharedInstance.currentLanguage == arrayLanguages[4]{
         print("Pt language")

         langue = "POT"
         _ = SetLanguage("Portugese_pg")
         }else if Localisator.sharedInstance.currentLanguage == arrayLanguages[5]{
         print("It language")

         langue = "IT"
         _ = SetLanguage("Italien_it")
        }else{
         print("EN language")

         langue = "EN"
         _ = SetLanguage("English_en")
     }
        return langue
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setSeeenNotfication()
        HHTabBarView.shared.tabBarTabs[3].badgeValue = 0
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func getUser(callback: @escaping (_ messageInfo : Bool) -> Void) {
          guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            return }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                         do {
                let a = try JSON(data: dataFromString!)
        let parameters : Parameters = [
            "userId" : a["_id"].stringValue
        ]
        let headers : HTTPHeaders = [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        Alamofire.request(URL(string: ScriptBase.sharedInstance.getZonz)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
             let data = JSON(response.data ?? Data())
            print("DataJSONUSER : ",data)
            if !data["message"].exists() && data != JSON.null {
                
            UserDefaults.standard.setValue(data["user"].rawString(), forKey: "UserZonzay")
            UserDefaults.standard.synchronize()
                
              
                                   
                callback(true)
                
            }else{
                callback(false)
            }
            }
                            
                         }catch {
                            print("GfyCatTrendingController :",error.localizedDescription)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                   sessionDataTask.forEach{ $0.cancel()}
                   uploadData.forEach{ $0.cancel()}

                   downloadData.forEach{ $0.cancel()}

               }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
      
       
    }
    func setSeeenNotfication(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
               let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
               do {
               let a = try JSON(data: dataFromString!)
               let params : Parameters = [
                   "userId" : a["_id"].stringValue
                   
                   ]
               let headers : HTTPHeaders = [
               "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                           ]
               Alamofire.request(URL(string: ScriptBase.sharedInstance.seenNotifications)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                   if response.error == nil {
                    print("seeeeeen: ")
                    print(JSON(response.data))
                           }else{
                                                      
                               }
                   }
               }catch{
                   
               }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        self.getUser { (valid) in
            
       
        if UserDefaults.standard.value(forKey: "UserZonzay") != nil {
                     let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                     let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                         do {
                         let a =  try JSON(data: dataFromString!)
                             self.zonzLBL.text = a["zonz"].stringValue
                             self.userProfileImg.setImage(with: URL(string: ScriptBase.Image_URL + a["userImageURL"].stringValue), placeholder: UIImage(named: "artist"))
                             let name = a["userFirstName"].stringValue
                             if name.contains(" ") {
                          self.userFirstNameLBL.text = name.split(separator: " ")[0] + "\n" + name.split(separator: " ")[1]
                             }else{
                      self.userFirstNameLBL.text = a["userFirstName"].stringValue + "\n" + a["userLastName"].stringValue
                             }
                         }catch{
                             print("TemporaryProfile : ",error.localizedDescription)
                         }
                       
              }else{
                  
              }
             }
        self.getNotificationsFormDataBase()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getNotificationsFormDataBase(){
        

        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            
            return
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
        let a = try JSON(data: dataFromString!)
        let params : Parameters = [
            "userId" : a["_id"].stringValue
            
            ]
        let headers : HTTPHeaders = [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                    ]
        Alamofire.request(URL(string: ScriptBase.sharedInstance.myNotifications)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.error == nil {
            let data = JSON(response.data)
                print("Notif: ",data)

                if data["status"].exists() {
                   if data["status"].boolValue {
                    self.notificationsData = data["data"]
                    if self.notificationsData.arrayObject?.count == 0 {
                        self.tableView.isHidden = true
                        self.defaultLBL.text = "No Notifications"
                        self.defaultLBL.textAlignment = .center
                    }
                    self.tableView.reloadData()
                    }else{
                    self.tableView.isHidden = true
                        }
                    }
                    }else{
                                               
                        }
            }
        }catch{
            
        }
        }
    
}
