//
//  ServerUpdateController.swift
//  YLYL
//
//  Created by macbook on 2019-05-20.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

import Foundation
import UIKit
class ServerUpdateController: UIViewController {
    
    @IBOutlet weak var NewUpdateLBL : UILabel!
    @IBOutlet weak var WeBetterEverLBL : UILabel!
    @IBOutlet weak var ToBetterImproLBL : UILabel!
    @IBOutlet weak var PleaseComeBack : UILabel!
    @IBOutlet weak var CloseBTN : UIButton!

    @IBAction func closeAction(_ sender: UIButton) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NewUpdateLBL.text = Localization("NewUpdateLBL")
        WeBetterEverLBL.text = Localization("WeBetterEverLBL")

        ToBetterImproLBL.text = Localization("ToBetterImproLBL")

        PleaseComeBack.text = Localization("PleaseComeBack")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
