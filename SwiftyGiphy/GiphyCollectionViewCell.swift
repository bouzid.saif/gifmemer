//
//  SwiftyGiphyCollectionViewCell.swift
//  Pods
//
//  Created by Brendan Lee on 3/9/17.
//
//

import UIKit
import FLAnimatedImage
import SDWebImage
import SwiftyGiphy
import GfycatKit
public class GiphyCollectionViewCell: UICollectionViewCell {
    
    fileprivate(set) var imageView: FLAnimatedImageView = FLAnimatedImageView()
     var clipImageViewBottom: UIImageView! = UIImageView()
     var clipImageViewCenter: UIImageView! = UIImageView()

    var gifUrl : URL!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    fileprivate func setup() {
        
        let backgroundRoundedCornerView = RoundedCornerView()
        backgroundRoundedCornerView.translatesAutoresizingMaskIntoConstraints = false
        backgroundRoundedCornerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        backgroundRoundedCornerView.clipsToBounds = true
        
        let foregroundRoundedCornerView = RoundedCornerView()
        foregroundRoundedCornerView.translatesAutoresizingMaskIntoConstraints = false
        foregroundRoundedCornerView.backgroundColor = UIColor.lightGray
        foregroundRoundedCornerView.clipsToBounds = true
       
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        
        backgroundRoundedCornerView.addSubview(foregroundRoundedCornerView)
        foregroundRoundedCornerView.addSubview(imageView)
       
        contentView.addSubview(backgroundRoundedCornerView)
        self.clipImageViewCenter.translatesAutoresizingMaskIntoConstraints = false
        self.clipImageViewBottom.translatesAutoresizingMaskIntoConstraints = false
         contentView.addSubview(clipImageViewBottom)
        contentView.addSubview(clipImageViewCenter)
        clipImageViewBottom.isHidden = true
        clipImageViewCenter.isHidden = true
        clipImageViewBottom.image = UIImage(named: "clipIcon")
        clipImageViewCenter.image = UIImage(named: "clipIcon")
        NSLayoutConstraint.activate([
                backgroundRoundedCornerView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
                backgroundRoundedCornerView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                backgroundRoundedCornerView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
                backgroundRoundedCornerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            ])
        
        NSLayoutConstraint.activate([
                foregroundRoundedCornerView.leftAnchor.constraint(equalTo: backgroundRoundedCornerView.leftAnchor, constant: 1.0 / UIScreen.main.scale),
                foregroundRoundedCornerView.topAnchor.constraint(equalTo: backgroundRoundedCornerView.topAnchor, constant: 1.0 / UIScreen.main.scale),
                foregroundRoundedCornerView.bottomAnchor.constraint(equalTo: backgroundRoundedCornerView.bottomAnchor, constant: -(1.0 / UIScreen.main.scale)),
                foregroundRoundedCornerView.rightAnchor.constraint(equalTo: backgroundRoundedCornerView.rightAnchor, constant: -(1.0 / UIScreen.main.scale))
            ])
        
        NSLayoutConstraint.activate([
                imageView.leftAnchor.constraint(equalTo: foregroundRoundedCornerView.leftAnchor),
                imageView.topAnchor.constraint(equalTo: foregroundRoundedCornerView.topAnchor),
                imageView.bottomAnchor.constraint(equalTo: foregroundRoundedCornerView.bottomAnchor),
                imageView.rightAnchor.constraint(equalTo: foregroundRoundedCornerView.rightAnchor)
            ])
        NSLayoutConstraint.activate([
                          self.clipImageViewCenter.widthAnchor.constraint(equalToConstant: 38),
                          self.clipImageViewCenter.heightAnchor.constraint(equalToConstant: 38),
                          self.clipImageViewCenter.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0),
                          self.clipImageViewCenter.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0)
                      ])
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        imageView.sd_cancelCurrentAnimationImagesLoad()
        imageView.sd_cancelCurrentImageLoad()
        imageView.sd_setImage(with: nil)
        imageView.animatedImage = nil
        imageView.image = nil
        
    
    }
    
    /// Configure the cell for a giphy image set
    ///
    /// - Parameter imageSet: The imageset to configure the cell with
    public func configureFor(imageSet: GiphyImageSet)
    {
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
        print("imageSet.url : ", imageSet.url)
        imageView.sd_setImage(with: imageSet.url)
    }
    public func configureForGfyCat(item : GfycatMedia) {
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
       
        imageView.sd_setImage(with: item.gif2MbUrl)
    }
    public func configureForOwnGifs(url: URL) {
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
        print("URL : ",url)
        gifUrl = url
        imageView.sd_setImage(with: url)
    }
      var moviePlayer  : AVPlayer?
     var moviePlayerLayer = AVPlayerLayer()
    var videoURLForAfter : String = ""
    public func configureForOwnMemeFavorite(url : URL){
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
       
        gifUrl = url
        imageView.sd_setImage(with: url)
    }
    public func configureForOwnGifsFavorite(url: URL) {
       
        
       
         
          
           print("URL : ",url)
           gifUrl = url
        //imageView.sd_setImage(with: URL(string: url.absoluteString  + ".jpg"))
        //self.bringSubviewToFront(clipImageView)
        
        let videoURL  = URL(fileURLWithPath: url.absoluteString + ".mp4")
               
           //let playerItem = CachingPlayerItem(url: videoURL)
               //playerItem.download()
        
               let assest = AVURLAsset(url: videoURL)
            videoURLForAfter = url.absoluteString + ".mp4"
               
               moviePlayer =  AVPlayer(playerItem: AVPlayerItem(asset: assest))
               moviePlayer!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
               //moviePlayer!.automaticallyWaitsToMinimizeStalling = false
            
               moviePlayerLayer = AVPlayerLayer(player: moviePlayer)
            moviePlayerLayer.backgroundColor = UIColor.clear.cgColor
               //moviePlayerLayer.frame = self.imageView.frame
               moviePlayerLayer.name = "videoLayer"
               moviePlayerLayer.videoGravity = AVLayerVideoGravity.resize
               moviePlayerLayer.name = "Video"
               self.imageView.layer.addSublayer(moviePlayerLayer)
               
               NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: moviePlayer?.currentItem)
       }
 
    @objc func playerDidFinishPlayingt(note : Notification) {
           moviePlayer!.seek(to: CMTime.zero)
           moviePlayer!.play()
       }
   /* func updateFrame(){
       moviePlayerLayer.frame = self.imageView.bounds
        self.imageView.layoutIfNeeded()
       } */
    func playLayer(){
        if moviePlayer?.currentItem?.asset == nil {
            self.configureForOwnGifsFavorite(url: URL(string: self.videoURLForAfter)!)
        }
       /* moviePlayerLayer = AVPlayerLayer(player: moviePlayer)
        moviePlayerLayer.frame = self.imageView.frame
        moviePlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        moviePlayerLayer.name = "Video"
        self.imageView.layer.insertSublayer(moviePlayerLayer, at: 0) */
        //self.thumbImage.layer.addSublayer(moviePlayerLayer)
    //self.viewGif.layer.addSublayer(moviePlayerLayer)
        print("Will Start Playing")
       moviePlayer!.play()
        
    }
    func removeLayer(){
        
        moviePlayer!.pause()
        moviePlayer?.replaceCurrentItem(with: nil)
        
        NotificationCenter.default.removeObserver(self)
        guard let layers = self.imageView.layer.sublayers else {
            return
        }
        if layers.count != 0 {
          
            for layer in layers {
                if layer.name == "Video" {
                    
                   layer.removeFromSuperlayer()
                }
            }
        //self.moviePlayerLayer.removeFromSuperlayer()
        }
    }
    public func activateClipKeyboard (){
        clipImageViewBottom.isHidden = false
        clipImageViewBottom.alpha = 1
    }
    public func deactivateClipKeyboard(){
        clipImageViewBottom.isHidden = true
    }
    var doLayout = true
    public override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        self.moviePlayerLayer.frame =   self.imageView.bounds
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
      
         clipImageViewBottom.frame = CGRect(x: self.contentView.frame.width - 40, y: self.contentView.frame.height - 40, width: 38, height: 38)
        
        
    }
    public func animateClipKeyboard(){
        print("animation")
       
        //let frame = CGRect(x: (self.imageView.frame.width / 2)  - 56, y: (self.imageView.frame.height / 2) - 56, width: 38, height: 38)
        //self.clipImageView.frame = frame
        //
        self.clipImageViewCenter.alpha = 0
        self.clipImageViewCenter.isHidden = false
       // UIView.animate(withDuration: 0.8, animations: {
            self.clipImageViewBottom.alpha = 0
            self.clipImageViewCenter.alpha = 1
       /// }) { (complete) in
            self.clipImageViewBottom.isHidden = true
            print("animationDone")
            
       // }
    }
    public func deAnimateClipKeyboard(){
           print("animation")
          
           //let frame = CGRect(x: (self.imageView.frame.width / 2)  - 56, y: (self.imageView.frame.height / 2) - 56, width: 38, height: 38)
           //self.clipImageView.frame = frame
           //
           self.clipImageViewBottom.alpha = 0
           self.clipImageViewBottom.isHidden = false
          // UIView.animate(withDuration: 0.8, animations: {
               self.clipImageViewCenter.alpha = 0
               self.clipImageViewBottom.alpha = 1
          // }) { (complete) in
               self.clipImageViewCenter.isHidden = true
               print("animationDone")
               
          // }
       }
}
public class GiphyCollectionViewCategoryCell: UICollectionViewCell {
    
    fileprivate(set) var imageView: FLAnimatedImageView = FLAnimatedImageView()
    fileprivate(set) var titleCategory : UILabel = UILabel()
    var categoryTitle: String = ""
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    fileprivate func setup() {
        
        let backgroundRoundedCornerView = RoundedCornerView()
        backgroundRoundedCornerView.translatesAutoresizingMaskIntoConstraints = false
        backgroundRoundedCornerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        backgroundRoundedCornerView.clipsToBounds = true
        
        let foregroundRoundedCornerView = RoundedCornerView()
        foregroundRoundedCornerView.translatesAutoresizingMaskIntoConstraints = false
        foregroundRoundedCornerView.backgroundColor = UIColor.lightGray
        foregroundRoundedCornerView.clipsToBounds = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        
        backgroundRoundedCornerView.addSubview(foregroundRoundedCornerView)
        let mask = UIView()
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        mask.clipsToBounds = true
        foregroundRoundedCornerView.addSubview(imageView)
        foregroundRoundedCornerView.addSubview(mask)
        
        titleCategory.translatesAutoresizingMaskIntoConstraints = false
        //titleCategory.clipsToBounds = true
        titleCategory.shadowOffset = CGSize(width: 0, height: 5)
        titleCategory.shadowColor = UIColor.black.withAlphaComponent(0.8)
        titleCategory.font = UIFont.init(name: "seguisb", size: 22)
        titleCategory.adjustsFontSizeToFitWidth = true
        titleCategory.textAlignment = .center
        titleCategory.textColor = UIColor.white
        
        contentView.addSubview(backgroundRoundedCornerView)
        contentView.addSubview(titleCategory)
        NSLayoutConstraint.activate([
                backgroundRoundedCornerView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
                backgroundRoundedCornerView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                backgroundRoundedCornerView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
                backgroundRoundedCornerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            ])
        
        NSLayoutConstraint.activate([
                foregroundRoundedCornerView.leftAnchor.constraint(equalTo: backgroundRoundedCornerView.leftAnchor, constant: 1.0 / UIScreen.main.scale),
                foregroundRoundedCornerView.topAnchor.constraint(equalTo: backgroundRoundedCornerView.topAnchor, constant: 1.0 / UIScreen.main.scale),
                foregroundRoundedCornerView.bottomAnchor.constraint(equalTo: backgroundRoundedCornerView.bottomAnchor, constant: -(1.0 / UIScreen.main.scale)),
                foregroundRoundedCornerView.rightAnchor.constraint(equalTo: backgroundRoundedCornerView.rightAnchor, constant: -(1.0 / UIScreen.main.scale))
            ])
        
        NSLayoutConstraint.activate([
                imageView.leftAnchor.constraint(equalTo: foregroundRoundedCornerView.leftAnchor),
                imageView.topAnchor.constraint(equalTo: foregroundRoundedCornerView.topAnchor),
                imageView.bottomAnchor.constraint(equalTo: foregroundRoundedCornerView.bottomAnchor),
                imageView.rightAnchor.constraint(equalTo: foregroundRoundedCornerView.rightAnchor)
            ])
        NSLayoutConstraint.activate([
            mask.leftAnchor.constraint(equalTo: foregroundRoundedCornerView.leftAnchor),
            mask.topAnchor.constraint(equalTo: foregroundRoundedCornerView.topAnchor),
            mask.bottomAnchor.constraint(equalTo: foregroundRoundedCornerView.bottomAnchor),
            mask.rightAnchor.constraint(equalTo: foregroundRoundedCornerView.rightAnchor)
        
        
        ])
        NSLayoutConstraint.activate([
            titleCategory.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15),
            titleCategory.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15),
        titleCategory.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        titleCategory.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        
        ])
       
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        imageView.sd_cancelCurrentAnimationImagesLoad()
        imageView.sd_cancelCurrentImageLoad()
        imageView.sd_setImage(with: nil)
        imageView.animatedImage = nil
        imageView.image = nil
    
    }
    
    /// Configure the cell for a giphy image set
    ///
    /// - Parameter imageSet: The imageset to configure the cell with
    public func configureFor(imageSet: GiphyImageSet)
    {
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
        print("imageSet.url : ", imageSet.url)
        imageView.sd_setImage(with: imageSet.url)
    }
    public func configureForGfyCat(item : GfycatMedia) {
        imageView.sd_cacheFLAnimatedImage = false
        imageView.sd_setShowActivityIndicatorView(true)
        imageView.sd_setIndicatorStyle(.gray)
       imageView.sd_setImage(with: item.gif2MbUrl) { (image, error, cache, url) in
                  self.titleCategory.text = self.categoryTitle
              }
        //imageView.sd_setImage(with: item.gif2MbUrl)
    }
    public func configureCategory(title:String) {
        
        categoryTitle = title.uppercased()
    }
   
}
public class GifFavoriteCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var imageViewGif: UIImageView!
    var clipImageViewBottom: UIImageView! = UIImageView()
        var clipImageViewCenter: UIImageView! = UIImageView()
 var doLayout = true
       var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var player : AVPlayer?
       var videoURL: String? {
           didSet {
               if let videoURL = videoURL {
                    print("VideoURL: ",videoURL)
                   ASVideoPlayerControllerOwned.sharedVideoPlayer.setupVideoFor(url: videoURL)
               }
               videoLayer.isHidden = videoURL == nil
           }
       }
    override public func awakeFromNib() {
           super.awakeFromNib()
           imageViewGif.layer.cornerRadius = 5
           imageViewGif.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
           imageViewGif.clipsToBounds = true
           imageViewGif.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
           imageViewGif.layer.borderWidth = 0.5
           videoLayer.backgroundColor = UIColor.clear.cgColor
           videoLayer.videoGravity = AVLayerVideoGravity.resize
           imageViewGif.layer.insertSublayer(videoLayer, at: 0)
           self.clipImageViewCenter.translatesAutoresizingMaskIntoConstraints = false
           
            contentView.addSubview(clipImageViewBottom)
           contentView.addSubview(clipImageViewCenter)
           clipImageViewBottom.isHidden = true
           clipImageViewCenter.isHidden = true
           clipImageViewBottom.image = UIImage(named: "clipIcon")
           clipImageViewCenter.image = UIImage(named: "clipIcon")
        NSLayoutConstraint.activate([
                                 self.clipImageViewCenter.widthAnchor.constraint(equalToConstant: 38),
                                 self.clipImageViewCenter.heightAnchor.constraint(equalToConstant: 38),
                                 self.clipImageViewCenter.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0),
                                 self.clipImageViewCenter.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0)
                             ])
       
       }
    var gifIsReady = false
    var gifIsDisplayed = false
    var defaultFrame : CGRect = CGRect.zero
    var gifUrl : URL!
    
    public override func prepareForReuse() {
        self.removeLayer()
        super.prepareForReuse()
      /* imageViewGif.sd_cancelCurrentAnimationImagesLoad()
       imageViewGif.sd_cancelCurrentImageLoad()
       imageViewGif.sd_setImage(with: nil)
       imageViewGif.animatedImage = nil
       imageViewGif.image = nil */
    
        
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
        videoLayer.frame = CGRect(x: 0, y: 0, width: self.imageViewGif.frame.width, height: self.imageViewGif.frame.height)
        clipImageViewBottom.frame = CGRect(x: self.contentView.frame.width - 40, y: self.contentView.frame.height - 40, width: 38, height: 38)
    }
    func configureGif(gifUrl : URL ) {
       self.gifUrl = URL(string: gifUrl.absoluteString + ".mp4")
        self.videoURL = gifUrl.absoluteString + ".mp4"
        self.imageViewGif.sd_setImage(with: URL(string: gifUrl.absoluteString + ".jpg"), completed: nil)
        player = AVPlayer(playerItem: AVPlayerItem(url:  URL(string: gifUrl.absoluteString + ".mp4")!))
          player!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
       /* videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        imageViewGif.layer.addSublayer(videoLayer) */
        guard let layers = self.imageViewGif.layer.sublayers else {
               return
           }
           if layers.count != 0 {
                var found = false
               for layer in layers {
                   if layer.name == "Video" {
                       found = true
                    
                   }
               }
            if found == false{
                videoLayer =  AVPlayerLayer()
             videoLayer.backgroundColor = UIColor.clear.cgColor
            videoLayer.videoGravity = AVLayerVideoGravity.resize
             imageViewGif.layer.insertSublayer(videoLayer, at: 0)
            }
           //self.moviePlayerLayer.removeFromSuperlayer()
           }else{
        videoLayer =  AVPlayerLayer()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        imageViewGif.layer.insertSublayer(videoLayer, at: 0)
            }
        self.videoLayer.player = player
        self.videoLayer.name = "Video"
        DispatchQueue.main.async {
            self.player?.play()
        }
         NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
      
        
        
    }
    @objc func playerDidFinishPlayingt(note : Notification) {
        player!.seek(to: CMTime.zero)
        player!.play()
    }
    public func activateClipKeyboard (){
          clipImageViewBottom.isHidden = false
          clipImageViewBottom.alpha = 1
      }
      public func deactivateClipKeyboard(){
          clipImageViewBottom.isHidden = true
      }
  public func animateClipKeyboard(){
         print("animation")
        
         //let frame = CGRect(x: (self.imageView.frame.width / 2)  - 56, y: (self.imageView.frame.height / 2) - 56, width: 38, height: 38)
         //self.clipImageView.frame = frame
         //
         self.clipImageViewCenter.alpha = 0
         self.clipImageViewCenter.isHidden = false
        // UIView.animate(withDuration: 0.8, animations: {
             self.clipImageViewBottom.alpha = 0
             self.clipImageViewCenter.alpha = 1
        /// }) { (complete) in
             self.clipImageViewBottom.isHidden = true
             print("animationDone")
             
        // }
     }
     public func deAnimateClipKeyboard(){
            print("animation")
           
            //let frame = CGRect(x: (self.imageView.frame.width / 2)  - 56, y: (self.imageView.frame.height / 2) - 56, width: 38, height: 38)
            //self.clipImageView.frame = frame
            //
            self.clipImageViewBottom.alpha = 0
            self.clipImageViewBottom.isHidden = false
           // UIView.animate(withDuration: 0.8, animations: {
                self.clipImageViewCenter.alpha = 0
                self.clipImageViewBottom.alpha = 1
           // }) { (complete) in
                self.clipImageViewCenter.isHidden = true
                print("animationDone")
                
           // }
        }
 func removeLayer(){
     
     player?.pause()
     player?.replaceCurrentItem(with: nil)
     
     NotificationCenter.default.removeObserver(self)
     guard let layers = self.imageViewGif.layer.sublayers else {
         return
     }
     if layers.count != 0 {
       
         for layer in layers {
             if layer.name == "Video" {
                 print("layer removed")
                layer.removeFromSuperlayer()
             }
         }
     //self.moviePlayerLayer.removeFromSuperlayer()
     }
 }
    
}
