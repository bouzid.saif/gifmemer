
//
//  GfyCatTrendingController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-27.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import SwiftyGiphy
import AVKit
import GfycatKit
import Alamofire
import SwiftyJSON
import AZSearchView
import MapleBacon
class GfyCatTrendingController: appleDetection {
    var cursor = ""
    var cursorSearch = ""
    var isSearching = false
    var isLoading: Bool = false
    var firstLoad = true
    var imagePicker : UIImagePickerController!
    var imagePicked: UIImage!
    var collectionSearchBar: UISearchBar!
    fileprivate var buffer:NSMutableData = NSMutableData()

    @IBOutlet weak var zonzLBL: UILabel!
     @IBOutlet weak var alrightBTN: UIButton!
    @IBOutlet weak var accountVerifiedView : UIView!
    @IBOutlet weak var backgroundPopup: UIImageView!
    @IBOutlet weak var topBarConstraint: NSLayoutConstraint!
    @IBOutlet weak var topBarView: UIView!
    fileprivate var currentGifs: [GfycatMediaCustom]?  = []
    @IBOutlet weak var collection : UICollectionView!
    @IBOutlet weak var collectionViewLayout : SwiftyGiphyGridLayout!
    @IBOutlet weak var searchContainer : UIView!
    fileprivate var currentGifsForSearch: [GfycatMediaCustom]?  = []
    
    @IBOutlet weak var profilePicture : ExtensionProfile!
    @IBOutlet weak var profileNameLabel : UILabel!
    var refreshControl:UIRefreshControl?
    var isGifOrMime : Bool = true
    @IBOutlet weak var trendingLBL : UILabel!
    @IBOutlet weak var searchLBL : UILabel!
    
    @IBOutlet weak var ChoiceView: UIView!
    
    @IBOutlet weak var cancelChoiceBTN: UIButton!
    
    @IBOutlet weak var gifMemeSuccessView: UIView!

    @IBOutlet weak var weareProcessingLBL: UILabel!
    
    @IBOutlet weak var youlGetNotificationLBL: UILabel!
    
    @IBOutlet weak var pleaseMakeSureNotifLBL: UILabel!
    var item : GfycatMediaCustom!
    
    @IBOutlet weak var addPhotoPresentationView: UIView!
    
    @IBOutlet weak var weNeedFaceLBL: UILabel!
    
    @IBOutlet weak var addPhotoView: UIView!
    @IBOutlet weak var addPhotoLBL: UILabel!
        
    @IBOutlet weak var thisAccountNotLBL: UILabel!
    
    @IBOutlet weak var clickResendLBL: UILabel!
    @IBOutlet weak var resendBTN: UIButton!
   
    @IBAction func resendAction(_ sender: UIButton) {
        if sender.title(for: .normal) != Localization("Login/Reg") {
          guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
              return }
          let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                           do {
                  let a = try JSON(data: dataFromString!)
           let parameters : Parameters = [
                             "email" : a["userEmail"].stringValue
                         ]
           let headers : HTTPHeaders = [
           "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                   ]
           Alamofire.request(URL(string: ScriptBase.sharedInstance.resendEmail)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                       let data = JSON(response.data ?? Data())
               if data["message"].stringValue != "invalid" {
                   UIView.animate(withDuration: 0.3, animations: {
                             self.accountVerifiedView.alpha = 0
                         }) { (complete) in
                             self.accountVerifiedView.isHidden = true
                         }
               }
                               
                               }
                           }catch{
                               
           }
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                      self.accountVerifiedView.alpha = 0
                  }) { (complete) in
                      self.accountVerifiedView.isHidden = true
                  }

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AuthentificationController") as! AuthentificationController
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
       }
    @IBAction func closeUserNotActivated (_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.accountVerifiedView.alpha = 0
        }) { (complete) in
            self.accountVerifiedView.isHidden = true
        }
    }
    
    @IBAction func takeAPhotoAction(_ sender: UITapGestureRecognizer) {
        if self.isGifOrMime {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomCameraPickController") as! CustomCameraPickController
               vc.modalPresentationStyle = .fullScreen
               vc.item = self.item
               vc.isGifOrMime = self.isGifOrMime
               self.collection.isUserInteractionEnabled = true
               self.present(vc, animated: true, completion: nil)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomCameraPickController") as! CustomCameraPickController
                          vc.modalPresentationStyle = .fullScreen
                    
                          vc.item = self.item
                          vc.isGifOrMime = self.isGifOrMime
                   self.collection.isUserInteractionEnabled = true

                          self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func fromLibraryAction(_ sender: UITapGestureRecognizer) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    @IBOutlet weak var pleaseMakeSureLBL: UILabel!
    
    @IBAction func alrightAction (_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.gifMemeSuccessView.alpha = 0
        }, completion: { complete in
            self.gifMemeSuccessView.isHidden = true
             self.backgroundPopup.isHidden = true
        })
    }
    @IBAction func GifChoosedAction(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.ChoiceView.alpha = 0
        }) { (complete) in
            self.ChoiceView.isHidden = true
        }
       
        self.isGifOrMime = true
         guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
        return
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
        let a = try JSON(data: dataFromString!)
            if a["userImageURL"].exists() {
                
                self.changePhotoView.alpha = 0
                self.changePhotoView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.changePhotoView.alpha = 1
                }) { (complete) in
                    
                }
            }else{
                self.weNeedFaceLBL.text = Localization("weNeedFaceLBL")
                self.addPhotoLBL.text = Localization("addPhotoLBL")
                self.pleaseMakeSureLBL.text = Localization("MessageFacePick")
                self.addPhotoPresentationView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.addPhotoPresentationView.alpha = 1
                }) { (completeOne) in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.addPhotoView.isHidden = false
                        
                        UIView.animate(withDuration: 0.3, animations: {
                             self.addPhotoPresentationView.alpha = 0
                            self.addPhotoView.alpha = 1
                        }) { (completeTwo) in
                        self.addPhotoPresentationView.isHidden = true
                        }
                    }
                }
            }
            
        }catch{
            print(error.localizedDescription)
        }
    }
    
    @IBAction func MemeChoosedAction(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
                   self.ChoiceView.alpha = 0
               }) { (complete) in
                   self.ChoiceView.isHidden = true
               }
               self.isGifOrMime = false
        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
        return
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
        let a = try JSON(data: dataFromString!)
            if a["userImageURL"].exists() {
                self.changePhotoView.alpha = 0
                self.changePhotoView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.changePhotoView.alpha = 1
                }) { (complete) in
                    
                }
            }else{
                self.weNeedFaceLBL.text = Localization("weNeedFaceLBL")
                self.addPhotoLBL.text = Localization("addPhotoLBL")
                self.pleaseMakeSureLBL.text = Localization("MessageFacePick")
                self.addPhotoPresentationView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.addPhotoPresentationView.alpha = 1
                }) { (completeOne) in
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.addPhotoView.isHidden = false
                        
                        UIView.animate(withDuration: 0.3, animations: {
                             self.addPhotoPresentationView.alpha = 0
                            self.addPhotoView.alpha = 1
                        }) { (completeTwo) in
                        self.addPhotoPresentationView.isHidden = true
                        }
                    }
                }
            }
            
        }catch{
            print(error.localizedDescription)
        }
    }
    @IBAction func cancelChoiceAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.ChoiceView.alpha = 0
        }) { (complete) in
            self.collection.isUserInteractionEnabled = true
            self.ChoiceView.isHidden = true
           self.backgroundPopup.isHidden = true

        }
    }
    //MARK: Change picture View
    func configureChangePicture(){
        self.useActualPhotoLBL.text = Localization("useActual")
        self.yesActualBTN.setTitle(Localization("Yes"), for: .normal)
        self.changeActualBTN.setTitle(Localization("change"), for: .normal)
    }
    @IBOutlet weak var changePhotoView: UIView!
    
    @IBOutlet weak var useActualPhotoLBL: UILabel!
    @IBOutlet weak var actualPhotoIMG: RoundedUIImageView!
    
    @IBOutlet weak var yesActualBTN: UIButton!
    @IBAction func closeAll( _ sender: UITapGestureRecognizer) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.ChoiceView.alpha = 0
            self.addPhotoPresentationView.alpha = 0
            self.addPhotoView.alpha = 0
            self.gifMemeSuccessView.alpha = 0
            self.changePhotoView.alpha = 0
            

        }) { (complete) in
            self.ChoiceView.isHidden = true
             self.addPhotoPresentationView.isHidden = true
             self.addPhotoView.isHidden = true
             self.gifMemeSuccessView.isHidden = true
             self.changePhotoView.isHidden = true
            self.backgroundPopup.isHidden = true
        }
    }
    @IBAction func yesActualAction(_ sender: UIButton) {
        
        if isGifOrMime {
            self.traitGif(collection: self.collection, gifId: self.item.gfyId, gifUrl: self.item.mpgUrl.absoluteString, nameGif: self.item.gfyName)
                       }else{
            self.traitMime(collection: self.collection, mimeId: self.item.gfyId, mimeUrl: self.item.posterUrl.absoluteString, nameGif: self.item.gfyName, gifWidth: self.item.width,gifHeight:  self.item.height)
                       }
    }
    @IBAction func changeActualAction(_ sender: UIButton) {
       self.addPhotoView.alpha = 0
        self.addPhotoView.isHidden = false
            self.addPhotoLBL.text = Localization("addPhotoLBL")
            self.pleaseMakeSureLBL.text = Localization("MessageFacePick")
            UIView.animate(withDuration: 0.3, animations: {
            self.changePhotoView.alpha = 0
                self.addPhotoView.alpha = 1
            }) { (completeTwo) in
            self.changePhotoView.isHidden = true
            }
        
    }
    
    @IBOutlet weak var changeActualBTN: UIButton!
    @IBAction func createSearchBar(_ sender: UITapGestureRecognizer) {
        if isSearching == false {
        collectionSearchBar = UISearchBar(frame: self.searchContainer.frame )
        collectionSearchBar.delegate = self
        self.view.addSubview(collectionSearchBar)
        self.collectionSearchBar.frame = self.searchContainer.frame
        UIView.animate(withDuration: 0.3) {
            self.collectionSearchBar.layoutIfNeeded()
        }
            self.collectionSearchBar.becomeFirstResponder()
        }
    }
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
           let shareLink = ["http://gifmemer.com/download"]
           let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
           activityVC.popoverPresentationController?.sourceView = self.view
           self.present(activityVC, animated: true, completion: nil)
       }
    @objc func configureViewFromLocalisation(){
        trendingLBL.text = Localization("trending")
        searchLBL.text = Localization("search")
        self.cancelChoiceBTN.setTitle(Localization("cancel"), for: .normal)
        self.alrightBTN.setTitle(Localization("alright"), for: .normal)
        configureChangePicture()
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    @objc func userNotActivated(_ notification : Notification) {
     DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         if self.isVisible {
            self.intiAccountNotActivated()
                if self.accountVerifiedView.alpha == 0 {
                    self.accountVerifiedView.isHidden = false
                    UIView.animate(withDuration: 0.3, animations: {
                        self.accountVerifiedView.alpha = 1
                    }, completion: nil)
                }
            }
     }
    
     }
    @objc func userLoginFlow(_ notification : Notification) {
     DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
         if self.isVisible {
            self.thisAccountNotLBL.text = Localization("NoAccountDetected")
            self.clickResendLBL.text = Localization("NoAccountMessage")
            self.resendBTN.setTitle(Localization("Login/Reg"), for: .normal)
                if self.accountVerifiedView.alpha == 0 {
                    self.accountVerifiedView.isHidden = false
                    UIView.animate(withDuration: 0.3, animations: {
                        self.accountVerifiedView.alpha = 1
                    }, completion: nil)
                }
            }
     }
    
     }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        intiAccountNotActivated()
        //self.navigationController?.navigationBar.removeFromSuperview()
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentSuccesGifMime(_:)), name: NSNotification.Name.init("presentRes"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userNotActivated(_:)), name: NSNotification.Name.init("UserActivation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userLoginFlow(_:)), name: NSNotification.Name.init("UserLogin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userLoggedIn(_:)), name: NSNotification.Name.init("UserLoggedIn"), object: nil)
               configureViewFromLocalisation()
        collection.delegate = self
        collection.dataSource = self
        collectionViewLayout.delegate = self
      
        print("viewDidLoad")
        getTrendingGifs()
    }
    @objc func presentSuccesGifMime(_ notif : Notification){
        self.changePhotoView.isHidden = true
        self.changePhotoView.alpha = 0
        self.addPhotoPresentationView.isHidden = true
        self.addPhotoPresentationView.alpha = 0
        self.addPhotoView.isHidden = true
        self.addPhotoView.alpha = 0
         if let _ = notif.object as? GfycatMedia {
            if isGifOrMime {
                self.weareProcessingLBL.text = Localization("weAreProcessingGif")
                self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                self.gifMemeSuccessView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.gifMemeSuccessView.alpha = 1
                }, completion: nil)
            }else{
                self.weareProcessingLBL.text = Localization("weAreProcessingMime")
                self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                self.gifMemeSuccessView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.gifMemeSuccessView.alpha = 1
                }, completion: nil)
            }
        }
    }
    @objc func userLoggedIn(_ sender: Notification) {
        DispatchQueue.main.async {
            guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AuthentificationController") as! AuthentificationController
                vc.modalPresentationStyle = .overCurrentContext
                
                self.present(vc, animated: true, completion: nil)
                DispatchQueue.main.async {
                    self.topBarConstraint.constant = 0
                    self.topBarView.isHidden = true
                    self.view.layoutIfNeeded()
                    HomeTabBarController.shared.hhTabBarView.viewHide.isHidden = false
                }
                return
            }
            HomeTabBarController.shared.hhTabBarView.lockTabIndexes = []

            HomeTabBarController.shared.hhTabBarView.unlockAllTabs()
            self.getUser()
                         
                  let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                  do {
                        let a = try JSON(data: dataFromString!)
                      let firstName = a["userFirstName"].stringValue
                      print(a)
                    self.zonzLBL.text = a["zonz"].stringValue

                      if firstName != "" {
                          self.profileNameLabel.text = firstName
                      }else{
                          self.profileNameLabel.text = ""
                      }
                      
                      let profilePic = a["userImageURL"].stringValue
                      if profilePic != ""{
                        self.actualPhotoIMG.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                          self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                          
                      }
                  }catch{
                      print("GfyCatTrendingController One: ",error.localizedDescription)
                  }
            DispatchQueue.main.async {
                self.topBarConstraint.constant = 40
                self.topBarView.isHidden = false
                self.view.layoutIfNeeded()
                HomeTabBarController.shared.hhTabBarView.viewHide.isHidden = true
            }
        }
    }
    func getUser() {
          guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            return }
        print("ab: ",ab)
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                         do {
                let a = try JSON(data: dataFromString!)
        let parameters : Parameters = [
            "userId" : a["_id"].stringValue
        ]
        let headers : HTTPHeaders = [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        Alamofire.request(URL(string: ScriptBase.sharedInstance.getZonz)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
             let data = JSON(response.data ?? Data())
            print("getting user : ", data)
            if !data["message"].exists() && data != JSON.null {
                
            UserDefaults.standard.setValue(data["user"].rawString(), forKey: "UserZonzay")
            UserDefaults.standard.synchronize()
                let profilePic = a["userImageURL"].stringValue
                                    if profilePic != ""{
                                        self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                                        self.actualPhotoIMG.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                                        
                                    }
            }
            }
                            
                         }catch {
                            print("GfyCatTrendingController Two:",error.localizedDescription)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                          sessionDataTask.forEach{ $0.cancel()}
                          uploadData.forEach{ $0.cancel()}

                          downloadData.forEach{ $0.cancel()}

                      }
        UIView.animate(withDuration: 0.3, animations: {
            self.accountVerifiedView.alpha = 0
        }) { (complete) in
            self.accountVerifiedView.isHidden = true
        }
    }
    func intiAccountNotActivated(){
        
        thisAccountNotLBL.text = Localization("thisAccountNotLBL")
        clickResendLBL.text = Localization("clickResendLBL")
        resendBTN.setTitle(Localization("resend"), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("viewWillAppear")
        
        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
           
           // let vc = self.storyboard?.instantiateViewController(withIdentifier: "AuthentificationController") as! AuthentificationController
           // vc.modalPresentationStyle = .overCurrentContext
            
          //  self.present(vc, animated: true, completion: nil)
            DispatchQueue.main.async {
                self.topBarConstraint.constant = 0
                self.topBarView.isHidden = true
                self.view.layoutIfNeeded()
          //      HomeTabBarController.shared.hhTabBarView.viewHide.isHidden = false
            }
            return
        }
        HomeTabBarController.shared.hhTabBarView.lockTabIndexes = []
        HomeTabBarController.shared.hhTabBarView.unlockAllTabs()
       self.getUser()

        
                     
                     
              let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
              do {
                    let a = try JSON(data: dataFromString!)
                  let firstName = a["userFirstName"].stringValue
                  self.zonzLBL.text = a["zonz"].stringValue
                  print(a)
                  if firstName != "" {
                      self.profileNameLabel.text = firstName
                  }else{
                      self.profileNameLabel.text = ""
                  }
                  
                  let profilePic = a["userImageURL"].stringValue
                  if profilePic != ""{
                      self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                    self.actualPhotoIMG.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                      
                  }
              }catch{
                  print("GfyCatTrendingController Three: ",error.localizedDescription)
              }
        if firstLoad == false && self.currentGifs!.count == 0 {
            print("gettingGifs")
            self.cursor = ""
            self.cursorSearch = ""
            self.getTrendingGifs()
        }
    }
    func traitGif(collection:UICollectionView, gifId:String, gifUrl: String,nameGif : String){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "gifId" : gifId,
                "gifUrl" : gifUrl,
                "name" : nameGif
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addGif, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
             if data["status"].boolValue {
                if self.isGifOrMime {
                               self.weareProcessingLBL.text = Localization("weAreProcessingGif")
                               self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                               self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                               self.gifMemeSuccessView.isHidden = false
                                self.changePhotoView.alpha = 0
                    self.changePhotoView.isHidden = true
                               UIView.animate(withDuration: 0.3, animations: {
                                   self.gifMemeSuccessView.alpha = 1
                               }, completion: nil)
                           }else{
                               self.weareProcessingLBL.text = Localization("weAreProcessingMime")
                               self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                               self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                               self.gifMemeSuccessView.isHidden = false
                                        self.changePhotoView.alpha = 0
                                       self.changePhotoView.isHidden = true
                               UIView.animate(withDuration: 0.3, animations: {
                                   self.gifMemeSuccessView.alpha = 1
                               }, completion: nil)
                           }
                 collection.isUserInteractionEnabled = true

                 /*let alert = UIAlertController(title: "Processing GIF", message: "We are processing your selected gif, we will notify you as soon as it's finished", preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                 self.present(alert, animated: true, completion: nil) */
                 
             }else{
                 collection.isUserInteractionEnabled = true

                 let alert = UIAlertController(title: "Processing GIF", message: data["message"].stringValue, preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                 self.present(alert, animated: true, completion: nil)
             }
             
               
            }
            
        }catch {
            
        }
    }
    func traitMime(collection:UICollectionView, mimeId:String, mimeUrl: String,nameGif : String,gifWidth : String, gifHeight: String){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "mimeId" : mimeId,
                "mimeUrl" : mimeUrl,
                "name" : nameGif,
                "gifHeight" : gifWidth,
                "gifWidth" : gifHeight
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addMime, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
             if data["status"].boolValue {
                if self.isGifOrMime {
                           self.weareProcessingLBL.text = Localization("weAreProcessingGif")
                           self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                           self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                           self.gifMemeSuccessView.isHidden = false
                            self.changePhotoView.alpha = 0
                            self.changePhotoView.isHidden = true
                           UIView.animate(withDuration: 0.3, animations: {
                               self.gifMemeSuccessView.alpha = 1
                           }, completion: nil)
                       }else{
                           self.weareProcessingLBL.text = Localization("weAreProcessingMime")
                           self.youlGetNotificationLBL.text = Localization("youllgetNotification")
                           self.pleaseMakeSureNotifLBL.text = Localization("pleaseMakeSureNotification")
                           self.gifMemeSuccessView.isHidden = false
                                    self.changePhotoView.alpha = 0
                                   self.changePhotoView.isHidden = true
                           UIView.animate(withDuration: 0.3, animations: {
                               self.gifMemeSuccessView.alpha = 1
                           }, completion: nil)
                       }
                 collection.isUserInteractionEnabled = true

                // let alert = UIAlertController(title: "Processing Mime", message: "We are processing your selected mime, we will notify you as soon as it's finished", preferredStyle: .alert)
                // let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                // alert.addAction(actionOK)
                 //self.present(alert, animated: true, completion: nil)
                 
             }else{
                 collection.isUserInteractionEnabled = true

                 let alert = UIAlertController(title: "Processing Mime", message: data["message"].stringValue, preferredStyle: .alert)
                 let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                 alert.addAction(actionOK)
                 self.present(alert, animated: true, completion: nil)
             }
             
               
            }
            
        }catch {
            
        }
    }
    func getTrendingGifs(){
        var tempGifs : [GfycatMediaCustom] = []
        var params : Parameters = [:]
        
        if cursor != ""  && cursor != "null" {
           // addition = "&cursor=" + cursor
            params = [
                "cursor" : cursor
            ]
        }else{
            params = [:]
        }
    Alamofire.request(URL(string:ScriptBase.sharedInstance.gifsTrending)!, method: .post,parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
            self.firstLoad = false
                    print("response.data: ", response.response?.statusCode)
                 var dataParser = JSON(response.data)
                  print("*************")
                  print(dataParser)
                  dataParser = dataParser["data"]
                  if dataParser["gfycats"].arrayObject != nil {
                      if dataParser["gfycats"].arrayObject?.count != 0 {
                        self.cursor = dataParser["cursor"].stringValue
                          for i in 0...((dataParser["gfycats"].arrayObject?.count)! - 1) {
                             
                              //tempGifs.append(GfycatMedia(gfyId: dataParser["gfycats"][i]["gfyId"].stringValue))
                            if ( dataParser["gfycats"][i]["nsfw"].stringValue == "0") {
                                let tempX = GfycatMediaCustom(info: dataParser["gfycats"][i].rawValue as! [AnyHashable : Any])
                                                   tempX.width = String(dataParser["gfycats"][i]["width"].intValue)
                                                   tempX.height = String(dataParser["gfycats"][i]["height"].intValue)
                              tempGifs.append(tempX)
                            }
                          }
                      }
                  }
            
            self.currentGifs?.append(contentsOf: tempGifs)
            self.collection.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.isLoading = false
                }
              }
    }
}
// MARK: - SwiftyGiphyGridLayoutDelegate
extension GfyCatTrendingController: SwiftyGiphyGridLayoutDelegate {

    public func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat
    {
        if isSearching == false {
        guard let imageSet = currentGifs?[indexPath.row] else {
            return 0.0
        }
             return AVMakeRect(aspectRatio: CGSize(width: imageSet.size.width, height: imageSet.size.height), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }else{
            guard let imageSet = currentGifsForSearch?[indexPath.row] else {
                      return 0.0
                  }
                       return AVMakeRect(aspectRatio: CGSize(width: imageSet.size.width, height: imageSet.size.height), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }
        
       
    }
}
// MARK: - UICollectionViewDataSource
extension GfyCatTrendingController: UICollectionViewDataSource {

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching == false {
        if currentGifs!.count != 0 {
            print("number of items : ",currentGifs!.count)
        }
        return currentGifs?.count ?? 0
        }else{
            if currentGifsForSearch!.count != 0 {
                       print("number of items Search: ",currentGifsForSearch!.count)
                   }
                   return currentGifsForSearch?.count ?? 0
        }
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSwiftyGiphyCollectionViewCell, for: indexPath) as! GiphyCollectionViewCell
        if isSearching == false {
        if let _ = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout, let imageSet = currentGifs?[indexPath.row]
        {
            
            
            cell.configureForGfyCat(item: imageSet)
        }
        }else{
            if let _ = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout, let imageSet = currentGifsForSearch?[indexPath.row]
            {
                
                
                cell.configureForGfyCat(item: imageSet)
            }
        }

        return cell
    }
}
// MARK: - UICollectionViewDelegate
extension GfyCatTrendingController: UICollectionViewDelegate {

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else {
            
            //HHTabBarView.shared.selectTabAtIndex(withIndex: 2)
            if self.isVisible {
                                self.topBarConstraint.constant = 0
                                self.topBarView.isHidden = true
                                self.thisAccountNotLBL.text = Localization("NoAccountDetected")
                                self.clickResendLBL.text = Localization("NoAccountMessage")
                                self.resendBTN.setTitle(Localization("Login/Reg"), for: .normal)
                                    if self.accountVerifiedView.alpha == 0 {
                                        self.accountVerifiedView.isHidden = false
                                        UIView.animate(withDuration: 0.3, animations: {
                                            self.accountVerifiedView.alpha = 1
                                        }, completion: nil)
                                    }
                                }
        //NotificationCenter.default.post(name: NSNotification.Name.init("UserLogin"), object: nil)
            return
        }
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
              let a = try JSON(data: dataFromString!)
            if a["firstConnection"].boolValue == false {
                collectionView.isUserInteractionEnabled = false
                       let alert = UIAlertController(title: "Processing GIF/MIMES", message: "Please select one of the following options", preferredStyle: .alert)
                       let actionGIF = UIAlertAction(title: "GIF", style: .default) { (alert) in
                           if self.isSearching == false {
                           let selectedGif = self.currentGifs![indexPath.row]
                              
                           //self.searchController.isActive = false
                           self.traitGif(collection: collectionView,gifId: selectedGif.gfyId, gifUrl: selectedGif.mpgUrl.absoluteString, nameGif: selectedGif.gfyName )
                           }else{
                               let selectedGif = self.currentGifsForSearch![indexPath.row]

                               //self.searchController.isActive = false
                               self.traitGif(collection: collectionView,gifId: selectedGif.gfyId, gifUrl: selectedGif.mpgUrl.absoluteString, nameGif: selectedGif.gfyName )
                           }
                           //self.delegate?.giphyControllerDidSelectGif(collection :collectionView , controller: self, item: selectedGif)
                       }
                       let actionMIME = UIAlertAction(title: "MIME", style: .default) { (alert) in
                           print("under construction")
                           if self.isSearching == false {
                                     let selectedGif = self.currentGifs![indexPath.row]
                                         
                                     //self.searchController.isActive = false
                            self.traitMime(collection: collectionView,mimeId: selectedGif.gfyId, mimeUrl: selectedGif.posterUrl.absoluteString, nameGif: selectedGif.gfyName, gifWidth: selectedGif.width, gifHeight: selectedGif.height )
                                     }else{
                                         let selectedGif = self.currentGifsForSearch![indexPath.row]

                                         //self.searchController.isActive = false
                            self.traitMime(collection: collectionView,mimeId: selectedGif.gfyId, mimeUrl: selectedGif.posterUrl.absoluteString, nameGif: selectedGif.gfyName, gifWidth: selectedGif.width,gifHeight: selectedGif.height )
                                     }
                       }
                       //actionMIME.isEnabled = false
                       let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) {(alert) in
                           print("under construction")
                           collectionView.isUserInteractionEnabled = true

                       }
                       alert.addAction(actionMIME)
                       alert.addAction(actionGIF)
                       alert.addAction(actionCancel)
                      // self.present(alert, animated: true, completion: nil)
               
                
                if self.isSearching == false {
                self.item =  self.currentGifs![indexPath.row]
                }else{
                    self.item =  self.currentGifsForSearch![indexPath.row]

                }
                 self.ChoiceView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.ChoiceView.alpha = 1
                    self.backgroundPopup.isHidden = false
                }, completion: nil)
                       
            }else{
                
                
                HomeTabBarController.shared.hhTabBarView.selectTabAtIndex(withIndex: 2)
                NotificationCenter.default.post(name: NSNotification.Name.init("UserActivation"), object: nil)
                let alert = UIAlertController(title: "GifMemer", message: "please activate your account to use the app", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(action)
                //self.present(alert, animated: true, completion: nil)
            }
        }catch {
            print("GfyCatTrending didselect : ",error.localizedDescription)
        }
       
        
        //searchController.isActive = false
        //delegate?.giphyControllerDidSelectGif(controller: self, item: selectedGif)
    }
    
}
extension GfyCatTrendingController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetX = scrollView.contentOffset.y
        
        //print(contentOffsetX)
        //print(scrollView.contentSize.height - scrollView.bounds.height - 20)
        if contentOffsetX >= (scrollView.contentSize.height - scrollView.bounds.height) - 20 /* Needed offset */ {
            guard !self.isLoading else { return }
            self.isLoading = true
            // load more data
            print("DID SCROLL")
            if isSearching == false {
            self.getTrendingGifs()
            }else{
                self.filterContentForSearchText(searchText: collectionSearchBar.searchTextField.text!)
            }
            // than set self.isLoading to false when new data is loaded
        }
    }
    
}
extension GfyCatTrendingController : UISearchBarDelegate {
    func filterContentForSearchText(searchText:String){
        var tempCursor = ""
        if cursorSearch != "" {
            tempCursor = "&cursor=" + self.cursorSearch
        }
        let url = URL(string:("https://api.gfycat.com/v1/gfycats/search?search_text=" + searchText.stringByAddingPercentEncodingForFormData()! + tempCursor))!
        print(url.absoluteString)
Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { (data) in
             var tempGifs : [GfycatMediaCustom] = []
        //self.currentGifsForSearch = []
            if data.response?.statusCode == 200 {
                let dataParser = JSON( data.data)
                print(dataParser)
                if dataParser["gfycats"].arrayObject != nil {
                if dataParser["gfycats"].arrayObject?.count != 0 {
                    self.cursorSearch = dataParser["cursor"].stringValue
                    for i in 0...((dataParser["gfycats"].arrayObject?.count)! - 1) {
                                           
//tempGifs.append(GfycatMedia(gfyId: dataParser["gfycats"][i]["gfyId"].stringValue))
                if ( dataParser["gfycats"][i]["nsfw"].stringValue == "0") {
                    let tempX = GfycatMediaCustom(info: dataParser["gfycats"][i].rawValue as! [AnyHashable : Any])
                    tempX.width = String(dataParser["gfycats"][i]["width"].intValue)
                    tempX.height = String(dataParser["gfycats"][i]["height"].intValue)
                    tempGifs.append(tempX)
                                          }
                                        }
                                    }
                                }
                self.currentGifsForSearch?.append(contentsOf: tempGifs)
                self.collection.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                   self.isLoading = false
                               }
                
            }
        
        }
          
       }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            self.isSearching = true
            self.currentGifsForSearch = []
            self.filterContentForSearchText(searchText: searchText)
            self.collection.reloadData()
        }else{
            self.isSearching = false
            self.currentGifsForSearch = []
            self.collection.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.collectionSearchBar.resignFirstResponder()
        self.collectionSearchBar.text = ""
        self.isSearching = false
        self.currentGifsForSearch = []
        self.collectionSearchBar.removeFromSuperview()
        self.collection.reloadData()
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = true
        self.view.endEditing(true)
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.collectionSearchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //self.isSearching = false
          self.collectionSearchBar.setShowsCancelButton(false, animated: false)
    }
    
    
}
extension GfyCatTrendingController  : UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true) {
                if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imagePicked = self.fixOrientation(img: image)
                }
    self.image(self.imagePicked, didFinishSavingWithError: nil, contextInfo: nil)
            
            
            
          
           
            /*DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
               // self.imageUser.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            } */

        }
        
        //imageUser.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer?) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
                        //self.process(image: self.imagePicked)
                    self.process(image: self.imagePicked) { (resultCode) in
                
          
                            //let openCvWrapper = OpenCVWrapper()
                           //let resultCode = openCvWrapper.isThisWorking(self.imagePicked)
                           print("ResultCode : ",resultCode)
                           if resultCode == 0 {
                               let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                               let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                               alert.addAction(alertAction)
                               self.present(alert, animated: true, completion: nil)
                                    return
                                  
                           }else if resultCode == 2 {
                               let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceMore"), preferredStyle: .alert)
                               let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                               alert.addAction(alertAction)
                               self.present(alert, animated: true, completion: nil)
                           }else if resultCode == 3{
                            let alert = UIAlertController(title: Localization("Photo"), message: "the face is not proprely recognized or it is not in a portrait mode", preferredStyle: .alert)
                                                          let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                                          alert.addAction(alertAction)
                                                          self.present(alert, animated: true, completion: nil)
                           }else{
            self.uploadToServer()

                            }
                  }
          
        }
    }
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
}
extension GfyCatTrendingController : URLSessionDelegate,URLSessionTaskDelegate, URLSessionDataDelegate {
func randomStringWithLength() -> String{
       let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
       let len = 15
       let date = Date()
       let randomString : NSMutableString = NSMutableString(capacity: len)
       for _ in 0...(len - 1){
           let length = UInt32(letters.length)
           let rand = arc4random_uniform(length)
           randomString.appendFormat("%C", letters.character(at: Int(rand)))
           
       }
       var resultFinal = "ios" + (randomString as String) + date.description
       resultFinal = resultFinal.replacingOccurrences(of: " ", with: "")
       resultFinal = resultFinal.replacingOccurrences(of: ":", with: "")
       resultFinal = resultFinal.replacingOccurrences(of: "+", with: "")
       resultFinal = resultFinal.replacingOccurrences(of: "-", with: "")
       resultFinal = resultFinal.replacingOccurrences(of: ".", with: "")
       return resultFinal
   }
func uploadToServer(){
       // SwiftSpinner.show("Uploading Picture...")
    self.changePhotoView.isHidden = true
    self.changePhotoView.alpha = 0
    self.addPhotoPresentationView.isHidden = true
    self.addPhotoPresentationView.alpha = 0
    self.addPhotoView.isHidden = true
    self.addPhotoView.alpha = 0
       let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
       let contentType = "multipart/form-data; boundary=" + boundaryConstant
       
       let mimeType = "image/jpeg"
       
       
       let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
       //?.rotate(radians: Float( -(Double.pi / 2)))
       let image = self.imagePicked
       let fileData : Data? = image!.jpegData(compressionQuality: 1)
       let requestBodyData : NSMutableData = NSMutableData()
       requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
       
       let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
       let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
       do {
           let a = try JSON(data: dataFromString!)
           let key = "userId"
           requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
           requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
       }catch{
           
       }
       requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
       do {
           let a = try JSON(data: dataFromString!)
           let fieldName = "picture"
           let filename = "zonz" + self.randomStringWithLength() + ".jpg"
           requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
       }catch{
           
       }
       requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
       
       //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
       requestBodyData.append(fileData!)
       // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
       requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
       requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
       var request = URLRequest(url: uploadScriptUrl!)
       
       
       request.httpMethod = "POST"
       request.httpBody = requestBodyData as Data
       request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
       request.setValue(contentType, forHTTPHeaderField: "Content-Type")
       let config = URLSessionConfiguration.default
       let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
       let task = session.uploadTask(withStreamedRequest: request)
       task.resume()
   }
func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
    if (error != nil ) {
        SwiftSpinner.show("Unexpected Error", animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            SwiftSpinner.hide()
        }
    }else{
            let q = JSON(buffer)
         SwiftSpinner.hide()
        if q["error"].exists() == false {
            do {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = try JSON(data: dataFromString!)
                a["userImageURL"].stringValue = q["userImageURL"].stringValue
                UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                UserDefaults.standard.synchronize()
                MapleBacon.shared.cache.clearDisk()
                MapleBacon.shared.cache.clearMemory()
                SwiftSpinner.hide()
                if isGifOrMime {
                    self.traitGif(collection: self.collection, gifId: self.item.gfyId, gifUrl: self.item.mpgUrl.absoluteString, nameGif: self.item.gfyName)
                }else{
                    self.traitMime(collection: self.collection, mimeId: self.item.gfyId, mimeUrl: self.item.posterUrl.absoluteString, nameGif: self.item.gfyName, gifWidth: self.item.width, gifHeight: self.item.height)
                }
            
            }catch{
                
            }
        }
    }
}
func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
    let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
  
    SwiftSpinner.show(progress: Double(uploadProgress), title: "\(Int(uploadProgress * 100))% \n Uploading" )
}
func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
    buffer.append(data)
}
func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
    completionHandler(URLSession.ResponseDisposition.allow)
}
}
import Vision
class appleDetection : UIViewController {
    var orientation:Int32 = 0
    // detect image orientation, we need it to be accurate for the face detection to work
    func process(image:UIImage,completion: @escaping (_ res : Int) -> Void ) {
        switch image.imageOrientation {
        case .up:
            orientation = 1
        case .right:
            orientation = 6
        case .down:
            orientation = 3
        case .left:
            orientation = 8
        default:
            orientation = 1
        }
    
        // vision
       
        let faceLandmarksRequest = VNDetectFaceRectanglesRequest { (request, error) in
            guard let observations = request.results as? [VNFaceObservation] else {
                fatalError("unexpected result type!")
            }
            if observations.count == 0 {
                completion(0)
                print("undetected photo")
            }
            var j = 0
            for _ in observations {
                j = j + 1
                print("detected photo")
                //addFaceLandmarksToImage(face)
            }
            if j > 1 {
                completion(2)
            }else{
                completion(1)
            }
        }
        let requestHandler = VNImageRequestHandler(cgImage: image.cgImage!, orientation: CGImagePropertyOrientation(rawValue: CGImagePropertyOrientation.RawValue(orientation))! ,options: [:])
        do {
            try requestHandler.perform([faceLandmarksRequest])
        } catch {
            print(error)
        }
    }
    func handleFaceFeatures(request: VNRequest, errror: Error?) {
        guard let observations = request.results as? [VNFaceObservation] else {
            fatalError("unexpected result type!")
        }
        if observations.count == 0 {
            print("undetected photo")
        }
        for face in observations {
            print("detected photo")
            //addFaceLandmarksToImage(face)
        }
    }
    
}
