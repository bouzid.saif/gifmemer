import ImageIO
import CoreGraphics
import SwiftSpinner
public class Decoder {
  public init() {

  }
}

public extension Decoder {
  /// Decode gif file to multile images
    func decode(gifUrl: URL) -> GifInfo? {
    guard let source = CGImageSourceCreateWithURL(gifUrl.toCF(), nil) else {
      return nil
    }

    let frameCount = CGImageSourceGetCount(source)
        
        let images: [UIImage] = Array(0..<frameCount).compactMap({
             
      guard let cgImage = CGImageSourceCreateImageAtIndex(source, $0, nil) else {
        return nil
      }
            
           
       return UIImage(cgImage: cgImage)
        // UIImage(data: cgImage as! Data, scale: .zero)
    })

    guard var gifInfo = getInfo(source: source) else {
      return nil
    }

    gifInfo.images = images
    return gifInfo
  }

  // MARK: - Helper

  func getInfo(source: CGImageSource) -> GifInfo? {
    guard let dictionary: JSONDictionary =
      CGImageSourceCopyPropertiesAtIndex(source, 0, nil)?.toDictionary()
      else {
        return nil
    }

    guard let colorModel = dictionary[kCGImagePropertyColorModel as String] as? String,
      let depth = dictionary[kCGImagePropertyDepth as String] as? Int,
      let pixelHeight = dictionary[kCGImagePropertyPixelHeight as String] as? Int,
      let pixelWidth = dictionary[kCGImagePropertyPixelWidth as String] as? Int,
      let gif = dictionary[kCGImagePropertyGIFDictionary as String] as? JSONDictionary,
      let unclampedDelayTime = gif[kCGImagePropertyGIFUnclampedDelayTime as String] as? TimeInterval else {
        return nil
    }

    let hasAlpha = dictionary[kCGImagePropertyHasAlpha as String] as? Bool

    return GifInfo(colorModel: colorModel,
                   depth: depth,
                   hasAlpha: hasAlpha,
                   pixelWidth: pixelWidth,
                   pixelHeight: pixelHeight,
                   frameDuration: unclampedDelayTime)
  }
}

public extension Decoder {
  /// Decode video file to multile images
    func decode(videoUrl: URL) -> VideoInfo? {
    return nil
  }
}
public struct GifInfo {
  public let colorModel: String
  public let depth: Int
  public let hasAlpha: Bool?
  public let pixelWidth: Int
  public let pixelHeight: Int
  public let frameDuration: TimeInterval
  public var images = [UIImage]()

  init(colorModel: String,
       depth: Int,
       hasAlpha: Bool?,
       pixelWidth: Int,
       pixelHeight: Int,
       frameDuration: TimeInterval) {
    self.colorModel = colorModel
    self.depth = depth
    self.hasAlpha = hasAlpha
    self.pixelWidth = pixelWidth
    self.pixelHeight = pixelHeight
    self.frameDuration = frameDuration
  }
}

public struct VideoInfo {
  public var images = [UIImage]()
}
typealias JSONDictionary = [String: Any]

  
extension URL {
  func toCF() -> CFURL {
    return self as CFURL
  }
}

extension Dictionary {
  func toCF() -> CFDictionary {
    return self as CFDictionary
  }
}

extension Data {
  func toCF() -> CFData {
    return self as CFData
  }
}

extension CFDictionary {
  func toDictionary() -> Dictionary<String, Any> {
    let nsDictionary = self as NSDictionary

    var dict = [String: Any]()
    for (key, value) in nsDictionary {
      if let key = key as? String {
        dict[key] = value
      }
    }

    return dict
  }
}
