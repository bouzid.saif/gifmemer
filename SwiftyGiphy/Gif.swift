//
//  Gif.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-12-02.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
class Gif: NSObject {
    var id : String = ""
    var gifName :String =  ""
    var giphyId : String = ""
    var priceGif : String = ""
    var gifUrl : String = ""
    var gifHeight : String = ""
    var gifWidth : String = ""
    init(id:String,gifName:String,giphyId:String,priceGif:String,gifUrl:String,gifHeight: String,gifWidth : String) {
        self.id = id
        self.gifName = gifName
        self.giphyId = giphyId
        self.priceGif = priceGif
        self.gifUrl = gifUrl
        self.gifHeight = gifHeight
        self.gifWidth = gifWidth
    }
}
