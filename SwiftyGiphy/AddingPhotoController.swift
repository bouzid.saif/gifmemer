//
//  AddingPhotoController.swift
//  YLYL
//
//  Created by macbook on 4/18/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import MapleBacon
import SwiftyJSON
class AddingPhotoController: ServerUpdateDelegate {
    
    @IBOutlet weak var imageUser:RoundedUIImageView!
    var imagePicker : UIImagePickerController!
    @IBOutlet weak var firstTextLBL: UILabel!
    
    @IBOutlet weak var secondTextLBL: UILabel!
    
    @IBOutlet weak var takePhotoIMG: UIImageView!
    
    @IBOutlet weak var fromLibraryIMG: UIImageView!
     @IBOutlet weak var FinalIMGPos: UIImageView!
    
    @IBOutlet weak var welcomeMSG: UILabel!
    
    @IBOutlet weak var doYouWishLBL: UILabel!
    
    @IBOutlet weak var NoBTN: UIButton!
    @IBOutlet weak var YesBTN: UIButton!
    @IBOutlet weak var messageLBL : UILabel!
    
    //news
    @IBOutlet weak var visualEffectSkip: UIVisualEffectView!
    
    
    @IBOutlet weak var popupSkip: UIView!
    
    @IBOutlet weak var areYSure: UILabel!
    
    @IBOutlet weak var goodface: UILabel!
    
    @IBOutlet weak var backSkip: UIButton!
    
    @IBAction func backSkipAction(_ sender: UIButton) {
       DispatchQueue.main.async {
           self.visualEffectSkip.isHidden = true
           self.popupSkip.isHidden = true
       }
    }
    
    @IBOutlet weak var skipFinal: UIButton!
    
    @IBAction func skipFinalAction(_ sender: Any) {
        DispatchQueue.main.async {
        self.visualEffectSkip.isHidden = true
        self.popupSkip.isHidden = true
        self.firstTextLBL.isHidden = true
        self.secondTextLBL.isHidden = true
        self.takePhotoIMG.isHidden = true
        self.fromLibraryIMG.isHidden = true
        self.imageUser.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        UIView.animate(withDuration: 0.7, delay: 0.2, options: [.curveLinear], animations: {
           
            self.imageUser.layer.setAffineTransform(CGAffineTransform(scaleX: 2.1, y: 2.1))
            
            print(self.imageUser.layer.cornerRadius)
            
        }) { (complete) in
            self.welcomeMSG.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
        self.performSegue(withIdentifier: "go_to_homeP", sender: self)

            })
        }
              }
    }
    
    @IBOutlet weak var pleaseAddPhoto: UILabel!
    
    @IBOutlet weak var skipButton: UIButton!
    
    
    @IBAction func skipButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.visualEffectSkip.isHidden = false
            self.popupSkip.isHidden = false
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageLBL.text = Localization("MessageFacePick")
        self.goodface.text = Localization("MessageSkip")
        self.areYSure.text = Localization("AYS")
        
        if UserDefaults.standard.value(forKey: "UserZonzay") != nil {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
            let a =  try JSON(data: dataFromString!)
       /* if a["userImageURL"].exists() {
            self.activateSecondSceen()
        }else{
            self.activateFirstSceen()
        } */
        self.activateFirstSceen()
      
        self.welcomeMSG.text = "Welcome " + a["userName"].stringValue + " !"
                }catch{
                
                }
        }else{
            self.activateFirstSceen()
            self.welcomeMSG.text = "Welcome Developer!"
        }
    
    }
    func activateFirstSceen(){
       //self.welcomeMSG.isHidden = false
        self.fromLibraryIMG.isHidden = false
        self.takePhotoIMG.isHidden = false
        self.secondTextLBL.isHidden = false
        self.firstTextLBL.isHidden = false
    }
    func activateSecondSceen(){
        self.doYouWishLBL.isHidden = false
        self.NoBTN.isHidden = false
        self.YesBTN.isHidden = false
        
    }
   @IBAction func YesAction(_ button: UIButton) {
        DispatchQueue.main.async {
            self.doYouWishLBL.isHidden = true
            self.NoBTN.isHidden = true
            self.YesBTN.isHidden = true
            
            self.activateFirstSceen()
        }
        
    }
  @IBAction func NoAction(_ button: UIButton) {
        self.doYouWishLBL.isHidden = true
        self.NoBTN.isHidden = true
        self.YesBTN.isHidden = true
        self.imageUser.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        // let displayLink = CADisplayLink(target: self, selector: #selector(self.animationDidUpdate))
        // displayLink.preferredFramesPerSecond = 6
        // displayLink.add(to: RunLoop.main, forMode: RunLoop.Mode.default)
        UIView.animate(withDuration: 0.7, delay: 0.2, options: [.curveLinear], animations: {
            //self.imageUser.transform =
            self.imageUser.layer.setAffineTransform(CGAffineTransform(scaleX: 2.1, y: 2.1))
            //self.imageUser.layer.cornerRadius *= 2.1
            print(self.imageUser.layer.cornerRadius)
            
        }) { (complete) in
            self.welcomeMSG.isHidden = false
            
            // self.imageUser.layer.cornerRadius = 189 / 2
            
            // self.imageUser.layoutIfNeeded()
            print(self.imageUser.layer.frame)
            print(self.imageUser.layer.cornerRadius)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.performSegue(withIdentifier: "go_to_homeP", sender: self)
                
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func takePhotoFromCamera(_ sender: UITapGestureRecognizer) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        didTakePhoto = true
        present(imagePicker, animated: true, completion: nil)
    }
    var didTakePhoto = false
    @IBAction func takePhotoFromLibrary(_ sender: UITapGestureRecognizer) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        didTakePhoto = false
        present(imagePicker, animated: true, completion: nil)
    }
    fileprivate var buffer:NSMutableData = NSMutableData()

}
extension AddingPhotoController : UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true) {

        
            
    if self.didTakePhoto { UIImageWriteToSavedPhotosAlbum(info[UIImagePickerController.InfoKey.originalImage] as! UIImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imageUser.image = self.fixOrientation(img: image)
                }
            }else{
                if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imageUser.image = self.fixOrientation(img: image)
                }
    self.image(self.imageUser.image!, didFinishSavingWithError: nil, contextInfo: nil)
            }
            
            
          
           
            /*DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
               // self.imageUser.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            } */

        }
        
        //imageUser.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer?) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            
                         //let metadata = VisionImageMetadata()
                                    //metadata.orientation = .rightTop
                        // metadata.orientation = image?.imageOrientation
                            //       visionImage.metadata = metadata
            self.process(image: self.imageUser.image!) { (resultCode) in
                          //  let openCvWrapper = OpenCVWrapper()
                            // let resultCode = openCvWrapper.isThisWorking(self.imageUser.image!)
                           print("ResultCode : ",resultCode)
                           if resultCode == 0 {
                               let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                               let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                               alert.addAction(alertAction)
                               self.present(alert, animated: true, completion: nil)
                                    return
                                  
                           }else if resultCode == 2 {
                               let alert = UIAlertController(title: Localization("Photo"), message: Localization("PhotoFaceMore"), preferredStyle: .alert)
                               let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                               alert.addAction(alertAction)
                               self.present(alert, animated: true, completion: nil)
                           }else if resultCode == 3{
                            let alert = UIAlertController(title: Localization("Photo"), message: "the face is not proprely recognized or it is not in a portrait mode", preferredStyle: .alert)
                                                          let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                                                          alert.addAction(alertAction)
                                                          self.present(alert, animated: true, completion: nil)
                           }else{
            self.uploadToServer()

                            }
            }
          
        }
    }
//    func detectAllFeature(_ feature:VisionFace) -> Bool{
//        guard let _ = feature.landmark(ofType: .leftCheek) else {
//            return false
//        }
//        guard let _ = feature.landmark(ofType: .leftEar) else {
//                   return false
//               }
//        guard let _ = feature.landmark(ofType: .leftEye) else {
//                   return false
//               }
//        guard let _ = feature.landmark(ofType: .mouthLeft) else {
//                   return false
//               }
//        guard let _ = feature.landmark(ofType: .mouthBottom) else {
//                   return false
//               }
//        guard let _ = feature.landmark(ofType: .mouthRight) else {
//                   return false
//               }
//        guard let _ = feature.landmark(ofType: .noseBase) else {
//                          return false
//                      }
//        guard let _ = feature.landmark(ofType: .rightCheek) else {
//                          return false
//                      }
//        guard let _ = feature.landmark(ofType: .rightEar) else {
//                          return false
//                      }
//        guard let _ = feature.landmark(ofType: .rightEye) else {
//                          return false
//                      }
//
//        return true
//
//    }

    
}
extension AddingPhotoController {
    func randomStringWithLength() -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = 15
        let date = Date()
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0...(len - 1){
            let length = UInt32(letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
            
        }
        var resultFinal = "ios" + (randomString as String) + date.description
        resultFinal = resultFinal.replacingOccurrences(of: " ", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: ":", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: "+", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: "-", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: ".", with: "")
        return resultFinal
    }
    func uploadToServer(){
        // SwiftSpinner.show("Uploading Picture...")
        let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
        let contentType = "multipart/form-data; boundary=" + boundaryConstant
        
        let mimeType = "image/jpeg"
        
        
        let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
        //?.rotate(radians: Float( -(Double.pi / 2)))
        let image = self.imageUser.image
        let fileData : Data? = image!.jpegData(compressionQuality: 1)
        let requestBodyData : NSMutableData = NSMutableData()
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let key = "userId"
            requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
        }catch{
            
        }
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
        do {
            let a = try JSON(data: dataFromString!)
            let fieldName = "picture"
            let filename = "zonz" + self.randomStringWithLength() + ".jpg"
            requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
        }catch{
            
        }
        requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
        
        //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
        requestBodyData.append(fileData!)
        // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
        requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
        requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
        var request = URLRequest(url: uploadScriptUrl!)
        
        
        request.httpMethod = "POST"
        request.httpBody = requestBodyData as Data
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.uploadTask(withStreamedRequest: request)
        task.resume()
      /*  let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            // SwiftSpinner.hide()
            
            let b = JSON(data!)
            print(b)
            if b["error"].exists() == false {
                print("upload finished")
                do {
                    var a = try JSON(data: dataFromString!)
                    a["userImageURL"].stringValue = b["userImageURL"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    MapleBacon.shared.cache.clearDisk()
                }catch{
                    
                }
            }else{
                print("upload failed")
            }
            
        }
        
        task.resume() */
    }
}
extension AddingPhotoController : URLSessionDelegate,URLSessionTaskDelegate, URLSessionDataDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if (error != nil ) {
            SwiftSpinner.show("Unexpected Error", animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                SwiftSpinner.hide()
            }
        }else{
                let q = JSON(buffer)
             SwiftSpinner.hide()
            if q["error"].exists() == false {
                do {
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = try JSON(data: dataFromString!)
                    a["userImageURL"].stringValue = q["userImageURL"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    MapleBacon.shared.cache.clearDisk()
                    MapleBacon.shared.cache.clearMemory()
                    
                    self.firstTextLBL.isHidden = true
                    self.secondTextLBL.isHidden = true
                    self.takePhotoIMG.isHidden = true
                    self.fromLibraryIMG.isHidden = true
                    self.imageUser.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
                    // let displayLink = CADisplayLink(target: self, selector: #selector(self.animationDidUpdate))
                    // displayLink.preferredFramesPerSecond = 6
                    // displayLink.add(to: RunLoop.main, forMode: RunLoop.Mode.default)
                    UIView.animate(withDuration: 0.7, delay: 0.2, options: [.curveLinear], animations: {
                        //self.imageUser.transform =
                        self.imageUser.layer.setAffineTransform(CGAffineTransform(scaleX: 2.1, y: 2.1))
                        //self.imageUser.layer.cornerRadius *= 2.1
                        print(self.imageUser.layer.cornerRadius)
                        
                    }) { (complete) in
                        self.welcomeMSG.isHidden = false
                        
                        // self.imageUser.layer.cornerRadius = 189 / 2
                        
                        // self.imageUser.layoutIfNeeded()
                        print(self.imageUser.layer.frame)
                        print(self.imageUser.layer.cornerRadius)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self.performSegue(withIdentifier: "go_to_homeP", sender: self)

                        })
                    }
                }catch{
                    
                }
            }
        }
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
      
        SwiftSpinner.show(progress: Double(uploadProgress), title: "\(Int(uploadProgress * 100))% \n Uploading" )
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
}
