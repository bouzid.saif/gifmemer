//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Ahmed Haddar on 03/10/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON
open class SocketIOManager  {
    
    var disconnects = 0
    public static let sharedInstance = SocketIOManager()
    
    private var manager : SocketManager
     private var managerUpdateServer : SocketManager
    private var socket : SocketIOClient
    private var socketUpdateServer : SocketIOClient
    var connectedMiddleWar : Bool = false

    var alert: UIAlertController = UIAlertController(title: "Server", message: "Reconnecting...", preferredStyle: .alert)
    var connected = 0
    var didConnect = false
    //51.254.37.192
    //, config: [.log(true)]
    
    func detectDisconnect(){
        socket.on(clientEvent: .error){ data,ack in
            print(" disconnectd")
          /*  if self.disconnects == 0 {
            self.disconnects = self.disconnects + 1
            print(self.currentViewController())
                if self.currentViewController().restorationIdentifier != "SplashScreenController" {
            self.currentViewController().present(self.alert, animated: true, completion: nil)
                }
            }
            } */
           // self.navigationController!.present( self.alert, animated: true, completion: nil)
        
    }
    }
    func currentViewController() -> UIViewController{
        return (UIApplication.shared.delegate?.window?!.rootViewController)!
    }
    
    private init() {
        print("INIT")
        
        manager = SocketManager(socketURL: URL(string: ScriptBase.socket_URL)!, config: [.reconnects(true),.log(true)])
        socket = manager.defaultSocket
       
         managerUpdateServer = SocketManager(socketURL: URL(string: ScriptBase.socket_URLUpdate)!, config: [.reconnects(true),.log(false)])
        socketUpdateServer = managerUpdateServer.defaultSocket
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        self.alert.view.addSubview(loadingIndicator)
    }
    @objc func connectWithUserId(_ notification: Notification) {
        SocketIOManager.sharedInstance.updateSocketId(userId: randomStringWithLength())
    }
    
    func randomStringWithLength() -> String{
           let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
           let len = 8
           let date = Date()
           let randomString : NSMutableString = NSMutableString(capacity: len)
           for _ in 0...(len - 1){
               let length = UInt32(letters.length)
               let rand = arc4random_uniform(length)
               randomString.appendFormat("%C", letters.character(at: Int(rand)))
               
           }
           return "ios" + (randomString as String) + date.description
       }
    var OneTimeRequest = true
    func establishConnection() {
        print("connecting..")
        NotificationCenter.default.addObserver(self, selector: #selector(self.connectWithUserId(_:)), name: NSNotification.Name.init("ConnectServer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sendVideoToServer(video:)), name: NSNotification.Name.init("sendVideo"), object: nil)
        listenForOtherMessages()
         listenForOtherMessagesFromUpdate()
        listenDisconnectUpdate()
       // socket.connect()
        //socketUpdateServer.connect()

        
        

            //(alertWindow.rootViewController as! UINavigationController).viewControllers.append(alert)
    
   
        }
    func listenForOtherMessagesFromUpdate(){
        socketUpdateServer.on(clientEvent: .connect){ data, ack in
            print("socketUpdate connected")
            self.connectedMiddleWar = true
            self.socketUpdateServer.on("currentAmount") {data, ack in
                guard let cur = data[0] as? Double else { return }
                self.socketUpdateServer.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
                    self.socketUpdateServer.emit("update", ["amount": cur + 2.50])
                }
                ack.with("Got your currentAmount", "dude")
                
            }
        }
    }
    func listenDisconnectUpdate(){
        socketUpdateServer.on(clientEvent: .reconnect){ data, ack in
            print("socket Update disconnected")
              self.connectedMiddleWar = false
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.restartApplication()
            //NotificationCenter.default.post(name: NSNotification.Name.init("closeUpdate"), object: nil)
            }
        
    }
    func isModal( _ view:UIViewController) -> Bool {
        if (view.presentingViewController != nil) {
            return true
        }
    if view.navigationController?.presentingViewController?.presentedViewController == view.navigationController {
        return true
    }
    if (view.tabBarController?.presentingViewController is UITabBarController) {
        return true
    }

    return false
}
    @objc func sendVideoToServer(video:Notification) {
        if let videoToSend = video.object as? String {
            NotificationCenter.default.post(name: NSNotification.Name.init("VideoSended"), object: nil)
            socket.emit("getMyvideo", videoToSend)
        }else{
            print("we can't send it")
        }
        
    }
    func listenToVideoFinsihed(completionHandler : @escaping (_ messageInfo : String) -> Void){
        socket.on("resultVideo") { (dataArray, socketACK) -> Void in
            if let result = dataArray as? [String] {
                completionHandler(result[0])

            }else{
                completionHandler("")
            }

        }
    }
    func closeConnection() {
        socket.disconnect()
    }
    func joinRoom(chatRoom: String,user:String) {
        socket.emit("join", chatRoom,user)
    }
    /*
     *Emit join then it has json object {"event" : "message", "senderID" : ,"receiverID" , "message"},{"event" : type, "userID" : ....}
     *
     */
    func getMessageJoin(completionHandler : @escaping (_ messageInfo : JSON) -> Void ) {
        socket.on("messageJoin") { (dataArray,socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            completionHandler(finalRes)
            
        }
    }
    func sendMessage(chatRoom:String,user:String,contact:String,message:String) {
        socket.emit("message",chatRoom,user,contact,message)
    }
    /*
     *this a message received it has json object {"user" : , "message" : {"message" : ,"createdAt" : , "_id" : ,"userFirstName" : ,"userLastName" : , "userImageURL"}}
     *
     */
    func getMessages(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("message") { (dataArray, socketAck) -> Void in
            
            let results = dataArray
            let finalRes = JSON(results)
            completionHandler(finalRes)
            
        }
    }
    func getLeavers(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("leave") { (dataArray, socketAck) -> Void in
            //Nothing
            
        }
    }
    
    func addRoom(userId: String,FriendId:String) {
        print("addRoom:",userId,FriendId)
        socket.emit("addRoom", userId,FriendId)
    }
    func getNewMessages(completionHandler : @escaping (_ messageInfo : JSON) -> Void)
    {
        socket.on("newMessage") { (dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            print("newMessage",finalRes)
            completionHandler(finalRes)
        }
    }
    func getCreatedChatRoom(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("newChatRoom") {(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            print("newChatRoom",finalRes)
            completionHandler(finalRes)
        }
    }
    func StopCreatedChatRoom(){
        socket.off("newChatRoom")
    }
    func Typing(room:String,userId:String,contact:String)  {
        socket.emit("isTyping",room, userId,contact)
    }
    func isTyping(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("isTyping") {(dataArray, socketAck) -> Void in
            completionHandler(dataArray)
            
        }
    }
    func listenForOtherMessages(){
        socket.on(clientEvent: .connect){ data, ack in
            print("socket connected")
            if self.disconnects > 0 {
              self.disconnects = self.disconnects - 1
            }
            self.socket.on("currentAmount") {data, ack in
                guard let cur = data[0] as? Double else { return }
                self.socket.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
                    self.socket.emit("update", ["amount": cur + 2.50])
                    }
            ack.with("Got your currentAmount", "dude")
                
            }
            guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                return
            }
            
            
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                SocketIOManager.sharedInstance.updateSocketId(userId: a["_id"].stringValue)
                self.alert.dismiss(animated: true, completion: nil)
                
            }catch{
                
            }
    
        }
             }
    
    
    
    func testSocketEmit(userId:String){
        socket.emit("matchMaking", userId)
    }
    func cancelChallenge(userId:String){
        socket.emit("cancelRoom", userId)
    }
    
    func videoRoomRequestAccepted(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
    socket.on("videoRoomRequestAccepted"){(dataArray, socketAck) -> Void in
        let results = dataArray
        //let finalRes = JSON(results)
        
        completionHandler(results as! [String])
    }
   
    
        }
    
    func removeVideoRoomRequestAccepted(){
        socket.off("videoRoomRequestAccepted")
    }
    func videoRoomRequestRefused(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("videoRoomRequestRefused"){(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            
            completionHandler(finalRes)
        }
    
        }
    func removeVideoRoomRequestRefused(){
        socket.off("videoRoomRequestRefused")
    }
    func videoRoomRequest(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
        socket.on("videoRoomRequest"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("dataArray: ",dataArray)
           // let finalRes = JSON(results)
            if let res = results as? [String] {
            completionHandler(res )
            }else{
                return
            }
        }
    }
    func removeVideoRoomRequest(){
        socket.off("videoRoomRequest")
    }
   
    
    func registerSocket(Name:String) {
        socket.emit("register", Name)
    }
    func registerSocketResponse(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("messageRegister"){(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            
            completionHandler(finalRes)
        }
        
    }
    func updateSocketId(userId:String) {
        socket.emit("updateId", userId)
        if userId != "" {
            let appd = (UIApplication.shared.delegate) as! AppDelegate
           // appd.loadData()
           
      
           /* SocketIOManager.sharedInstance.getAvailableUsers { (users) in
            print("AvailableUsers : ",users)
            if users.count != 0 {
                self.friendsConnected = users
               
            }
            self.clientDisconnectFromRoom() */
            //socket.on("client", callback: <#T##NormalCallback##NormalCallback##([Any], SocketAckEmitter) -> ()#>)
            /* let tempUsers = users[0]
             if tempUsers.arrayObject?.count != 0 {
             
             for i in 0...((self.FriendsL.count) - 1) {
             if tempUsers.contains(where: { (S, J) -> Bool in
             J.stringValue == self.FriendsL[i].getId()
             }) {
             self.FriendsL[i].setConnected("1")
             }
             }
             if self.ViewSelector.selectedIndex == 1 {
             self.friendsTableView.reloadData()
             }
             } */
        }
        }
    func getAllChatRooms(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("chatrooms"){(dataArray, socketAck) -> Void in
            let results = dataArray
            
            let finalRes = JSON(results)
            print("ChatRoomReceived:",finalRes)
            completionHandler(finalRes)
        }
    }
    func StopgetAllChatRooms(){
        socket.off("chatrooms")
    }
    func startNotifyChatRooms(userId:String) {
        socket.emit("chatrooms", userId)
    }
    func sendMatchMaking(userId:String) {
        socket.emit("matchMaking", userId)
    }
    func sendMatchMakingPremium(userId:String,minAge:String,maxAge:String,Gender:String) {
        
        socket.emit("matchMaking", userId,Int(minAge)!,Int(maxAge)!,Gender)
    }
    func getOppents(completionHandler : @escaping (_ messageInfo : [Any]) -> Void){
        socket.on("getOpponent") {(dataArray, socketAck) -> Void in
            if (!(dataArray[0] is NSNull)) && (!(dataArray[1] is NSNull)) {
                
            let results = dataArray[0] as! String
            let room = dataArray[1] as! NSNumber
                let whois = dataArray[2] as! String
                let temp = [results,room,whois] as [Any]
                completionHandler(temp)
            }else{
                completionHandler([])
            }
           
                
            //let finalRes = JSON(results)
            
            
        }
            
        }
    func removeFromMatchMaking(roomId:String){
        socket.emit("exitRoom", roomId)
    }
    func searchUsers(userId:String,text:String,completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("getUsersResponse") {(dataArray, socketAck) -> Void in
            let results = dataArray[0]
            print("Users: ",results)
            completionHandler(results)
        }
        socket.emit("getUsers", userId,text.lowercased())
    }
    func NotifyChangeLife(userId:String){
        socket.emit("onLifeChanged", userId)
    }
    func LifeChangedNotifier(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("lifeChanged"){(dataArray, socketAck) -> Void in
            let results = dataArray
            completionHandler(results)
            
        }
    }
    func closeLifeChangedNotifier(){
        socket.off("lifeChanged")
    }
    func gameWasLeaved(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("videoRoomExited"){(dataArray, socketAck) -> Void in
            print("User leaved with no reason")

            let results = dataArray
            completionHandler(results)
            
        }
    }
    func gameWasLeavedWithUserDecision(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("gameExitCnx"){(dataArray, socketAck) -> Void in
            print("User leaved with decision")
            let results = dataArray
            completionHandler(results)
            
        }
    }
    func closeGameWasLeaved(){
        socket.off("videoRoomExited")
    }
    func closeGameWasLeavedWithNoFace(){
        socket.off("gameExitCnx")
    }
    func leaveGame(userMaster:String,userSlave:String,pathVideo:String) {
        
        socket.emit("leaveVideoRoom", userMaster,userSlave,pathVideo)
    }
    func leaveGameWithUserDecision(userMaster:String,userSlave:String,pathVideo:String){
        socket.emit("onGameExitCnx", userSlave,pathVideo)
    }
    func LifeChangedNotifierStop(){
        socket.off("lifeChanged")
    }
    func jokeChangedStop(){
        socket.off("jokeChanged")
    }
    func StopGameNotifier(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("StopGame"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("STOPPPPGAMMEEEEEE: ",results)
            completionHandler(results)
        }
    }
    func StopGameNotifierClose(){
        socket.off("StopGame")
    }
    func StopGameNotifierAtEnd(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("StopGameEndTime"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("STOPPPPGAMMEEEEEE: ",results)
            completionHandler(results)
            
        }
    }
    func StopGameNotifierAtEndClose(){
        socket.off("StopGameEndTime")
    }
    
    func constructJson(json:[Int]) -> JSON{
        var result = "["
        for i in 1...59 {
            result = result + "{\"score\(i)\":\"\(json[i - 1])\"},"
        }
            result = String(result.dropLast())
             result = result + "]"
        return JSON(stringLiteral: result)
    }
    

   /* func constructTargetString(_ input:[String : Any],smiles:[Int]) -> [String : Any]{
        var test : [String : Any] = input
        for i in 1...(smiles.count) {
            let number = numbers["\(i)"]
            //test["\"\(numbers["\"\()\"])\"]
        }
        return input
    }*/
    /*func testBougiNumbers(tab: [Int]) {
    let constructedJson : JSON = self.constructJson(json: tab)
        socket.emit("testSocket", ["scores" : constructedJson.rawValue])
        
    }*/
    
   
    func test(userId:String){
        socket.emit("test", userId)
    }
    func notifyRound(userId:String,owner:String) {
        print("please your turn")
        socket.emit("onjokeChanged", userId,owner)
        
    }
    func RoundFinished(completionHandler : @escaping (_ messageInfo : [String]) -> Void)
    {
        socket.on("jokeChanged"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("RoundFinished: ",results)
            completionHandler(results as! [String])
            
        }
    }
    func synchronizeAndroid(userId:String){
        socket.emit("deviceSynchronisation", userId)
    }
    func listenFromAndroid(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
        socket.on("deviceSynchronisation"){(dataArray, socketAck) -> Void in
        let results = dataArray
        print("deviceSynchronisation: ",results)
        completionHandler([])
        
    }
    }
    func closeListenFromAndroid(){
        socket.off("deviceSynchronisation")
    }
    func RoundFinishedStop(){
        socket.off("jokeChanged")
    }
    
    func clientDisconnectFromRoom(){
      /*  socket.on("clientDisRoom") { (dataArray, socketAck) -> Void in
            let results = dataArray
            if let res = results[0] as? String {
                self.friendsConnected.removeString(text: res)
                NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
            }else{
                print("dataBaseError : ",results)
            }
            
        } */
    }
  

    
}
