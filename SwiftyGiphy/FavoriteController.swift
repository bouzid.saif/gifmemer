//
//  FavoriteController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-27.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import SwiftyGiphy
import AVKit
import GfycatKit
import Alamofire
import SwiftyJSON
import Photos
import SDWebImage
class FavoriteController: UIViewController {
     var isSearching = false
    @IBOutlet weak var zonzLBL:UILabel!

    var collectionSearchBar: UISearchBar!
    @IBOutlet weak var collection : UICollectionView!
    @IBOutlet weak var collectionViewLayout : SwiftyGiphyGridLayout!
    
    @IBOutlet weak var clipKeyboardBTN: UIButton!
    
    
    @IBOutlet weak var clipeKeyboardSuplementary: UIView!
    @IBOutlet weak var cancelKeyboardIMG: UIImageView!
    
    @IBOutlet weak var saveKeyboardIMG: UIImageView!
    var isForKeybord : Bool = false
    var numbersOfKeyboardGif = 0
    var numbersOfKeyboardMeme = 0

    @IBAction func clipKeyboardAction(_ sender: UIButton) {
        sender.isHidden = true
        self.clipeKeyboardSuplementary.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionTopLayout.constant = 40.5
            self.tapFiveLBL.isHidden = false
            
        }) { (complete) in
            self.favoritesLBL.text = Localization("ClipKeyboard")
            self.isForKeybord = true
            self.collection.reloadData()
        }
    }
    @IBOutlet weak var collectionTopLayout: NSLayoutConstraint!
    @IBOutlet weak var profilePicture : ExtensionProfile!
    @IBOutlet weak var profileNameLabel : UILabel!
    fileprivate var currentGifs: [GfycatMedia]?  = []
     ///Selector code
    @IBOutlet weak var MemesContainer: UIView!
    
    @IBOutlet weak var GifsContainer: UIView!
    
    var selectorContainer: UIView!
    
    @IBOutlet weak var gifsLabel: UILabel!
    var oneTimeWork = true
    @IBOutlet weak var memesLabel: UILabel!
    var isGif = true
    @IBOutlet weak var tapFiveLBL : UILabel!
    ///
    //MARK: Show Full Gif
       var gifUser : GifUser!
       var mimeUser : GifUser!
       @IBOutlet weak var showFullGifView: UIView!
       
       @IBOutlet weak var fullGifImageView: FLAnimatedImageView!
       
       @IBOutlet weak var fullGifNameLBL: UILabel!
       
       @IBOutlet weak var backFullGifLBL: UILabel!
       
       @IBAction func backFullGifAction(_ sender: UIButton) {
           
           UIView.animate(withDuration: 0.4, animations: {
               self.showFullGifView.alpha = 0
           }) { (complete) in
               self.showFullGifView.isHidden = true
               self.fullGifImageView.stopAnimatingGIF()
               self.fullGifImageView.image = nil
           }
       }
       
       @IBAction func remakeGifAction(_ sender: UIButton) {
           
       }
       @IBAction func downloadGifAction(_ sender: UIButton) {
           PHPhotoLibrary.shared().performChanges({
                  let request = PHAssetCreationRequest.forAsset()
               var urlGifTemp = self.gifUser.url
               urlGifTemp.removeLast()
               if urlGifTemp.starts(with: "192.168.1.25") {
                   urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
               }
               request.addResource(with: .photo, data: self.fullGifImageView.animatedImage.data, options: nil)
              }) { (success, error) in
                  if let error = error {
                   
                   print("Gif can't be saved : ",error.localizedDescription)
                      //completion(.failure(error))
                  } else {
                     print("Gif saved")
                     // completion(.success(true))
                  }
              }
          
       }
       
       @IBAction func shareGifAction(_ sender: UIButton) {
           if isGif {
       let shareData: Data = self.fullGifImageView.animatedImage.data
       let firstActivityItem: Array = [shareData]
       let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)
           self.present(activityViewController, animated: true, completion: nil)
       }
       }
       @IBOutlet weak var favoriteBTN: UIButton!
       @IBAction func favoriteGifAction(_ sender: UIButton) {
           sender.isUserInteractionEnabled = false
           if sender.image(for: .normal) == UIImage(named: "favoriteProfile"){
               
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
           let a = try JSON(data: dataFromString!)
               if self.isGif {
               let params : Parameters = [
                   "userId" : a["_id"].stringValue,
                   "gifId" : self.gifUser.id
               ]
               let headers : HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
               ]
               print(params)
               Alamofire.request(URL(string: ScriptBase.sharedInstance.addLikeGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                   if response.error == nil {
                       let data = JSON(response.data)
                       if data["status"].exists() {
                           if data["status"].boolValue {
                               self.getGifsFavoritesFormDataBase()
                               sender.setImage(UIImage(named: "favoriteFull"), for: .normal)
                           }else{
                             
                           }
                       }
                         sender.isUserInteractionEnabled = true
                   }else{
                       sender.isUserInteractionEnabled = true
                   }
               }
               }else{
                   let params : Parameters = [
                               "userId" : a["_id"].stringValue,
                               "mimeId" : self.gifUser.id
                           ]
                           let headers : HTTPHeaders = [
                                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                           ]
                           print(params)
                           Alamofire.request(URL(string: ScriptBase.sharedInstance.addLikeMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                               if response.error == nil {
                                   let data = JSON(response.data)
                                   if data["status"].exists() {
                                       if data["status"].boolValue {
                                           self.getMemesFavoritesFormDataBase()
                                           sender.setImage(UIImage(named: "favoriteFull"), for: .normal)
                                       }else{
                                         
                                       }
                                   }
                                     sender.isUserInteractionEnabled = true
                               }else{
                                   sender.isUserInteractionEnabled = true
                               }
                           }
               }
           }catch {
                       
           }
           }else{
               let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
               let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
               do {
                   if self.isGif {
               let a = try JSON(data: dataFromString!)
                   let params : Parameters = [
                       "userId" : a["_id"].stringValue,
                       "gifId" : self.gifUser.id
                   ]
                   let headers : HTTPHeaders = [
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                   ]
                   Alamofire.request(URL(string: ScriptBase.sharedInstance.removeLikeGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                       if response.error == nil {
                           let data = JSON(response.data)
                           if data["status"].exists() {
                               if data["status"].boolValue {
                                  sender.isUserInteractionEnabled = true
                                   sender.setImage(UIImage(named: "favoriteProfile"), for: .normal)
                                   self.getGifsFavoritesFormDataBase()
                               }else{
                                   
                               }
                           }
                       }else{
                           
                       }
                   }
                   }else{
                       let a = try JSON(data: dataFromString!)
                                      let params : Parameters = [
                                          "userId" : a["_id"].stringValue,
                                          "mimeId" : self.gifUser.id
                                      ]
                                      let headers : HTTPHeaders = [
                                           "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                      ]
                                      Alamofire.request(URL(string: ScriptBase.sharedInstance.removeLikeMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                          if response.error == nil {
                                              let data = JSON(response.data)
                                              if data["status"].exists() {
                                                  if data["status"].boolValue {
                                                     sender.isUserInteractionEnabled = true
                                                      sender.setImage(UIImage(named: "favoriteProfile"), for: .normal)
                                                      self.getMemesFavoritesFormDataBase()
                                                  }else{
                                                      
                                                  }
                                              }
                                          }else{
                                              
                                          }
                                      }
                   }
               }catch {
                           
               }
           }
       }
       //
    var gifsModel : [GifUser] = []
       var MimesModel : [GifUser] = []
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
           let shareLink = ["http://gifmemer.com/download"]
           let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
           activityVC.popoverPresentationController?.sourceView = self.view
           self.present(activityVC, animated: true, completion: nil)
       }
    
    
    @IBOutlet weak var NoFavoriteLBL : UILabel!
    @IBOutlet weak var favoritesLBL : UILabel!
       @objc func configureViewFromLocalisation(){
              NoFavoriteLBL.text = Localization("NoFavorite")
            
              favoritesLBL.text = Localization("favorites")
        self.tapFiveLBL.text = Localization("tapFive")
        self.clipKeyboardBTN.setTitle(Localization("ClipKeyboard"), for: .normal)
          }
          @objc func receiveLanguageChangedNotification(notification:NSNotification) {
              if notification.name == kNotificationLanguageChanged {
                  configureViewFromLocalisation()
              }
          }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                   sessionDataTask.forEach{ $0.cancel()}
                   uploadData.forEach{ $0.cancel()}

                   downloadData.forEach{ $0.cancel()}

               }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if oneTimeWork {
           self.oneTimeWork = false
        selectorContainer.frame = self.GifsContainer.frame
            self.view.bringSubviewToFront(self.GifsContainer)
            self.view.bringSubviewToFront(self.gifsLabel)
            self.view.bringSubviewToFront(self.MemesContainer)
            self.view.bringSubviewToFront(self.memesLabel)
            self.view.bringSubviewToFront(self.showFullGifView)
        }
    }
    func initSelector(){
        memesLabel.font = UIFont.init(name: "SegoeUI-Regular", size: 18)
        memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)
        gifsLabel.font = UIFont.init(name: "SegoeUI-Bold", size: 18)
        gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
        selectorContainer = UIView(frame: self.GifsContainer.frame)
        selectorContainer.backgroundColor = .white
        self.view.addSubview(selectorContainer)
       
        //selectorContainer.layoutIfNeeded()
        let tapgestureGifs = UITapGestureRecognizer(target: self, action: #selector(self.goToGifs))
        let tapgestureMimes = UITapGestureRecognizer(target: self, action: #selector(self.goToMimes))
        MemesContainer.isUserInteractionEnabled = true
        MemesContainer.addGestureRecognizer(tapgestureMimes)
        GifsContainer.isUserInteractionEnabled = true
        GifsContainer.addGestureRecognizer(tapgestureGifs)
    }
    @objc func goToMimes(){
           isGif = false
          // self.pendingLBL.text = Localization("MyMimes")
           //self.myLBL.text =  Localization("MyMimes")

           UIView.performWithoutAnimation {
           self.gifsLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
           self.memesLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
           }
           //self.collectionMeme.alpha = 0
           //self.collectionMeme.isHidden = false
           UIView.animate(withDuration: 0.2, animations: {
               
               self.gifsLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

               self.memesLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
             self.selectorContainer.transform = CGAffineTransform(translationX: (self.MemesContainer.frame.origin.x), y: 0)
              // self.collectionMeme.alpha = 1
           
            }, completion: nil)
           self.collection.reloadData()
           
       }
       @objc func goToGifs(){
         isGif = true
           //self.pendingLBL.text = "Pending Gifs"
          // self.myLBL.text = "My Gifs"
           UIView.performWithoutAnimation {
               self.memesLabel.font = UIFont(name: "SegoeUI-Regular", size: 18)
               self.gifsLabel.font = UIFont(name: "SegoeUI-Bold", size: 18)
           }
           UIView.animate(withDuration: 0.2, animations: {
                //self.collectionMeme.alpha = 0
                   self.memesLabel.textColor = UIColor.init(red: 228 / 255, green: 228 / 255, blue: 228 / 255, alpha: 1)

                   self.gifsLabel.textColor = UIColor.init(red: 176 / 255, green: 18 / 255, blue: 164 / 255, alpha: 1)
                   self.selectorContainer.transform = CGAffineTransform.identity
                 // self.selectorContainer.layoutIfNeeded()
                  }, completion: {complete in
                   
                   // self.collectionMeme.isHidden = true
           })
           
           self.collection.reloadData()
           //self.collectionMyGifs.reloadData()
       }
    @objc func cancelKeyboardAction(_ sender : UITapGestureRecognizer) {
        self.favoritesLBL.text = Localization("favorites")
        self.clipKeyboardBTN.isHidden = false
        self.clipeKeyboardSuplementary.isHidden = false
        self.isForKeybord = false
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionTopLayout.constant = 0
            self.tapFiveLBL.isHidden = false
            
        }) { (complete) in
           

            self.collection.reloadData()
        }
    }
    @objc func saveKeyboardAction(_ sender : UITapGestureRecognizer) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initSelector()
        let tapgestureCancelKeyboard = UITapGestureRecognizer(target: self, action: #selector(self.cancelKeyboardAction(_:)))
         //let tapgestureSaveKeyboard = UITapGestureRecognizer(target: self, action: #selector(self.saveKeyboardAction(_:)))
        self.cancelKeyboardIMG.isUserInteractionEnabled = true
        //self.saveKeyboardIMG.isUserInteractionEnabled = true
        self.cancelKeyboardIMG.addGestureRecognizer(tapgestureCancelKeyboard)
        //self.saveKeyboardIMG.addGestureRecognizer(tapgestureSaveKeyboard)
        self.saveKeyboardIMG.isHidden = true
         NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        collection.delegate = self
        collection.dataSource = self
        collection.isHidden = false
        collectionViewLayout.delegate = self
        
        print("viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else {
            return
        }
               
               
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
              let a = try JSON(data: dataFromString!)
            let firstName = a["userFirstName"].stringValue
            print(a)
            self.zonzLBL.text = a["zonz"].stringValue

            if firstName != "" {
                self.profileNameLabel.text = firstName
            }else{
                self.profileNameLabel.text = ""
            }
            let profilePic = a["userImageURL"].stringValue
            if profilePic != ""{
            self.profilePicture.setImage(with: URL(string: ScriptBase.Image_URL + profilePic), placeholder: UIImage(named: "artist"), displayOptions: [.withTransition], transformer: nil, progress: nil, completion: nil)
                
            }
        }catch{
            print("GfyCatTrendingController : ",error.localizedDescription)
        }
        getGifsFavoritesFormDataBase()
        getMemesFavoritesFormDataBase()
    }
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
      if let cell = cell as? GiphyCollectionViewCell {
        if isGif {
            //cell.layer.
            
            //cell.updateFrame()
            cell.imageView.layer.sublayers?.forEach({ (layer) in
                print(layer.name)
            })
            //print("sublayers: ",)
            cell.playLayer()
            print("frame layer : ",cell.moviePlayerLayer.frame)
        }
                  
                   }
    }
    func getGifsFavoritesFormDataBase(){
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
           let a = try JSON(data: dataFromString!)
           let params : Parameters = [
               "userId" : a["_id"].stringValue
               
               ]
           let headers : HTTPHeaders = [
           "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                       ]
            self.gifsModel = []
            self.numbersOfKeyboardGif = 0
           Alamofire.request(URL(string: ScriptBase.sharedInstance.myFavoritesGifs)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
               if response.error == nil {
               let data = JSON(response.data)
                   print("favoriteGifs: ",data)
                if data["message"].exists() {
                    self.collection.isHidden = true
                }
                       if data.arrayObject?.count == 0 {
                           self.collection.isHidden = true
                           //self.defaultLBL.text = "No Notifications"
                           //self.defaultLBL.textAlignment = .center
                       }
                        if data.arrayObject != nil {
                            self.collection.isHidden = false
                            for i in 0...(data.arrayObject!.count - 1) {
                                let gif = GifUser(id: data[i]["_id"].stringValue, gifId: Gif(id: data[i]["gifId"]["_id"].stringValue, gifName: data[i]["gifId"]["gifName"].stringValue, giphyId: data[i]["gifId"]["giphyId"].stringValue, priceGif: data[i]["gifId"]["priceGif"].stringValue, gifUrl: data[i]["gifId"]["gifUrl"].stringValue, gifHeight: data[i]["gifId"]["gifHeight"].stringValue, gifWidth: data[i]["gifId"]["gifWidth"].stringValue), gifDate: data[i]["gifDate"].stringValue, gifPaid: data[i]["gifPaid"].boolValue, url: data[i]["url"].stringValue, favorite: data[i]["favorite"].boolValue,cliped: data[i]["cliped"].boolValue)
                                           
                                if gif.cliped {
                                    self.numbersOfKeyboardGif += 1
                                }
                                self.gifsModel.append(gif)
                                                      //self.myGifsModel.append(gif)
                                                 
                                             }
                            if self.isGif {
                                if self.gifsModel.count == 0 {
                                    self.clipKeyboardBTN.isHidden = true
                                    self.collection.isHidden = true
                                    
                                }else{
                                    self.clipKeyboardBTN.isHidden = false
                                     self.collection.isHidden = false
                                }
                              self.collection.reloadData()
                            }
                                         }
                     
                       
                       
                       }else{
                                                  
                           }
               }
           }catch{
               
           }
           }
    func getMemesFavoritesFormDataBase(){
              let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
              let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
              do {
              let a = try JSON(data: dataFromString!)
              let params : Parameters = [
                  "userId" : a["_id"].stringValue
                  
                  ]
              let headers : HTTPHeaders = [
              "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                          ]
                self.MimesModel = []
                self.numbersOfKeyboardMeme = 0
              Alamofire.request(URL(string: ScriptBase.sharedInstance.myFavoritesMemes)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                  if response.error == nil {
                  let data = JSON(response.data)
                      print("favoriteMemes: ",data)

                if data.arrayObject != nil {
                    for i in 0...(data.arrayObject!.count - 1) {
                        self.collection.isHidden = false
                        let gif = GifUser(id: data[i]["_id"].stringValue, gifId: Gif(id: data[i]["mimeId"]["_id"].stringValue, gifName: data[i]["mimeId"]["mimeName"].stringValue, giphyId: data[i]["mimeId"]["giphyId"].stringValue, priceGif: data[i]["mimeId"]["priceMime"].stringValue, gifUrl: data[i]["mimeId"]["mimeUrl"].stringValue, gifHeight: data[i]["mimeId"]["gifHeight"].stringValue, gifWidth: data[i]["mimeId"]["gifWidth"].stringValue), gifDate: data[i]["mimeDate"].stringValue, gifPaid: data[i]["mimePaid"].boolValue, url: data[i]["url"].stringValue, favorite: data[i]["favorite"].boolValue,cliped: data[i]["cliped"].boolValue)
                        if gif.cliped {
                            self.numbersOfKeyboardMeme += 1
                        }
                            self.MimesModel.append(gif)
                                                 
                                             }
                    if self.isGif == false {
                        self.collection.reloadData()
                    }
                                         }
                          }else{
                                                     
                              }
                  }
              }catch{
                  
              }
              }
}
// MARK: - SwiftyGiphyGridLayoutDelegate
extension FavoriteController: SwiftyGiphyGridLayoutDelegate {

    public func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat
    {
        if isGif  {
       guard let imageSet = self.gifsModel[indexPath.row].gifId else {
        return 96.5
        }
        guard let width = NumberFormatter().number(from: imageSet.gifWidth) else {  return 0.0 }
        guard let height = NumberFormatter().number(from: imageSet.gifHeight) else {  return 0.0 }
        print(width)
        print(height)
        print("size will be : ", AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height)
        return AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }else{
           guard let imageSet = self.MimesModel[indexPath.row].gifId else {
                return 0.0
           }
           guard let width = NumberFormatter().number(from: imageSet.gifWidth) else {  return 0.0 }
           guard let height = NumberFormatter().number(from: imageSet.gifHeight) else {  return 0.0 }
           print(width)
           print(height)
           return AVMakeRect(aspectRatio: CGSize(width: CGFloat(truncating: width), height: CGFloat(truncating: height)), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
        }

    }
}
// MARK: - UICollectionViewDataSource
extension FavoriteController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isGif {
            guard let cell = cell  as? GifFavoriteCollectionViewCell else{
                return
            }
            cell.removeLayer()
        }
       }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isGif {
        
            return self.gifsModel.count
        }else{
            return self.MimesModel.count
        }
        
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//GifFavoriteCollectionViewCell
         if self.isGif {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gifcell", for: indexPath) as! GifFavoriteCollectionViewCell
       
            if let _ = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout
        {
            var urlGifTemp = gifsModel[indexPath.row].url
             urlGifTemp.removeLast()
                              if urlGifTemp.starts(with: "192.168.1.25") {
                                  urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                              }
            cell.configureGif(gifUrl: URL(string: ScriptBase.Image_URL + urlGifTemp )!)
            //cell.configureForOwnGifsFavorite(url: )
            if gifsModel[indexPath.row].cliped {
                cell.activateClipKeyboard()
            }else{
                cell.deactivateClipKeyboard()
            }
            if isForKeybord {
                cell.doLayout = false
                cell.animateClipKeyboard()
            }else{
                cell.doLayout = true
                cell.deAnimateClipKeyboard()
            }

        }
          return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSwiftyGiphyCollectionViewCell, for: indexPath) as! GiphyCollectionViewCell
            if let _ = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout
                   {
                       
                         var urlGifTemp = gifsModel[indexPath.row].url
                                  urlGifTemp.removeLast()
                                                   if urlGifTemp.starts(with: "192.168.1.25") {
                                                       urlGifTemp = urlGifTemp.deletingPrefix("192.168.1.25/")
                                                   }
                                 cell.configureForOwnMemeFavorite(url: URL(string: ScriptBase.Image_URL + urlGifTemp + ".jpg")!)
                   }
              return cell
        }
      
    }
}
// MARK: - UICollectionViewDelegate
extension FavoriteController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if let cell = collectionView.cellForItem(at: indexPath) as? GiphyCollectionViewCell {
            if self.isForKeybord == false {
        self.showFullGifView.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
            self.showFullGifView.alpha = 1
            }) { (complete) in
                if self.isGif {
                self.fullGifImageView.sd_cacheFLAnimatedImage = false
                self.fullGifImageView.contentMode = .scaleAspectFill
                self.fullGifImageView.sd_setShowActivityIndicatorView(true)
                self.fullGifImageView.sd_setIndicatorStyle(.gray)
                self.fullGifImageView.image = cell.imageView.image
                self.fullGifNameLBL.text = self.gifsModel[indexPath.row].gifId?.gifName
                self.gifUser = self.gifsModel[indexPath.row]
                self.favoriteBTN.setImage(self.gifsModel[indexPath.row].favorite ? UIImage(named: "favoriteFull") : UIImage(named: "favoriteProfile"), for: .normal)
                self.fullGifImageView.sd_setImage(with: cell.gifUrl) { (image, error, cacheType, url) in
                    self.fullGifImageView.startAnimatingGIF()
                    
                    }
                }else{
                    self.fullGifImageView.sd_cacheFLAnimatedImage = false
                    self.fullGifImageView.contentMode = .scaleAspectFill
                    self.fullGifImageView.sd_setShowActivityIndicatorView(true)
                    self.fullGifImageView.sd_setIndicatorStyle(.gray)
                    self.fullGifImageView.image = cell.imageView.image
                    self.fullGifNameLBL.text = self.MimesModel[indexPath.row].gifId?.gifName
                    self.gifUser = self.MimesModel[indexPath.row]
                    self.favoriteBTN.setImage(self.MimesModel[indexPath.row].favorite ? UIImage(named: "favoriteFull") : UIImage(named: "favoriteProfile"), for: .normal)
                    self.fullGifImageView.sd_setImage(with: cell.gifUrl) { (image, error, cacheType, url) in
                        self.fullGifImageView.startAnimatingGIF()
                        
                        }
                }
                }
            }else{
                if isGif {
                if cell.doLayout {
                    if self.numbersOfKeyboardGif < 5 {
                        
                            
                        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            do {
                            let a = try JSON(data: dataFromString!)
                                let params : Parameters = [
                                    "userId" : a["_id"].stringValue,
                                    "gifId" : self.gifsModel[indexPath.row].id
                                ]
                                let headers : HTTPHeaders = [
                                     "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                ]
                                Alamofire.request(URL(string: ScriptBase.sharedInstance.addKeyboardGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                    if response.error == nil {
                                        let data = JSON(response.data)
                                        print("AddKeyboard : ",data)
                                        if data["status"].exists() {
                                            if data["status"].boolValue {
                                            cell.activateClipKeyboard()
                                            }else{
                                                
                                            }
                                        }
                                    }else{
                                        
                                    }
                                }
                            }catch{
                                
                        }
                 
                    }else{
                        ///Vibrate
                          AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                    ///UpdateGif
                }else{
                                      let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                                      let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                          do {
                                          let a = try JSON(data: dataFromString!)
                                              let params : Parameters = [
                                                  "userId" : a["_id"].stringValue,
                                                  "gifId" : self.gifsModel[indexPath.row].id
                                              ]
                                              let headers : HTTPHeaders = [
                                                   "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                              ]
                                              Alamofire.request(URL(string: ScriptBase.sharedInstance.removeKeyboardGif)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                                  if response.error == nil {
                                                      let data = JSON(response.data)
                                                      if data["status"].exists() {
                                                          if data["status"].boolValue {
                                                            self.numbersOfKeyboardGif += 1
                                                          cell.deactivateClipKeyboard()
                                                          }else{
                                                              
                                                          }
                                                      }
                                                  }else{
                                                      
                                                  }
                                              }
                                          }catch{
                                              
                                      }
                   
                    ///Update Gif
                }
                }else{
                  if cell.doLayout {
                                     if self.numbersOfKeyboardMeme < 5 {
                                         
                                             
                                         let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                                         let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                             do {
                                             let a = try JSON(data: dataFromString!)
                                                 let params : Parameters = [
                                                     "userId" : a["_id"].stringValue,
                                                     "gifId" : self.gifsModel[indexPath.row].id
                                                 ]
                                                 let headers : HTTPHeaders = [
                                                      "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                                 ]
                                                 Alamofire.request(URL(string: ScriptBase.sharedInstance.addKeyboardMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                                     if response.error == nil {
                                                         let data = JSON(response.data)
                                                         print("AddKeyboard : ",data)
                                                         if data["status"].exists() {
                                                             if data["status"].boolValue {
                                                             cell.activateClipKeyboard()
                                                             }else{
                                                                 
                                                             }
                                                         }
                                                     }else{
                                                         
                                                     }
                                                 }
                                             }catch{
                                                 
                                         }
                                  
                                     }else{
                                         ///Vibrate
                                           AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                                     }
                                     ///UpdateGif
                                 }else{
                                                       let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                                                       let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                                           do {
                                                           let a = try JSON(data: dataFromString!)
                                                               let params : Parameters = [
                                                                   "userId" : a["_id"].stringValue,
                                                                   "gifId" : self.gifsModel[indexPath.row].id
                                                               ]
                                                               let headers : HTTPHeaders = [
                                                                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                                                               ]
                                                               Alamofire.request(URL(string: ScriptBase.sharedInstance.removeKeyboardMeme)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                                                                   if response.error == nil {
                                                                       let data = JSON(response.data)
                                                                       if data["status"].exists() {
                                                                           if data["status"].boolValue {
                                                                             self.numbersOfKeyboardMeme += 1
                                                                           cell.deactivateClipKeyboard()
                                                                           }else{
                                                                               
                                                                           }
                                                                       }
                                                                   }else{
                                                                       
                                                                   }
                                                               }
                                                           }catch{
                                                               
                                                       }
                                    
                                     ///Update Gif
                                 }
                    
                }
            }
            }
        

        //searchController.isActive = false
        //delegate?.giphyControllerDidSelectGif(controller: self, item: selectedGif)
    }
    func traitGif(collection:UICollectionView, gifId:String, gifUrl: String,nameGif : String){
           
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
               let a = try JSON(data: dataFromString!)
               
               let settings : Parameters = [
                   "userId" : a["_id"].stringValue,
                   "gifId" : gifId,
                   "gifUrl" : gifUrl,
                   "name" : nameGif
               ]
               let headers : HTTPHeaders = [
                   "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
               ]
               print(settings)
               print(headers)
               Alamofire.request(ScriptBase.sharedInstance.addGif, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                   let data = JSON(response.data ?? Data())
                if data["status"].boolValue {
                    collection.isUserInteractionEnabled = true

                    let alert = UIAlertController(title: "Processing GIF", message: "We are processing your selected gif, we will notify you as soon as it's finished", preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    collection.isUserInteractionEnabled = true

                    let alert = UIAlertController(title: "Processing GIF", message: data["message"].stringValue, preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                }
                
                  
               }
               
           }catch {
               
           }
       }
       
}
extension FavoriteController : UISearchBarDelegate {
    func filterContentForSearchText(searchText:String){
        let url = URL(string:("https://api.gfycat.com/v1/gfycats/search?search_text=" + searchText.stringByAddingPercentEncodingForFormData()!))!
        print(url.absoluteString)
Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { (data) in
             var tempGifs : [GfycatMedia] = []
    
            if data.response?.statusCode == 200 {
                let dataParser = JSON( data.data)
                if dataParser["gfycats"].arrayObject != nil {
                if dataParser["gfycats"].arrayObject?.count != 0 {
                    for i in 0...((dataParser["gfycats"].arrayObject?.count)! - 1) {
                                           
                                            //tempGifs.append(GfycatMedia(gfyId: dataParser["gfycats"][i]["gfyId"].stringValue))
                if ( dataParser["gfycats"][i]["nsfw"].stringValue == "0") {
                    tempGifs.append(GfycatMedia(info: dataParser["gfycats"][i].rawValue as! [AnyHashable : Any]))
                                          }
                                        }
                                    }
                                }
                
                self.collection.reloadData()
                
            }
        
        }
          
       }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            self.isSearching = true
            self.filterContentForSearchText(searchText: searchText)
            self.collection.reloadData()
        }else{
            self.isSearching = false
            self.collection.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.collectionSearchBar.resignFirstResponder()
        self.collectionSearchBar.text = ""
        self.isSearching = false
        self.collectionSearchBar.removeFromSuperview()
        self.collection.reloadData()
        self.collection.isHidden = true
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = true
        self.view.endEditing(true)
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.collectionSearchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //self.isSearching = false
          self.collectionSearchBar.setShowsCancelButton(false, animated: false)
    }
    
    
}
