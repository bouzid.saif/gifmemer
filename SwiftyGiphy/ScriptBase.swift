//
//  ScriptBase.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
class ScriptBase {
    static var sharedInstance = ScriptBase()
    private init() {
        
    }
     //****************BackEnd-Server**************************///
    //"192.168.1.102"
    static let isRemote = true
    static let DNSServer =  isRemote == true ? "gifmemer.com" : "192.168.1.25"
    static let Base_URL = "http://" + DNSServer 
    static let Base_URLMiddle = "http://" + DNSServer + ":5000"
    static let socket_URL = "http://" + DNSServer + ":3030"
    static let socket_URLMiddle = "http://" + DNSServer + ":5000"
    static let socket_URLUpdate = "http://" + DNSServer + ":5001"
    static let janus_URI = "ws://" + DNSServer + ":8188"
    static let Image_URL = "http://" + DNSServer +  "/"
    static let client_id = "2_OuNFTz"
    static let client_secret = "WP1T_TtBYVFE9O3rNWeAyziINQgmAD0OypUYO2yKSLk20OjdNozWlE6T31HZV9Vr"
    static var client_token = ""
    static var refresh_token = ""
     //****************USERS**************************///
    var testScript = "http://" + DNSServer + ":8088/janus/"
    var JANUS_URL = "http://" + DNSServer + ":8088/janus/"
    
     /*
     *POST
     *firstName,lastName,password,age,email
     */
    var registerUser = Base_URL + "/users/registerUser"
    
    /*
     *POST
     *firstName,lastName,password,age,email
     */
    var registerApi = Base_URL + "/users/registerAPI"
    
    /*
     *POST
     *userId
     */
    var uploadImage = Base_URL + "/users/uploadImage"
    
    var uploadImageCustom = Base_URL + "/users/uploadImageCustom"
    /*
     *POST
     *email,password
     */
    var login = Base_URL + "/users/login"
    
    var setIosPlayerId = Base_URL + "/users/setNotification"
    /*
     *POST
     *Header : x-access-token
     *userEmail
     */
    var forgotPassword = Base_URL + "/users/forgot"
    /*
        *POST
        *Header : x-access-token
        *userEmail,code
        */
    var verifySecurityCode = Base_URL + "/users/verifySecurityCode"
    /*
        *POST
        *Header : x-access-token
        *userEmail,code,password
        */
    var resetPassword = Base_URL + "/users/resetPassword"
    /*
     *POST
     *Header : x-access-token
     *firstName,lastName,age,userImageURL,userName
     */
    var updateUser = Base_URL + "/users/updateUser"
    
    var checkConncetion = Base_URL + "/users/checkConnection"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var getZonz = Base_URL + "/users/user"
    
    /**
    *POST
    *Header :
    *email
    */
     var resendEmail = Base_URL + "/users/resendValidationEmail"
    /**
     *POST
     *Header : x-access-token
     *userId , type (adnroid, ios)
     */
    var deletePushNotification = Base_URL + "/users/deleteNotification"
    /**
     *POST
     *Header : x-access-token
     *userId , type (android, ios)
     */
    var enablePushNotification = Base_URL + "/users/enablePushNotification"
    /**
     *POST
     *Header : x-access-token
     *userId , type (android, ios)
     */
    var disablePushNotification = Base_URL + "/users/disablePushNotification"
    /**
     *POST
     *Header : x-access-token
     *userId,purchaseZonz(String numbers),purchaseMoney(String number),currency(String Euro Dollars)
     */
    var zonzPayment = Base_URL + "/users/zonzPurchase"
     //****************GIFS**************************///
    /**
     *POST
     *Header : x-access-token
     *gifId, userId, gifUrl,name,price
     */
    var addGif = Base_URL + "/gifs/addGif"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var myGifs = Base_URL + "/gifs/MyGifs"
    
    /**
     *POST
     *Header : x-access-token
     *gifId(UserGifId)
     */
    var payGif = Base_URL + "/gifs/payGif"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
    var addLikeGif = Base_URL + "/gifs/addLike"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
    var addKeyboardGif = Base_URL + "/gifs/addKeyboard"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
     var removeLikeGif = Base_URL + "/gifs/removeLike"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
    var removeKeyboardGif = Base_URL + "/gifs/removeKeyboard"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var myFavoritesGifs = Base_URL + "/gifs/MyFavorites"
    
    var categoriesGifs = Base_URL + "/adminMobile/getCategoryGifs"
    var gifsByRegion = Base_URL + "/adminMobile/getGifsByRegion"
    var gifsTrending = Base_URL + "/adminMobile/getTrending"
    var accessTokenGfy = Base_URL + "/adminMobile/getGfycatToken"
     //****************MIMES**************************///
    /**
     *POST
     *Header : x-access-token
     *mimeId, userId, mimeUrl,name,price
     */
    var addMime = Base_URL + "/mimes/addMime"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var myMimes = Base_URL + "/mimes/MyMimes"
    /**
     *POST
     *Header : x-access-token
     *mimeId(UserMimeId)
     */
    var payMime = Base_URL + "/mimes/payMime"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
    var addLikeMeme = Base_URL + "/mimes/addLike"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
     var removeLikeMeme = Base_URL + "/mimes/removeLike"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
    var addKeyboardMeme = Base_URL + "/mimes/addKeyboard"
    /**
     *POST
     *Header : x-access-token
     *gifId, userId
     */
     var removeKeyboardMeme = Base_URL + "/mimes/removeKeyboard"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
       var myFavoritesMemes = Base_URL + "/mimes/MyFavorites"
     //****************NOTIFICATIONS**************************///
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var deleteNotification = Base_URL + "/notifications/deleteNotifications"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var myNotifications = Base_URL + "/notifications/MyNotifications"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var seenNotifications = Base_URL + "/notifications/seen"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var notSeenNotifications = Base_URL + "/notifications/notSeen"
    
     //****************GifMy-API**************************///
    static let Base_URLGif = "http://api.com/v1/gifs/"
    static let Base_URLGifAnnex = "http://api.gifphy.com/v1/gifs"
    static let api_key_gifs = "umqgJrIgrN7bflp0esheVj45Jlzt6zI5"
    
    /**
    *POST
    *api_key , limit , offset , rating  , random_id
    */
    var searchGifs = Base_URLGif + "search"
    /**
    *POST
    *api_key , q, limit , offset , rating , lang , random_id
    */
    var trendingGifs = Base_URLGifAnnex + "/trending"
    /**
    *POST
    *{gif_id}, api_key ,  random_id
    */
    var getGifById = Base_URLGifAnnex + "/"
    /**
    *POST
    *api_key , ids,  random_id
    */
    var getGifByIds = Base_URLGifAnnex
    
     //****************GifMy-Tokens**************************///
    
    /*
     import Foundation
    class GiphyApiKeys {
        static let sharedInstance = GiphyApiKeys()
         var giphyTokens = ["umqgJrIgrN7bflp0esheVj45Jlzt6zI5","UeIbNPyvO25m7uVwxTexyRS3A92zkipd","c6WKXNmVXhzkePiiGOqiBs73h2ek6vHy"]
        var defaultKey = "umqgJrIgrN7bflp0esheVj45Jlzt6zI5"
        func changeApiKey(){
            switch defaultKey {
            case giphyTokens[0]:
                defaultKey = giphyTokens[1]
            case giphyTokens[1]:
                defaultKey = giphyTokens[2]

            case giphyTokens[2]:
                defaultKey = giphyTokens[0]

            default:
                break
            }
        }
     
     }*/
    }

