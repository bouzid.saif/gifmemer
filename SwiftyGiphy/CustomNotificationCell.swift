//
//  CustomNotificationCell.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-12-29.
//  Copyright © 2019 52inc. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
class CustomNotificationCell: UITableViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var gifMemeImageView: FLAnimatedImageView!
    @IBOutlet weak var messageLBL: UILabel!
    @IBOutlet weak var clickProfileLBL : UILabel!
    @IBOutlet weak var dateLBL : UILabel!
      var currentFrame : UIImage? = nil
     var gifURL : URL!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureGif(gifUrl : URL ) {
        self.gifURL = gifUrl
        //let holdgesture = UILongPressGestureRecognizer(target: self, action: #selector(self.didHoldPressGif(_:)))
        //holdgesture.minimumPressDuration = 0.7
       // self.addGestureRecognizer(holdgesture)
        if self.currentFrame == nil {
        gifMemeImageView.sd_cacheFLAnimatedImage = false
        gifMemeImageView.setShouldResizeFrames(false)
        gifMemeImageView.contentMode = .scaleAspectFill
        gifMemeImageView.sd_setShowActivityIndicatorView(true)
        gifMemeImageView.sd_setIndicatorStyle(.gray)
        
       // imageViewGif.sd_setImage(with: gifUrl)
            
          /*  guard let info =  Decoder().decode(gifUrl: gifUrl) else {
                    return
        }
            
            self.imageViewGif.image = info.images[0]
            self.currentFrame = info.images[0] */
           
        gifMemeImageView.sd_setImage(with: gifUrl) { (image, error, cacheType, url) in
            //print(image?.size)
            if self.currentFrame == nil {
            self.currentFrame = self.gifMemeImageView.currentFrame
            self.gifMemeImageView.image = self.currentFrame
            let frame =  CGRect(x: self.gifMemeImageView.frame.origin.x, y: self.gifMemeImageView.frame.origin.y, width: 44, height: 44)
                
            self.gifMemeImageView.frame = frame
            self.gifMemeImageView.layoutIfNeeded()
            }
            print(self.gifMemeImageView.frame.size)
             self.gifMemeImageView.startAnimatingGIF()
        }
           
       // imageViewGif.setGifFromURL(gifUrl)
        /*imageViewGif.prepareForAnimation(withGIFURL: (gifUrl)){
            self.gifIsReady = true
        } */
        
        }else{
            gifMemeImageView.sd_setImage(with: gifUrl) { (image, error, cacheType, url) in
                       //print(image?.size)
                       if self.currentFrame == nil {
                       self.currentFrame = self.gifMemeImageView.currentFrame
                       self.gifMemeImageView.image = self.currentFrame
                       let frame =  CGRect(x: self.gifMemeImageView.frame.origin.x, y: self.gifMemeImageView.frame.origin.y, width: 181, height: 122)
                           
                       self.gifMemeImageView.frame = frame
                       self.gifMemeImageView.layoutIfNeeded()
                       }
                       print(self.gifMemeImageView.frame.size)
            }
            /*guard let info =  Decoder().decode(gifUrl: gifUrl) else {
                               return
                   }
            if self.imageViewGif.image != info.images[0] {
              self.currentFrame = info.images[0]
                self.imageViewGif.image = info.images[0]
            } */
            
        }
    }

}
class CustomNotificationVideoCell: UITableViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var gifMemeImageView: UIImageView!
    @IBOutlet weak var messageLBL: UILabel!
    @IBOutlet weak var clickProfileLBL : UILabel!
    @IBOutlet weak var dateLBL : UILabel!
      var currentFrame : UIImage? = nil
     var gifURL : URL!
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
      var player : AVPlayer?
    override func awakeFromNib() {
        super.awakeFromNib()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        gifMemeImageView.layer.insertSublayer(videoLayer, at: 0)
        // Initialization code
    }
    override func prepareForReuse() {
         self.removeLayer()
        super.prepareForReuse()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
         videoLayer.frame = CGRect(x: 0, y: 0, width: self.gifMemeImageView.frame.width, height: self.gifMemeImageView.frame.height)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var videoURL: String = ""
    func configureGif(gifUrl : URL ) {
        self.gifURL = gifUrl
     self.videoURL = gifUrl.absoluteString + ".mp4"
         self.gifMemeImageView.sd_setImage(with: URL(string: gifUrl.absoluteString + ".jpg"), completed: nil)
         player = AVPlayer(playerItem: AVPlayerItem(url:  URL(string: gifUrl.absoluteString + ".mp4")!))
           player!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        /* videoLayer.backgroundColor = UIColor.clear.cgColor
         videoLayer.videoGravity = AVLayerVideoGravity.resize
         imageViewGif.layer.addSublayer(videoLayer) */
         guard let layers = self.gifMemeImageView.layer.sublayers else {
                return
            }
            if layers.count != 0 {
                 var found = false
                for layer in layers {
                    if layer.name == "Video" {
                        found = true
                     
                    }
                }
             if found == false{
                 videoLayer =  AVPlayerLayer()
              videoLayer.backgroundColor = UIColor.clear.cgColor
             videoLayer.videoGravity = AVLayerVideoGravity.resize
              gifMemeImageView.layer.insertSublayer(videoLayer, at: 0)
             }
            //self.moviePlayerLayer.removeFromSuperlayer()
            }else{
         videoLayer =  AVPlayerLayer()
         videoLayer.backgroundColor = UIColor.clear.cgColor
         videoLayer.videoGravity = AVLayerVideoGravity.resize
         gifMemeImageView.layer.insertSublayer(videoLayer, at: 0)
             }
         self.videoLayer.player = player
         self.videoLayer.name = "Video"
         DispatchQueue.main.async {
             self.player?.play()
         }
          NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    @objc func playerDidFinishPlayingt(note : Notification) {
           player!.seek(to: CMTime.zero)
           player!.play()
       }
    func removeLayer(){
        
        player?.pause()
        player?.replaceCurrentItem(with: nil)
        
        NotificationCenter.default.removeObserver(self)
        guard let layers = self.gifMemeImageView.layer.sublayers else {
            return
        }
        if layers.count != 0 {
          
            for layer in layers {
                if layer.name == "Video" {
                    print("layer removed")
                   layer.removeFromSuperlayer()
                }
            }
        //self.moviePlayerLayer.removeFromSuperlayer()
        }
    }
    func playLayer(){
          if player?.currentItem?.asset == nil {
            self.configureGif(gifUrl: self.gifURL)
          }
         /* moviePlayerLayer = AVPlayerLayer(player: moviePlayer)
          moviePlayerLayer.frame = self.imageView.frame
          moviePlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
          moviePlayerLayer.name = "Video"
          self.imageView.layer.insertSublayer(moviePlayerLayer, at: 0) */
          //self.thumbImage.layer.addSublayer(moviePlayerLayer)
      //self.viewGif.layer.addSublayer(moviePlayerLayer)
          print("Will Start Playing")
         player!.play()
          
      }

}
