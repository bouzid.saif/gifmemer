//
//  GifUser.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-12-02.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
class GifUser: NSObject {
    var id : String = ""
    var gifId : Gif? = nil
    var gifDate: String = ""
    var gifPaid : Bool = false
    var url : String = ""
    var favorite: Bool = false
    var cliped: Bool = false
    init(id: String,gifId: Gif  , gifDate : String, gifPaid : Bool, url : String,favorite: Bool) {
        self.id = id
        self.gifId = gifId
        self.gifDate =  gifDate
        self.gifPaid = gifPaid
        self.url = url
        self.favorite = favorite
    }
    init(id: String,gifId: Gif  , gifDate : String, gifPaid : Bool, url : String,favorite: Bool,cliped:Bool) {
        self.id = id
        self.gifId = gifId
        self.gifDate =  gifDate
        self.gifPaid = gifPaid
        self.url = url
        self.favorite = favorite
        self.cliped = cliped
    }
}
