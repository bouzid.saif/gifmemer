//
//  RegisterController.swift
//  YLYL
//
//  Created by macbook on 1/7/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import NotificationBannerSwift
import OneSignal
class RegisterController: ServerUpdateDelegate,UIPickerViewDelegate,UIPickerViewDataSource,OSSubscriptionObserver,UITextFieldDelegate {
   
    /*
     * Full Name Textfield
     */
    @IBOutlet weak var LastNameTF : TextField!
    /*
     * User Name Textfield
     */
    @IBOutlet weak var FirstNameTF : TextField!
    /*
     * Email Textfield
     */
    @IBOutlet weak var EmailTF: TextField!
    /*
     * Password Textfield
     */
    @IBOutlet weak var PasswordTF : PasswordTextField!
  
    @IBOutlet weak var validationOKBTN: UIButton!
    /*
     * Check whether the user has checked the terms or not
     */
    var verifiedTerms : Bool = false
    /*
     * Accept Terms checkbox
     */
    @IBOutlet weak var checkBox: Checkbox!
    /*
     * SignUp Button
     */
    @IBOutlet weak var signUpBTN: UIButton!
    
    @IBOutlet weak var byRegistringTerms: UILabel!
    let _notificationToast = CustomToast.shared
    
    @IBOutlet weak var registerContainer: UIView!
    
    //animation reguster
    
    
    @IBOutlet weak var customRegisterView: UIView!
    @IBOutlet weak var gifmemeLetter: UIImageView!
    
    @IBOutlet weak var verificationMailLBL: UILabel!
    @IBOutlet weak var weSentVerificationLBL: UILabel!
    
    @IBOutlet weak var homeBTN: UIButton!
    @IBOutlet weak var topHomeButtonLayout: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewLayout: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewLayoutSecond: NSLayoutConstraint!
    @IBAction func closeRegister(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func homeBTNAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
        NotificationCenter.default.post(name: NSNotification.Name.init("dissmissIt"), object: nil)
                                           }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        LastNameTF.delegate = self
        FirstNameTF.delegate = self
        EmailTF.delegate = self
        PasswordTF.delegate = self
        self.navigationController?.isNavigationBarHidden = true
       // self.configureImageView()
        self.configurePasswordTF()
        //self.configureGenderTF()
        self.configureCheckBox()
        self.configureConnectButton(button: signUpBTN)
        initTerms()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
  /*  func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.PasswordTF {
            textField.keyboardType = .default
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.PasswordTF {
            textField.keyboardType = .numberPad
        }
    } */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    func initTerms(){
        let privacy = byRegistringTerms.text!
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: privacy)
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: privacy.count - 1))
        byRegistringTerms.attributedText = attributedString
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.go_to_terms(_:)))
        byRegistringTerms.isUserInteractionEnabled = true
        byRegistringTerms.addGestureRecognizer(tapgesture)
    }
    @objc func go_to_terms(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "TermsAService") as! TermsController
        self.present(vc, animated: true, completion: nil)
    }
    /*
     * Configure the style and action of the checkbox
     */
    func configureCheckBox(){
        checkBox.borderStyle = .square
        checkBox.checkmarkStyle = .tick
        checkBox.checkmarkSize = 0.7
        checkBox.uncheckedBorderColor = UIColor.gray
        checkBox.checkedBorderColor = UIColor(red: 23/255, green: 118/255, blue: 174/255, alpha: 1)
        checkBox.valueChanged = { (value) in
            print("square checkbox value change: \(value)")
            self.verifiedTerms = value
        }
       
        
    }
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
        
        button.addTarget(self, action: #selector(self.RegisterWithDataBase), for: .touchUpInside)
    }
    let salutations = ["", Localization("Male"), Localization("Female"),Localization("Other")]
    let defaultSalutations = ["","Male","Female","Other"]
    var finalGender = ""
     let picker  = UIPickerView()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return salutations.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return salutations[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("yellow")
    }


    func configurePasswordTF(){
        PasswordTF.leftViewMode = .always
        PasswordTF.textContentType = .oneTimeCode
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   func doValidationAnimation(){
          //self.scrollView.scrollToBottom(animated: true)
          //self.scrollView.isScrollEnabled = false
          //self.BlurEffect.isHidden = false
    self.customRegisterView.alpha = 0
    self.customRegisterView.isHidden = false
    
          //self.CustomValidation.alpha = 0
          //self.CustomValidation.isHidden = false
          UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
              self.customRegisterView.alpha = 1
            self.registerContainer.alpha = 0
          }) { (okay) in
            
            //self.heightViewLayout.constant =
           
            self.topHomeButtonLayout.constant = -34
            
              UIView.animate(withDuration: 0.5, delay: 0.5, options: [.curveLinear], animations: {
                self.heightViewLayoutSecond.isActive = false
                self.heightViewLayout.isActive = true
                self.customRegisterView.layoutIfNeeded()
                self.homeBTN.layoutIfNeeded()
               
              }, completion: { (okayTwo) in
                self.gifmemeLetter.frame.origin.y =  self.gifmemeLetter.frame.origin.y - 20
                UIView.animate(withDuration: 0.5, animations: {
                     self.gifmemeLetter.layoutIfNeeded()
                }) { (complete) in
                     self.homeBTN.isHidden = false
                }
                 
              })
          }
      }
    @objc func RegisterWithDataBase(){
        if LastNameTF.text != "" && EmailTF.text != "" && PasswordTF.text != ""  && FirstNameTF.text != ""  {
            if checkBox.isChecked == false{
                print ("accept check please")
                let banner = NotificationBanner(customView: self._notificationToast)
              _notificationToast.LabelN.text = Localization("TermsAccept")
                banner.customBannerHeight = 80
                banner.show(queuePosition: .front, bannerPosition: .top)
            }else{
                SwiftSpinner.show("Register...")
                if true{
                
                    var lastName = LastNameTF.text!
                    lastName = lastName.replacingOccurrences(of: (LastNameTF.text?.components(separatedBy: " ").first)! + " ", with: "")
                    let settings : Parameters = [
                        //"firstName" : (LastNameTF.text?.components(separatedBy: " ").first)!.capitalized,
                        "firstName" : FirstNameTF.text!.capitalized,
                        "lastName" : LastNameTF.text!.capitalized,
                        "password" : PasswordTF.text!,
                        "email" : EmailTF.text!
                    
                    ]
                    print(settings)
                    Alamofire.request(ScriptBase.sharedInstance.registerUser, method: .post, parameters: settings,encoding: JSONEncoding.default).responseJSON { response in
                        if response.error == nil {
                            SwiftSpinner.hide()
                        let resp = JSON(response.data)
                            print(resp)
                            if resp["message"].exists() {
                                if resp["message"].stringValue == "Email already exist" {
                                    print("User already exist")
                                    let banner = NotificationBanner(customView: self._notificationToast)
                                    self._notificationToast.LabelN.text = Localization("UserExist") + "!"
                                    banner.customBannerHeight = 80
                                    banner.show(queuePosition: .front, bannerPosition: .top)
                                }else if resp["message"].stringValue == "Thanks for registering with us, Please Validate Your Email" {
                                   //MARK: TODO REGISTRING
                            UserDefaults.standard.setValue(resp["user"].rawString(), forKey: "UserZonzay")
                                                                   UserDefaults.standard.synchronize()
                                   /* let alert = UIAlertController(title: "Register", message: resp["message"].stringValue, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { act in
                                        self.dismiss(animated: true) {
                                                                   NotificationCenter.default.post(name: NSNotification.Name.init("dissmissIt"), object: nil)
                                                                                                             }
                                                                                                        
                                        
                                    })
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil) */
                                     self.doValidationAnimation()
                                    
                                }else if resp["message"].stringValue == "There is some error sending the validation Email, please try again !" {
                                    ///dont forget
                                    let alert = UIAlertController(title: "Register", message: resp["message"].stringValue, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }else{
                                  /// next Step
                                
                                UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                                UserDefaults.standard.synchronize()
                                self.doValidationAnimation()
//                                let alert = UIAlertController(title: Localization("Register"), message: Localization("EmailSent"), preferredStyle: .alert)
//                                alert.addAction(UIAlertAction(title: Localization("OK"), style: .default, handler: { adc in
//                                    self.dismiss(animated: true) {
//                                        NotificationCenter.default.post(name: NSNotification.Name.init("dissmissIt"), object: nil)
//                                    }
//                                }))
                               // self.present(alert, animated: true, completion: nil)
                               
                            }
                        }else{
                            print("there was an error")
                            let banner = NotificationBanner(customView: self._notificationToast)
                            self._notificationToast.LabelN.text = response.error?.localizedDescription
                            banner.customBannerHeight = 80
                            banner.show(queuePosition: .front, bannerPosition: .top)
                        }
                    }
                }
            }
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = Localization("FillBlanks")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
    }
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    let params : Parameters = [
                        "userId" : a["_id"].stringValue ,
                        "playerId" : stateChanges.to.userId,
                        "type" : "ios"
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.setIosPlayerId , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            //      LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            print(b)
                            UserDefaults.standard.setValue(b.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
                }catch{
                    
                }
            }
            
        }
    }
    
    @IBAction func BackAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name.init("backToLogin"), object: nil)
        self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
}
