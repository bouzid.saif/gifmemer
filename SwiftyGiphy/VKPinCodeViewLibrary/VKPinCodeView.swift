//
//  VKPinCodeView.swift
//  VKPinCodeView
//
//  Created by Vladimir Kokhanevich on 22/02/2019.
//  Copyright © 2019 Vladimir Kokhanevich. All rights reserved.
//

import UIKit

/// Vadation closure. Use it as soon as you need to validate input text which is different from digits.
public typealias PinCodeValidator = (_ code: String) -> Bool


/// Main container with PIN input items.
/// You can use it in storyboards, nib files or right in code.
public final class VKPinCodeView: UIView {
    
    private lazy var _stack = UIStackView(frame: CGRect(x: 8, y: 0, width: self.bounds.size.width - 16, height: self.bounds.size.height))
    
    private lazy var _textField = UITextField(frame: bounds)
    
    private var _code = "" {
        
        didSet { onCodeDidChange?(_code) }
    }
    
    private var _activeIndex: Int {
        
        return _code.count == 0 ? 0 : _code.count - 1
    }
    public var codeFinal : String = ""
    /// Enable or disable the error mode. Default value is false.
    public var isError = false {

        didSet { if oldValue != isError { updateErrorState() } }
    }
    
    /// Number of input items.
    public var length: Int = 4 {
        
        willSet { createLabels() }
    }
    
    /// Spacing between input items.
    public var spacing: CGFloat = 10 {
        
        willSet { if newValue != spacing { _stack.spacing = newValue } }
    }

    /// Setup the keaboard type. Default value is numberPad.
    public var keyBoardType = UIKeyboardType.numberPad {
        
        willSet { _textField.keyboardType = newValue }
    }
    
    /// Enable or disable selection animation for active input item. Default value is true.
    public var animateSelectedInputItem = false
    
    /// Enable or disable shake animation on error. Default value is true.
    public var shakeOnError = true
    
    /// Setup your preferred error reset type. Default value is none.
    public var resetAfterError = ResetType.none
    
    /// Fires when PIN is completely entered. Provides actuall code and completion closure to set error state.
    public var onComplete: ((_ code: String, _ pinView: VKPinCodeView) -> Void)?
    
    /// Fires after an each char has been entered.
    public var onCodeDidChange: ((_ code: String) -> Void)?
    
    /// Fires after begin editing.
    public var onBeginEditing: (() -> Void)?
    
    /// Vadation closure. Use it as soon as you need to validate a text input which is different from a digits.
    /// You don't need this by default.
    public var validator: PinCodeValidator?

    /// Fires every time when a label is ready to set a style
    public var onSettingStyle: (() -> EntryViewStyle)? {

        didSet { createLabels() }
    }
    
    
    // MARK: - Initializers

    public convenience init() {

        self.init(frame: CGRect.zero)
    }

    override public init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    
    // MARK: Life cycle
    
    override public func awakeFromNib() {
        
        super.awakeFromNib()
        setup()
    }
    
    
    // MARK: Overrides

    @discardableResult
    override public func becomeFirstResponder() -> Bool {
        
        onBecomeActive()
        return super.becomeFirstResponder()
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        onBecomeActive()
    }
    
    
    // MARK: Public methods

    /// Use this method to reset the code
    public func resetCode() {
        _code = ""
        _textField.text = nil
        _stack.arrangedSubviews.forEach({ ($0 as! VKLabel).text = nil })
        isError = false
    }
    
    // MARK: Private methods
    
    private func setup() {
        
        setupTextField()
        setupStackView()
        createLabels()
        self.backgroundColor = .white
    }
    
    private func setupStackView() {
        
        //_stack.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        _stack.alignment = .fill
        _stack.axis = .horizontal
        _stack.distribution = .fillEqually
       
        _stack.translatesAutoresizingMaskIntoConstraints = false
        _stack.spacing = spacing
        addSubview(_stack)
       
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
        _stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
         _stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
         _stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
         _stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
    private func setupTextField() {
        
        _textField.keyboardType = keyBoardType
        _textField.isHidden = true
        _textField.delegate = self
        _textField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        _textField.addTarget(self, action: #selector(self.onTextChanged(_:)), for: .editingChanged)
        
        if #available(iOS 12.0, *) { _textField.textContentType = .oneTimeCode }
        
        addSubview(_textField)
    }
    
    @objc private func onTextChanged(_ sender: UITextField) {
        
        let text = sender.text!
        
        if _code.count > text.count {
            
            deleteChar(text)
            var index = _code.count - 1
            if index < 0 { index = 0 }
            highlightActiveLabel(index, withWhite: false)
        }
        else {
            
            appendChar(text)
            let index = _code.count - 1
            highlightActiveLabel(index, withWhite: true)
        }
        
        if _code.count == length {

            _textField.resignFirstResponder()
            self.codeFinal = self._code
            //onComplete?(_code, self)
        }
    }
    
    private func deleteChar(_ text: String) {
        
        let index = text.count
        let previous = _stack.arrangedSubviews[index] as! UILabel
        previous.text = ""
        _code = text
    }
    
    private func appendChar(_ text: String) {
        
        if text.isEmpty { return }
        
        let activeLabel = text.count - 1
        let label = _stack.arrangedSubviews[activeLabel] as! UILabel
        let index = text.index(text.startIndex, offsetBy: activeLabel)
        label.text = String(text[index])
        _code += label.text!
    }
    
    private func highlightActiveLabel(_ activeIndex: Int,withWhite: Bool) {
        
        for i in 0 ..< _stack.arrangedSubviews.count {
            
            let label = _stack.arrangedSubviews[i] as! VKLabel
            label.alreadySelected = withWhite
            label.isSelected = i == activeIndex
        }
    }
    
    private func turnOffSelectedLabel() {
        
        let label = _stack.arrangedSubviews[_activeIndex] as! VKLabel
        label.isSelected = false
    }
    
    private func createLabels() {
        
        _stack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        var i = 1
        for _ in 1 ... length {
            let lbl = VKLabel(onSettingStyle?())
            _stack.addArrangedSubview(lbl)
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.bottomAnchor.constraint(equalTo: _stack.bottomAnchor, constant: 8).isActive = true
            if i == length {
                
                
            }
            if i == 1 {
                // lbl.leadingAnchor.constraint(equalTo: _stack.leadingAnchor, constant: 8).isActive = true
            }
            i = i + 1
            
        }
    }
    
    public func updateErrorState() {
        
        if isError {
            
            turnOffSelectedLabel()
            if shakeOnError { shakeAnimation() }
        }
        
        _stack.arrangedSubviews.forEach({ ($0 as! VKLabel).isError = isError })
    }
    
    private func shakeAnimation() {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: .linear)
        animation.duration = 0.5
        animation.values = [-15.0, 15.0, -15.0, 15.0, -12.0, 12.0, -10.0, 10.0, 0.0]
        animation.delegate = self
        layer.add(animation, forKey: "shake")
    }
    
    private func onBecomeActive() {
        
        _textField.becomeFirstResponder()
        highlightActiveLabel(_activeIndex, withWhite: false)
    }
}


extension VKPinCodeView: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {

        onBeginEditing?()
        handleErrorStateOnBeginEditing()
    }
    
    public func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        if string.isEmpty { return true }
        return (validator?(string) ?? true) && _code.count < length
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        if isError { return }
        turnOffSelectedLabel()
    }

    private func handleErrorStateOnBeginEditing() {

        if isError, case ResetType.onUserInteraction = resetAfterError {

            return resetCode()
        }

        isError = false
    }
}

extension VKPinCodeView: CAAnimationDelegate {
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {

        if !flag { return }

        switch resetAfterError {

            case let .afterError(delay):
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) { self.resetCode() }
            default:
                break
        }
    }
}
