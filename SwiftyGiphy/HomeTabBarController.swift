//
//  HomeTabBarController.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-27.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import SwiftyJSON
class HomeTabBarController {
    
    //Initialize and keeping reference of HHTabBarView.
    let hhTabBarView = HHTabBarView.shared
    static let shared = HomeTabBarController()
    //Keeping reference of iOS default UITabBarController.
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    func setupReferenceUITabBarController() {
        
        //Creating a storyboard reference
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController1: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "GfyCatTrendingController"))
        navigationController1.isNavigationBarHidden = true
        //Creating navigation controller for navigation inside the second tab.
        let navigationController2: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "GfyCatCategoryController"))
        navigationController2.isNavigationBarHidden = true

         let navigationController3: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FavoriteController"))
        navigationController3.isNavigationBarHidden = true
        
        let navigationController5: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "NotificationController"))
        navigationController5.isNavigationBarHidden = true
        let navigationController4: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "TemporaryProfile"))
        navigationController4.isNavigationBarHidden = true

        //Third, Fourth and Fifth will be created runtime.
      
        
        //Update referenced TabbarController with your viewcontrollers
        referenceUITabBarController.setViewControllers([navigationController1, navigationController3, navigationController2, navigationController5, navigationController4], animated: false)
    }
    func setupHHTabBarView() {
        
        //Default & Selected Background Color
        let defaultTabColor = UIColor.black
        
        let tabFont = UIFont.init(name: "Helvetica-Light", size: 12.0)
        //let spacing: CGFloat = 3.0
        
        //Create Custom Tabs
        //Note: As tabs are subclassed of UIButton so you can modify it as much as possible.
        
        let titles = ["", "", "", "", ""]
        let icons = [UIImage(named: "trending")!,UIImage(named: "heart")!, UIImage(named: "home")!,UIImage(named: "notification")! , UIImage(named: "profile")!]
        var tabs = [HHTabButton]()
        for index in 0...4 {
            let tab = HHTabButton(withTitle: titles[index], tabImage: icons[index], index: index)
            tab.titleLabel?.font = tabFont
            tab.viewIndicator = UIView()
            tab.viewIndicator.tag = 0
            tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
            tab.imageToTitleSpacing = 0
            tab.imageVerticalAlignment = .center
            tab.imageHorizontalAlignment = .center
            tabs.append(tab)
        }
        
        //Set HHTabBarView position.
        hhTabBarView.tabBarViewPosition = .bottom
        
        //Set this value according to your UI requirements.
        hhTabBarView.tabBarViewTopPositionValue = 64

        //Set Default Index for HHTabBarView.
        hhTabBarView.tabBarTabs = tabs
        
        // To modify badge label.
        // Note: You should only modify badgeLabel after assigning tabs array.
        // Example:
        //t1.badgeLabel?.backgroundColor = .white
        //t1.badgeLabel?.textColor = selectedTabColor
        //t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        //Handle Tab Change Event
        hhTabBarView.defaultIndex = 2
        
        //Show Animation on Switching Tabs
        hhTabBarView.tabChangeAnimationType = .none
        
        //Handle Tab Changes
        hhTabBarView.onTabTapped = { (tabIndex, isSameTab, controller,lastTab) in
            if isSameTab {
                if let navcon = controller as? UINavigationController {
                    navcon.popToRootViewController(animated: true)
                } else if let vc = controller as? UIViewController {
                    vc.navigationController?.popToRootViewController(animated: true)
                }
            }else{
                if tabIndex == 1 || tabIndex == 3 || tabIndex == 4 {
                    guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                        
                    return
                    }
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                            let a = try JSON(data: dataFromString!)
                         if a["firstConnection"].boolValue {
                            
                            self.hhTabBarView.selectTabAtIndex(withIndex: lastTab)
                            NotificationCenter.default.post(name: NSNotification.Name.init("UserActivation"), object: nil)
                            //let alert = UIAlertController(title: "GifMemer", message: "please validate your account to use the app", preferredStyle: .alert)
                            //let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            //alert.addAction(action)
                            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
                           // appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                        }catch {
                            print("HomeTabBarController : ",error.localizedDescription)
                    }
                    
                    
                }
            }
            
            print("Selected Tab Index:\(tabIndex)")
        }
    }
    func getTabBar() -> UIViewController {
        setupReferenceUITabBarController()
        setupHHTabBarView()
        return  referenceUITabBarController
    }
}
