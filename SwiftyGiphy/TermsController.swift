//
//  TermsAndSerivceController.swift
//  YLYL
//
//  Created by macbook on 1/11/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
class TermsController: UIViewController {
    @IBOutlet weak var titleView : UILabel!
    @IBOutlet weak var textViewContent: UITextView!
    @IBOutlet weak var declinBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.text = "GIFMEMER"
        guard let filePath = Bundle.main.path(forResource: "TermsGifmemer", ofType: "rtf")else {
            return
        }
let attrStr = try! NSAttributedString(url: NSURL(fileURLWithPath: filePath) as URL, options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.rtf], documentAttributes: nil)
        textViewContent.attributedText = attrStr

        configureConnectButton(button: declinBTN)
    }
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
        button.setTitle(Localization("close"), for: .normal)
        button.layer.cornerRadius = 9.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.go_back(_:)), for: .touchUpInside)
    }
    @objc func go_back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
