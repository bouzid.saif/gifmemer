//
//  ServerUpdateDelegate.swift
//  YLYL
//
//  Created by macbook on 2019-05-20.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class ServerUpdateDelegate: appleDetection{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPoPup(notification:)), name: NSNotification.Name.init("closeAppUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPoPup(notification:)), name: NSNotification.Name.init("closeUpdate"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.dissmissIfPossible(_:)), name: NSNotification.Name.init("DissmissAll"), object: nil)
        
    }
    @objc func dissmissIfPossible(_ notification: Notification) {
        if let navigationController = self.navigationController, navigationController.isBeingPresented {
            // being presented
            print("Being presented")
            self.dismiss(animated: false, completion: nil)
        }else{
            // being pushed
            print("Being pushed")
        }
    }
       var couldWork = 1
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    @objc func showPoPup(notification:NSNotification) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.restartApplication()
    }
}
