//
//  SwiftyGiphyViewController.swift
//  SwiftyGiphy
//
//  Created by Brendan Lee on 3/9/17.
//  Copyright © 2017 52inc. All rights reserved.
//

import UIKit
import AVFoundation
import NSTimer_Blocks
import SwiftyGiphy
import SDWebImage
import SwiftyJSON
import Alamofire
public protocol GiphyViewControllerDelegate: class {

    func giphyControllerDidSelectGif(collection: UICollectionView, controller: GiphyViewController, item: GiphyItem)
    func giphyControllerDidCancel(controller: GiphyViewController)
}

 let kSwiftyGiphyCollectionViewCell = "SwiftyGiphyCollectionViewCell"


public class GiphyViewController: UIViewController,GiphyViewControllerDelegate {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var topBar : UIView!
    @IBOutlet weak var profilePicture : ExtensionProfile!
    @IBOutlet weak var profileNameLabel : UILabel!
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
        let shareLink = ["http://zonzay.com/download"]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    @IBOutlet weak var poweredGifContainer: UIView!
    public func giphyControllerDidSelectGif(collection: UICollectionView, controller: GiphyViewController, item: GiphyItem) {
        
        //item.imageSetClosestTo(width: 720, animated: true)?.mp4URL
        collection.isUserInteractionEnabled = false
        let url = item.originalImage?.mp4URL
        //print(url?.absoluteString)
        guard let tempTrunc = url?.absoluteString.components(separatedBy: "?")else {
            collection.isUserInteractionEnabled = true

            return
        }
        let tempTruncTwo = tempTrunc[0].components(separatedBy: "/")
        guard let tempName = tempTruncTwo.last else {
            collection.isUserInteractionEnabled = true

            return
        }
        let keyName = tempTruncTwo[tempTruncTwo.count - 2]
        let finalName = "https://i.giphy.com/media/" + keyName + "/" + tempName
       // NotificationCenter.default.post(name: NSNotification.Name.init("sendVideo"), object: finalName)
        traitGif(collection: collection,gifId: item.identifier, gifUrl: finalName, nameGif: item.caption ?? "Unknown gif")
       print(finalName)
    }
    
    func traitGif(collection:UICollectionView, gifId:String, gifUrl: String,nameGif : String){
           
           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           do {
               let a = try JSON(data: dataFromString!)
               
               let settings : Parameters = [
                   "userId" : a["_id"].stringValue,
                   "gifId" : gifId,
                   "gifUrl" : gifUrl,
                   "name" : nameGif
               ]
               let headers : HTTPHeaders = [
                   "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
               ]
               print(settings)
               print(headers)
               Alamofire.request(ScriptBase.sharedInstance.addGif, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                   let data = JSON(response.data ?? Data())
                if data["status"].boolValue {
                    collection.isUserInteractionEnabled = true

                    let alert = UIAlertController(title: "Processing GIF", message: "We are processing your selected gif, we will notify you as soon as it's finished", preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    collection.isUserInteractionEnabled = true

                    let alert = UIAlertController(title: "Processing GIF", message: data["message"].stringValue, preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                }
                
                  
               }
               
           }catch {
               
           }
       }
       
    public func giphyControllerDidCancel(controller: GiphyViewController) {
        print("Did cancel")
    }
    

     let searchController: UISearchController = UISearchController(searchResultsController: nil)
     @IBOutlet weak var searchContainerView: UIView!

     let collectionView: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: SwiftyGiphyGridLayout())

     let loadingIndicator = UIActivityIndicatorView(style: .whiteLarge)

     let errorLabel: UILabel = UILabel()

     var latestTrendingResponse: GiphyMultipleGIFResponse?
     var latestSearchResponse: GiphyMultipleGIFResponse?

     var combinedTrendingGifs: [GiphyItem] = [GiphyItem]()
     var combinedSearchGifs: [GiphyItem] = [GiphyItem]()

     var currentGifs: [GiphyItem]? {
        didSet {
            collectionView.reloadData()
        }
    }

     var currentTrendingPageOffset: Int = 0
     var currentSearchPageOffset: Int = 0

     var searchCounter: Int = 0

     var isTrendingPageLoadInProgress: Bool = false
     var isSearchPageLoadInProgress: Bool = false

     var keyboardAdjustConstraint: NSLayoutConstraint!

     var searchCoalesceTimer: Timer? {
        willSet {
            if searchCoalesceTimer?.isValid == true
            {
                searchCoalesceTimer?.invalidate()
            }
        }
    }

    /// The maximum content rating allowed for the shown gifs
    public var contentRating: SwiftyGiphyAPIContentRating = .pg13

    /// The maximum allowed size for gifs shown in the feed
    public var maxSizeInBytes: Int = 20480000 // 2MB size cap by default. We're on mobile, after all.

    /// Allow paging the API results. Enabled by default, but you can disable it if you use a custom base URL that doesn't support it.
    public var allowResultPaging: Bool = true

    /// The collection view layout that governs the waterfall layout of the gifs. There are a few parameters you can modify, but we recommend the defaults.
    public var collectionViewLayout: SwiftyGiphyGridLayout? {
        get {
            return collectionView.collectionViewLayout as? SwiftyGiphyGridLayout
        }
    }

    /// The object to receive callbacks for when the user cancels or selects a gif. It is the delegate's responsibility to dismiss the SwiftyGiphyViewController.
    public weak var delegate: GiphyViewControllerDelegate?

    public override func loadView() {
        super.loadView()
        var powerByG = UIImage(named: "GiphyLogoEmblem", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil)
        self.title = NSLocalizedString("Giphy", comment: "Giphy")
        (self.poweredGifContainer.viewWithTag(1) as! UIImageView).image = powerByG
        self.poweredGifContainer.backgroundColor = UIColor.groupTableViewBackground
        //self.navigationItem.titleView = UIImageView(image: UIImage(named: "GiphyLogoEmblem", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil))
        if #available(iOS 13, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                powerByG = UIImage(named: "GiphyLogoEmblemLight", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil)
               (self.poweredGifContainer.viewWithTag(1) as! UIImageView).image = powerByG
                
                //self.navigationItem.titleView = UIImageView(image: UIImage(named: "GiphyLogoEmblemLight", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil))
            }
        }
        searchController.searchBar.placeholder = NSLocalizedString("Search GIFs", comment: "The placeholder string for the Giphy search field")
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.delegate = self

        searchContainerView.backgroundColor = UIColor.clear
        searchContainerView.translatesAutoresizingMaskIntoConstraints = false
        searchContainerView.addSubview(searchController.searchBar)

        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .interactive

        collectionView.translatesAutoresizingMaskIntoConstraints = false

        loadingIndicator.color = UIColor.lightGray
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        delegate = self
        errorLabel.textAlignment = .center
        errorLabel.textColor = UIColor.lightGray
        errorLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .medium)
        errorLabel.numberOfLines = 0
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.isHidden = true

        self.container.addSubview(collectionView)
        self.container.addSubview(loadingIndicator)
        self.container.addSubview(errorLabel)
        //self.container.addSubview(searchContainerView)
        
        keyboardAdjustConstraint = collectionView.bottomAnchor.constraint(equalTo: self.container.bottomAnchor)
/*searchContainerView.leftAnchor.constraint(equalTo: self.container.leftAnchor),
    searchContainerView.topAnchor.constraint(equalTo: self.topBar.bottomAnchor),
    searchContainerView.rightAnchor.constraint(equalTo: self.container.rightAnchor),
searchContainerView.heightAnchor.constraint(equalToConstant: 44.0),*/
        NSLayoutConstraint.activate([
                
                collectionView.leftAnchor.constraint(equalTo: self.container.leftAnchor),
                collectionView.topAnchor.constraint(equalTo: self.container.topAnchor),
                collectionView.rightAnchor.constraint(equalTo: self.container.rightAnchor),
                keyboardAdjustConstraint
            ])

        NSLayoutConstraint.activate([
                loadingIndicator.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor, constant: 0.0),
                loadingIndicator.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor, constant: 32.0)
            ])

        NSLayoutConstraint.activate([
                errorLabel.leftAnchor.constraint(equalTo: self.container.leftAnchor, constant: 30.0),
                errorLabel.rightAnchor.constraint(equalTo: self.container.rightAnchor, constant: -30.0),
                errorLabel.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor, constant: 32.0)
            ])

        collectionView.register(GiphyCollectionViewCell.self, forCellWithReuseIdentifier: kSwiftyGiphyCollectionViewCell)

        self.container.backgroundColor = UIColor.groupTableViewBackground
        self.searchContainerView.layoutIfNeeded()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        profilePicture.layer.borderWidth = 1.0
        profilePicture.layer.borderColor = UIColor.white.cgColor
        profilePicture.isUserInteractionEnabled = true
                   let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goToProfile(_:)))
                   gesture.numberOfTapsRequired = 1
                   profilePicture.addGestureRecognizer(gesture)
        NotificationCenter.default.post(name: NSNotification.Name.init("ConnectServer"), object: nil)
        // Do any additional setup after loading the view.
        if let collectionViewLayout = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout
        {
            collectionViewLayout.delegate = self
        }

        NotificationCenter.default.addObserver(self, selector: #selector(updateBottomLayoutConstraintWithNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        fetchNextTrendingDataPage()
    }
    @objc func goToProfile(_ sender: UITapGestureRecognizer) {
        print("GoToProfile")
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let navigation = storyboard.instantiateViewController(withIdentifier: "TemporaryProfile") as! TemporaryProfile
        self.navigationController?.pushViewController(navigation, animated: true)
       //fatalError("it's seems to works")
     ///TODO: fix this
    //appDelegate.navigation.present(navigation, animated: true, completion: nil)
        //appDelegate.navigation.pushViewController(storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable, animated: true)
        
        
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            if a["userImageURL"].exists() {
                
            self.profilePicture.setImage(with: constructURL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            self.profileNameLabel.text = a["userName"].stringValue
            }
            
            
        }catch{
            
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

       
    }

    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        if #available(iOS 11, *)
        {
            collectionView.contentInset = UIEdgeInsets.init(top: 44.0, left: 0.0, bottom: 10.0, right: 0.0)
            collectionView.scrollIndicatorInsets = UIEdgeInsets.init(top: 44.0, left: 0.0, bottom: 10.0, right: 0.0)
        }
        else
        {
            collectionView.contentInset = UIEdgeInsets.init(top: self.topLayoutGuide.length + 44.0, left: 0.0, bottom: 10.0, right: 0.0)
            collectionView.scrollIndicatorInsets = UIEdgeInsets.init(top: self.topLayoutGuide.length + 44.0, left: 0.0, bottom: 10.0, right: 0.0)
        }
        self.container.bringSubviewToFront(self.searchContainerView)
    }

    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        searchController.searchBar.frame = searchContainerView.bounds
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        if #available(iOS 13.0, *) {
            let hasUserInterfaceStyleChanged = previousTraitCollection?.hasDifferentColorAppearance(comparedTo: traitCollection) ?? false
            if hasUserInterfaceStyleChanged {
                if traitCollection.userInterfaceStyle == .dark {
                    self.navigationItem.titleView = UIImageView(image: UIImage(named: "GiphyLogoEmblemLight", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil))
                } else {
                    self.navigationItem.titleView = UIImageView(image: UIImage(named: "GiphyLogoEmblem", in: Bundle(for: SwiftyGiphyViewController.self), compatibleWith: nil))
                }
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        
        //SDImageCache.shared().clearMemory()
        //SDImageCache.shared().clearDisk(onCompletion: nil)
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk(onCompletion: nil)
    }
    @objc  func dismissPicker()
    {
        searchController.isActive = false
        delegate?.giphyControllerDidCancel(controller: self)
    }

     func fetchNextTrendingDataPage()
    {
        guard !isTrendingPageLoadInProgress else {
            return
        }

        if currentGifs?.count ?? 0 == 0
        {
            loadingIndicator.startAnimating()
            errorLabel.isHidden = true
        }

        isTrendingPageLoadInProgress = true

        let maxBytes = maxSizeInBytes
        let width = max((collectionView.collectionViewLayout as? SwiftyGiphyGridLayout)?.columnWidth ?? 0.0, 0.0)

        SwiftyGiphyAPI.shared.getTrending(limit: 100, rating: contentRating, offset: currentTrendingPageOffset) { [weak self] (error, response) in

            self?.isTrendingPageLoadInProgress = false
            self?.loadingIndicator.stopAnimating()
            self?.errorLabel.isHidden = true

            guard error == nil else {

                if self?.currentGifs?.count ?? 0 == 0
                {
                    self?.errorLabel.text = error?.localizedDescription
                    self?.errorLabel.isHidden = false
                }

                print("Giphy error: \(String(describing: error?.localizedDescription))")
                return
            }

            self?.latestTrendingResponse = response
            self?.combinedTrendingGifs.append(contentsOf: response!.gifsSmallerThan(sizeInBytes: maxBytes, forWidth: width))
            self?.currentTrendingPageOffset = (response!.pagination?.offset ?? (self?.currentTrendingPageOffset ?? 0)) + (response!.pagination?.count ?? 0)

            self?.currentGifs = self?.combinedTrendingGifs

            self?.collectionView.reloadData()
        }
    }

     func fetchNextSearchPage()
    {
        guard !isSearchPageLoadInProgress else {
            return
        }

        guard let searchText = searchController.searchBar.text, searchText.count > 0 else {

            self.searchCounter += 1
            self.currentGifs = combinedTrendingGifs
            return
        }

        searchCoalesceTimer = Timer.scheduledTimer(withTimeInterval: 0.5, block: { [unowned self] () -> Void in

            self.isSearchPageLoadInProgress = true

            if self.currentGifs?.count ?? 0 == 0
            {
                self.loadingIndicator.startAnimating()
                self.errorLabel.isHidden = true
            }

            self.searchCounter += 1

            let currentCounter = self.searchCounter

            let maxBytes = self.maxSizeInBytes
            let width = max((self.collectionView.collectionViewLayout as? SwiftyGiphyGridLayout)?.columnWidth ?? 0.0, 0.0)

            SwiftyGiphyAPI.shared.getSearch(searchTerm: searchText, limit: 100, rating: self.contentRating, offset: self.currentSearchPageOffset) { [weak self] (error, response) in

                self?.isSearchPageLoadInProgress = false

                guard currentCounter == self?.searchCounter else {

                    return
                }

                self?.loadingIndicator.stopAnimating()
                self?.errorLabel.isHidden = true

                guard error == nil else {

                    if self?.currentGifs?.count ?? 0 == 0
                    {
                        self?.errorLabel.text = error?.localizedDescription
                        self?.errorLabel.isHidden = false
                    }

                    print("Giphy error: \(String(describing: error?.localizedDescription))")
                    return
                }

                self?.latestSearchResponse = response
                self?.combinedSearchGifs.append(contentsOf: response!.gifsSmallerThan(sizeInBytes: maxBytes, forWidth: width))
                self?.currentSearchPageOffset = (response!.pagination?.offset ?? (self?.currentSearchPageOffset ?? 0)) + (response!.pagination?.count ?? 0)

                self?.currentGifs = self?.combinedSearchGifs

                self?.collectionView.reloadData()

                if self?.currentGifs?.count ?? 0 == 0
                {
                    self?.errorLabel.text = NSLocalizedString("No GIFs match this search.", comment: "No GIFs match this search.")
                    self?.errorLabel.isHidden = false
                }
            }
        }, repeats: false) as! Timer?
    }
}

// MARK: - SwiftyGiphyGridLayoutDelegate
extension GiphyViewController: SwiftyGiphyGridLayoutDelegate {

    public func collectionView(collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat
    {
        guard let imageSet = currentGifs?[indexPath.row].imageSetClosestTo(width: withWidth, animated: true) else {
            return 0.0
        }

        return AVMakeRect(aspectRatio: CGSize(width: imageSet.width, height: imageSet.height), insideRect: CGRect(x: 0.0, y: 0.0, width: withWidth, height: CGFloat.greatestFiniteMagnitude)).height
    }
}

// MARK: - UICollectionViewDataSource
extension GiphyViewController: UICollectionViewDataSource {

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return currentGifs?.count ?? 0
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kSwiftyGiphyCollectionViewCell, for: indexPath) as! GiphyCollectionViewCell

        if let collectionViewLayout = collectionView.collectionViewLayout as? SwiftyGiphyGridLayout, let imageSet = currentGifs?[indexPath.row].imageSetClosestTo(width: collectionViewLayout.columnWidth, animated: true)
        {
            cell.configureFor(imageSet: imageSet)
        }

        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension GiphyViewController: UICollectionViewDelegate {

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       //collectionView.deselectItem(at: indexPath, animated: false)
        
        let alert = UIAlertController(title: "Processing GIF/MIMES", message: "Please select one of the following options", preferredStyle: .alert)
        let actionGIF = UIAlertAction(title: "GIF", style: .default) { (alert) in
            let selectedGif = self.currentGifs![indexPath.row]

            self.searchController.isActive = false
            self.delegate?.giphyControllerDidSelectGif(collection :collectionView , controller: self, item: selectedGif)
        }
        let actionMIME = UIAlertAction(title: "MIME", style: .default) { (alert) in
            print("under construction")
        }
        actionMIME.isEnabled = false
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(actionMIME)
        alert.addAction(actionGIF)
        alert.addAction(actionCancel)
        self.present(alert, animated: true, completion: nil)
        
        
    }
}

// MARK: - UISearchControllerDelegate
extension GiphyViewController: UISearchControllerDelegate {

    public func willPresentSearchController(_ searchController: UISearchController) {

        searchCounter += 1
        latestSearchResponse = nil
        currentSearchPageOffset = 0
        combinedSearchGifs = [GiphyItem]()
        currentGifs = [GiphyItem]()

        errorLabel.isHidden = true
        loadingIndicator.stopAnimating()
    }

    public func willDismissSearchController(_ searchController: UISearchController) {

        searchController.searchBar.text = nil

        searchCounter += 1
        latestSearchResponse = nil
        currentSearchPageOffset = 0
        combinedSearchGifs = [GiphyItem]()

        currentGifs = combinedTrendingGifs
        collectionView.setContentOffset(CGPoint(x: 0.0, y: -collectionView.contentInset.top), animated: false)

        errorLabel.isHidden = true
        loadingIndicator.stopAnimating()
    }
}

// MARK: - UISearchResultsUpdating
extension GiphyViewController: UISearchResultsUpdating {

    public func updateSearchResults(for searchController: UISearchController) {

        // Destroy current results
        searchCounter += 1
        latestSearchResponse = nil
        currentSearchPageOffset = 0
        combinedSearchGifs = [GiphyItem]()
        currentGifs = [GiphyItem]()
        fetchNextSearchPage()
    }
}

// MARK: - UIScrollViewDelegate
extension GiphyViewController: UIScrollViewDelegate {

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {

        guard allowResultPaging else {
            return
        }

        if scrollView.contentOffset.y + scrollView.bounds.height + 100 >= scrollView.contentSize.height
        {
            if searchController.isActive
            {
                if !isSearchPageLoadInProgress && latestSearchResponse != nil
                {
                    // Load next search page
                    fetchNextSearchPage()
                }
            }
            else
            {
                if !isTrendingPageLoadInProgress && latestTrendingResponse != nil
                {
                    // Load next trending page
                    fetchNextTrendingDataPage()
                }
            }
        }
    }
}

// MARK: - Keyboard
extension GiphyViewController {

    @objc  func updateBottomLayoutConstraintWithNotification(notification: NSNotification?) {

        let constantAdjustment: CGFloat = 0.0

        guard let bottomLayoutConstraint: NSLayoutConstraint = keyboardAdjustConstraint else {
            return
        }

        guard let userInfo = notification?.userInfo else {

            bottomLayoutConstraint.constant = constantAdjustment
            return
        }

        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        let rawAnimationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        let animationCurve = UIView.AnimationOptions(rawValue: UInt(rawAnimationCurve))

        let newConstantValue: CGFloat = max(self.container.bounds.maxY - convertedKeyboardEndFrame.minY + constantAdjustment, 0.0)

        if abs(bottomLayoutConstraint.constant - newConstantValue) >= 1.0
        {
            UIView.animate(withDuration: animationDuration, delay: 0.0, options: animationCurve, animations: {

                bottomLayoutConstraint.constant = -newConstantValue
                self.container.layoutIfNeeded()

            }, completion: nil)
        }
        else
        {
            bottomLayoutConstraint.constant = -newConstantValue
        }
    }
}
