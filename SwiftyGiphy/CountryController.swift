//
//  CountryController.swift
//  YLYL
//
//  Created by macbook on 1/7/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
protocol PassCountryDelegate {
    func getCountryCode(data: String,country:String,alpha:String)
}

class CountryController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var searchBar: UISearchBar!
    var countrySort : JSON = []
    var isSearching = false
    /*
     * Number of items get from AppDelegate
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(AppDelegate.countryArray.count)
        if isSearching == false {
        return AppDelegate.countryArray.count
        }else{
            return countrySort.count
        }
    }
    /*
     *  Cell configuration
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
       let label =  cell.viewWithTag(1) as! UILabel
        let imageFlag = cell.viewWithTag(2) as! UIImageView
        if language != "en" {
            if isSearching == false {
        label.text = AppDelegate.countryArray[indexPath.row]["translations"]["\(language)"].stringValue +
            " (+" + AppDelegate.countryArray[indexPath.row]["callingCodes"]["0"].stringValue + ")"
            imageFlag.image = UIImage(named: AppDelegate.countryArray[indexPath.row]["alpha2Code"].stringValue)
            }else{
                label.text = countrySort[indexPath.row]["translations"]["\(language)"].stringValue +
                    " (+" + countrySort[indexPath.row]["callingCodes"]["0"].stringValue + ")"
                imageFlag.image = UIImage(named: countrySort[indexPath.row]["alpha2Code"].stringValue)
            }
            
        }else{
            if isSearching == false {
            label.text = AppDelegate.countryArray[indexPath.row]["name"].stringValue +
                " (+" + AppDelegate.countryArray[indexPath.row]["callingCodes"][0].stringValue + ")"
            imageFlag.image = UIImage(named: AppDelegate.countryArray[indexPath.row]["alpha2Code"].stringValue)
        }else{
            label.text = countrySort[indexPath.row]["name"].stringValue +
                " (+" + countrySort[indexPath.row]["callingCodes"][0].stringValue + ")"
            imageFlag.image = UIImage(named: countrySort[indexPath.row]["alpha2Code"].stringValue)
        }
        }
        //print(countries[indexPath.row])
        return cell
    }
    /*
     * protocol used to pass Object to the preview viewController
     */
    var delegate : PassCountryDelegate?
    /*
     * Tableview listing the Countries
     */
    @IBOutlet weak var tableView: UITableView!
    /*
     * view that contain the the tableView
     */
    @IBOutlet weak var dissmisView: UIView!
    /*
     * The actual language of the user phone
     */
    var language : NSString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        language = Bundle.main.preferredLocalizations.first! as NSString
        print(language)
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goBack))
        tapGesture.numberOfTapsRequired = 1
        self.dissmisView.isUserInteractionEnabled = true
        self.dissmisView.addGestureRecognizer(tapGesture)
        
    }
    /*
     * When selecting a row, we pass that objet ot the preview view
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            if self.isSearching == false {
            self.delegate?.getCountryCode(data:  AppDelegate.countryArray[indexPath.row]["callingCodes"][0].stringValue, country:  AppDelegate.countryArray[indexPath.row]["name"].stringValue, alpha: AppDelegate.countryArray[indexPath.row]["alpha2Code"].stringValue)
            }else{
                self.delegate?.getCountryCode(data:  self.countrySort[indexPath.row]["callingCodes"][0].stringValue, country:  self.countrySort[indexPath.row]["name"].stringValue, alpha: self.countrySort[indexPath.row]["alpha2Code"].stringValue)
            }
        }
    }
    /*
     * user canceled the action
     */
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
        
    }
}
extension CountryController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" {
            self.isSearching = true
            self.countrySort = []
            for i in 0...(AppDelegate.countryArray.count - 1) {
                if AppDelegate.countryArray[i]["name"].stringValue.starts(with: searchText) {
                     self.countrySort.appendIfArray(json: AppDelegate.countryArray[i])
                }
            }
            
            self.tableView.reloadData()
        }else{
            self.isSearching = false
        }
    }
}
extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr);
        }
    }
    
    mutating func appendIfDictionary(key:String,json:JSON){
        if var dict = self.dictionary{
            dict[key] = json;
            self = JSON(dict);
        }
    }
}
