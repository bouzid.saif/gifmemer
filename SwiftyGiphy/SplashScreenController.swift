//
//  SplashScreenController.swift
//  YLYL
//
//  Created by macbook on 1/4/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AVKit
import OneSignal
import Alamofire
import Hero
class SplashScreenController: UIViewController,OSSubscriptionObserver {
    
    
    var viewC: UIView?
    
    let animationDurationBase: Double = 2.5
    let logoVigetViewTag = 102
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var ringContainer: UIView!
    // MARK: - Animate

       func animateAfterLaunch() {
           hideLogo()
        playerDidFinishPlayingt(note: Notification(name: Notification.Name.init("Saif")))
       }

     

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
    }
       func hideLogo() {
        let logoViget = self.logoContainer.viewWithTag(logoVigetViewTag)!

           UIView.animate(
               withDuration: animationDurationBase / 3,
               delay: self.animationDurationBase / 6,
               options: .curveEaseOut,
               animations: {
               //    logoIsIt.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
               },
               completion: { _ in
                   UIView.animate(
                       withDuration: self.animationDurationBase / 6,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
                        //   logoIsIt.alpha = 0
                        //   logoIsIt.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                       }
                   )
               }
           )

           UIView.animate(
               withDuration: animationDurationBase / 4,
               delay: animationDurationBase / 3,
               options: .curveEaseOut,
               animations: {
                   logoViget.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
               },
               completion: { _ in
                   UIView.animate(
                       withDuration: self.animationDurationBase / 6,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
                           logoViget.alpha = 0
                           logoViget.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                       }
                   )
               }
           )

         
       }

    func hideRingSegments(route:String) {
       /*let distanceToMove = self.view.frame.size.height * 1.75

           for number in 1...12 {
               let ringSegment = self.ringContainer.viewWithTag(number)!

               // Get the degrees we want to move to
               let degrees = 360 - (number * 30) + 15

               // Convert to float
               let angle = CGFloat(degrees)

               // Convert to radians
               let radians = angle * (CGFloat.pi / 180)

               // Calculate the final X value from this angle and the total distance.
               // See https://academo.org/demos/rotation-about-point/ for more.
               let translationX = (cos(radians) * distanceToMove)
               let translationY = (sin(radians) * distanceToMove) * -1

               UIView.animate(
                   withDuration: animationDurationBase * 1.75,
                   delay: animationDurationBase / 1.5,
                   options: .curveLinear,
                   animations: {
                       var transform = CGAffineTransform.identity
                       transform = transform.translatedBy(x: translationX, y: translationY)

                       // This rotation accounts for the curve in the segment images.
                       // I just eyeballed it; different curves will require tweaks.
                       transform = transform.rotated(by: -1.95)

                       ringSegment.transform = transform
                   }
               )

               // When segments are very curved, sometimes pieces of them reappear on-screen
               // before the animation finishes. This timer stops the animation early and removes
               // the entire view.
               DispatchQueue.main.asyncAfter(deadline: .now() + animationDurationBase * 1.25) {
                if route == "TABBAR" {
                    self.navigationController?.pushViewController(HomeTabBarController.shared.getTabBar(), animated: true)
                }else if route == "PHOTOSPLASH" {
                    self.performSegue(withIdentifier: "go_to_photoFromSplash", sender: self)
                }else if route == "MAIN" {
                      self.performSegue(withIdentifier: "go_to_main", sender: self)
                }else if route == "SERVERUPDATE" {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServerUpdateController") as! ServerUpdateController
                                                   vc.hero.modalAnimationType = .zoom
                                                   vc.modalPresentationStyle = .overCurrentContext
                                                   self.present(vc, animated: true, completion: nil)
                }else if route == "APPUPDATE" {
                    //Update Available
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppUpdateController") as! AppUpdateController
                    vc.hero.modalAnimationType = .zoom
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }else if route == "NOINTERNET" {
                    let alert = UIAlertController(title: "Zonzay", message: Localization("CANTSERVER"), preferredStyle: .alert)
                    let action = UIAlertAction(title: Localization("Retry"), style: .default) { (alertt) in
                        let appDeleg = UIApplication.shared.delegate as! AppDelegate
                        appDeleg.restartApplication()
                    }
                    
                    alert.addAction(action)
                    
                    self.present(alert, animated: true, completion: nil)
                }
                  
               }

               /*
                   // Uncomment this code (and comment the above code)
                   // to "freeze" the ring animation for easier visual debugging
                   DispatchQueue.main.asyncAfter(deadline: .now() + 1.05) {
                       let pausedTime: CFTimeInterval = ringSegment.layer.convertTime(CACurrentMediaTime(), from: nil)
                       ringSegment.layer.timeOffset = pausedTime
                       ringSegment.layer.speed = 0.0
                   }
               */

           } */
       }
    
    
    @objc func LogoutCalled(_ notifcication : Notification) {
        print("Logout Received")
        if self.determineEligibality() {
                           print(UserDefaults.standard.object(forKey: "UserZonzay"))
                           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                               // SocketIOManager.sharedInstance.detectDisconnect()
                           }
                           
                           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                           do {
                               let a = try JSON(data: dataFromString!)
                               
                               //let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                              // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                               
                               
                               self.ConnectTopush()
                               self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                               
                           }catch{
                               
                               print("UserDefaultsPlayer: ",error.localizedDescription)
                           }
                           
                       }else{
                          // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                          // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                           
                           self.performSegue(withIdentifier: "go_to_main", sender: self)
                       }
    }
    
    /*
     * Init
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.LogoutCalled(_:)), name: NSNotification.Name.init("LogoutCalled"), object: nil)
       
       /* if let currentLanguage = Locale.currentLanguage {
            print(currentLanguage.rawValue)
            if currentLanguage == .en {
               _ = SetLanguage("English_en")
            }else if currentLanguage == .fr {
                _ = SetLanguage("French_fr")
                
            }else if currentLanguage == .es {
                _ = SetLanguage("Spanish_es")
            }else if currentLanguage == .pt{
                 _ = SetLanguage("Portugese_pg")
            }else if currentLanguage == .it {
                _ = SetLanguage("English_en")
            }else{
                _ = SetLanguage("Italien_it")
            }
            // Your code here.
        } */
       // Config.doneButtonTitle = Localization("Done")
        print("SplashScreen")
            ///TODO: Notifications
            /*let mode = (UIApplication.shared.delegate as! AppDelegate).NormalMode
            let index = (UIApplication.shared.delegate as! AppDelegate).indexToShow */
            //if mode {
            if true {
                self.animateAfterLaunch()

            }else{
                if self.determineEligibality() {
                    print(UserDefaults.standard.object(forKey: "UserZonzay"))
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        // SocketIOManager.sharedInstance.detectDisconnect()
                    }
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        
                        //let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                       // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                        
                        
                        self.ConnectTopush()
                        self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                        
                    }catch{
                        
                        print("UserDefaultsPlayer: ",error.localizedDescription)
                    }
                    
                }else{
                   // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                   // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                   
                    self.performSegue(withIdentifier: "go_to_main", sender: self)
                }
            }
        
    
        
        
    }
    /*
     * When the animation finish, we go to the next Controller
     */
    var oneTime = false
    @objc func playerDidFinishPlayingt(note: Notification) {
        print("VIdeo Finished")
        
     /*   let pinghelper = PingHelper()
        pinghelper.host = "www.google.com"
        pinghelper.hostForCheck = "apple.com"
        pinghelper.ping { (isPingable) in
            print("ping succefully : ",isPingable)
        } */
        let settings : Parameters = [:]
        let headers : HTTPHeaders = [:]
        print(settings)
        print(headers)
        
        Alamofire.request(ScriptBase.sharedInstance.checkConncetion, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).response { response in
            print("response code : ",response.response?.statusCode )
            if response.response?.statusCode != nil {
                VersionCheck.shared.isUpdateAvailable() { (hasUpdates) in
                    print("is update available: \(hasUpdates)")
                    if self.oneTime == false {
                        self.oneTime = true
                        if hasUpdates == false {
                            if SocketIOManager.sharedInstance.connectedMiddleWar == false {

                                if self.determineEligibality() {
                                    print(UserDefaults.standard.object(forKey: "UserZonzay"))
                                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                                    
                                    
                                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                    do {
                                        let a = try JSON(data: dataFromString!)
                                        
                                        // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                                        
                                        // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                                        
                                        
                                        self.ConnectTopush()
                                       
                                        /*var testTab : [Int] = []
                                                                           for i in 1...59 {
                                                                               testTab.append(i)
                                                                           }
                                                                           SocketIOManager.sharedInstance.testBougiNumbers(tab: testTab) */
                                        if self.determinePhoto() {
                                            self.hideRingSegments(route: "TABBAR")

                                            //self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                                        }else{
                                          
                                            self.hideRingSegments(route: "PHOTOSPLASH")
                                            
                                        }
                                        
                                        
                                    }catch{
                                        
                                        print("UserDefaultsPlayer: ",error.localizedDescription)
                                    }
                                    
                                }else{
                                    
                                     if UserDefaults.standard.object(forKey: "UserZonzay") == nil {
    
                                        self.hideRingSegments(route: "MAIN")
                                     }else{
                                        
                                    }
                                    
                                    
                                    
                                    // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                                    //  navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                                    
                                    
                                   
                                   
                                }
                            }else{
                                // Connected To Update
                                self.hideRingSegments(route: "SERVERUPDATE")
                            }
                        }else{
                            self.hideRingSegments(route: "APPUPDATE")
                            
                        }
                    }
                }
            }else{
                self.hideRingSegments(route: "NOINTERNET")
            }
        }
      
    }
    func determineEligibality() -> Bool{
         if UserDefaults.standard.object(forKey: "UserZonzay") != nil {
            
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a =  try JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.updateSocketId(userId: a["_id"].stringValue)
            print ("UserZonzay",a)
        if (a["userEmail"].exists() == false ) || (a["userBirthday"].exists() == false) || (a["userGender"].exists() == false) || (a["userName"].exists() == false) || (a["alphaCode"].exists() == false) {
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                return false
            }else {
                
                return true
            }
            }catch {
                print("SplachScreen determineEligibality : ",error.localizedDescription)
                return false
            }
         }else{
            return false
        }
    }
    func determinePhoto() -> Bool {
         if UserDefaults.standard.object(forKey: "UserZonzay") != nil {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
            let a =  try JSON(data: dataFromString!)
            if (a["userImageURL"].exists() == false ) {
                return false
            }else{
                return true
            }
            }catch {
                print("SplachScreen determinePhoto : ",error.localizedDescription)
                return false
            }
         }else{
            return false
        }
    }
    /*
     * To play to animation
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ///TODO: Notfification
        //let mode = (UIApplication.shared.delegate as! AppDelegate).NormalMode
        //if mode {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    let params : Parameters = [
                        "userId" : a["_id"].stringValue ,
                        "playerId" : stateChanges.to.userId,
                        "type" : "ios"
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.setIosPlayerId , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            //      LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            print(b)
                            UserDefaults.standard.setValue(b.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
                }catch{
                    print("UserDefaultsError: ",error.localizedDescription)
                }
            }
            
        }
    }
    func connectToGfyCat(){
        let params : Parameters = [
            "grant_type" : "password",
            "client_id" : ScriptBase.client_id,
            "client_secret" : ScriptBase.client_secret,
            "username" : "atacand1920",
            "password" : "Quinn1024"
        ]
        Alamofire.request(URL(string: "https://api.gfycat.com/v1/oauth/token")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (data) in
            if data.response?.statusCode == 200 {
                let tempData = JSON(data.data)
                ScriptBase.client_token = tempData["access_token"].stringValue
                ScriptBase.refresh_token = tempData["refresh_token"].stringValue
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.renewRefreshToken()
            }else{
                self.connectToGfyCat()
            }
        }
    }
}
class VersionCheck {
    
    public static let shared = VersionCheck()
    let isDebuggerAttached: Bool = {
        var debuggerIsAttached = false
        
        var name: [Int32] = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        var info: kinfo_proc = kinfo_proc()
        var info_size = MemoryLayout<kinfo_proc>.size
        
        let success = name.withUnsafeMutableBytes { (nameBytePtr: UnsafeMutableRawBufferPointer) -> Bool in
            guard let nameBytesBlindMemory = nameBytePtr.bindMemory(to: Int32.self).baseAddress else { return false }
            return -1 != sysctl(nameBytesBlindMemory, 4, &info/*UnsafeMutableRawPointer!*/, &info_size/*UnsafeMutablePointer<Int>!*/, nil, 0)
        }
        
        // The original HockeyApp code checks for this; you could just as well remove these lines:
        if !success {
            debuggerIsAttached = false
        }
        
        if !debuggerIsAttached && (info.kp_proc.p_flag & P_TRACED) != 0 {
            debuggerIsAttached = true
        }
        
        return debuggerIsAttached
    }()
    func isUpdateAvailable(callback: @escaping (Bool)->Void) {
        if isDebuggerAttached == true {
           
        let bundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        if detectAppStoreInstallation() == false { Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(bundleId)").responseJSON { response in
            
            if let json = response.result.value as? NSDictionary, let results = json["results"] as? NSArray, let entry = results.firstObject as? NSDictionary, let appStoreVersion = entry["version"] as? String, let installedVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                print("json : ", Float(installedVersion))
                print("json : ", Float(appStoreVersion))
               
                if let appstoreFloat = Float(appStoreVersion), let installedVersionFloat = Float(installedVersion) {
                     print(installedVersionFloat < appstoreFloat)
                    callback(installedVersionFloat < appstoreFloat)
                }else{
                    callback(false)
                }
               
            }else{
                callback(false)
            }
        }
        }else{
            callback(false)
            }
        }else{
            callback(false)
        }
    }
    
    
}
func detectAppStoreInstallation() -> Bool{
    if let provisionPath = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") {
    if (!(FileManager.default.fileExists(atPath: provisionPath))) {
        	return true
    }
    }else{
       return true
    }
    return false
}

