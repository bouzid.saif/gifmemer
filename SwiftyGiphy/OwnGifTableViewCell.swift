//
//  OwnGifTableViewCell.swift
//  SwiftyGiphySample
//
//  Created by macbook on 2019-11-15.
//  Copyright © 2019 52inc. All rights reserved.
//

import Foundation
import Gifu
import SDWebImage
import AVFoundation
public class OwnGifTableViewCell : UICollectionViewCell, ASAutoPlayVideoLayerContainer {
    
    @IBOutlet weak var imageViewGif: UIImageView!
    @IBOutlet weak var watermarket : UIImageView!
    var currentFrame : UIImage? = nil
    var gifIsReady = false
    var gifIsDisplayed = false
    var gifURL : URL!
    var viewSource : UIView = UIView()
    var gif : FLAnimatedImageView!
    var watermarketToShow : UIImageView!
    var isOneTime = true
    var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
          didSet {
              if let videoURL = videoURL {
                  ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
              }
              videoLayer.isHidden = videoURL == nil
          }
      }
   
    override public func awakeFromNib() {
        super.awakeFromNib()
        imageViewGif.layer.cornerRadius = 5
        imageViewGif.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        imageViewGif.clipsToBounds = true
        imageViewGif.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
        imageViewGif.layer.borderWidth = 0.5
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        imageViewGif.layer.addSublayer(videoLayer)
        
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
        //let frame =  CGRect(x: self.imageViewGif.frame.origin.x, y: self.imageViewGif.frame.origin.y, width: 181, height: 122)
       // self.imageViewGif.frame = frame
        videoLayer.frame = CGRect(x: 0, y: 0, width: 181, height: 132)
    }
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(imageViewGif.frame, from: imageViewGif)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    func visibleVideoWidth() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(imageViewGif.frame, from: imageViewGif)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.width
    }
    public override func prepareForReuse() {
         imageViewGif.imageURL = nil
        super.prepareForReuse()
      
        
        //
        
        //
        //self.currentFrame = nil
        
    }
   /* @objc func didHoldPressGif (_ press: UILongPressGestureRecognizer) {
        
        if press.state != UIGestureRecognizer.State.ended {
           //did begin
            if isOneTime {
                self.isOneTime = false
            gif = FLAnimatedImageView(frame: self.frame)
            watermarketToShow = UIImageView(frame: self.frame)
            gif.sd_cacheFLAnimatedImage = false
            gif.sd_setShowActivityIndicatorView(true)
            gif.sd_setIndicatorStyle(.gray)
            watermarketToShow.image = UIImage(named: "watermarketLight")
            gif.gifImage = imageViewGif.gifImage
            viewSource.addSubview(gif)
            viewSource.addSubview(watermarketToShow)
            viewSource.bringSubviewToFront(gif)
                viewSource.bringSubviewToFront(watermarketToShow)
             gif.sd_setImage(with: gifURL) { (image, error, cacheType, url) in
                self.gif.frame = CGRect(x: 0, y: 167, width: self.viewSource.frame.width, height: 200)
                self.watermarketToShow.frame = CGRect(x: 0, y: 167, width: self.viewSource.frame.width, height: 200)

                UIView.animate(withDuration: 0.3, animations: {
                self.gif.layoutIfNeeded()
                self.watermarketToShow.layoutIfNeeded()
            }) { (complete) in
                print("ok")
                self.gif.startAnimatingGIF()
            }
            }
            }
            
        }else{
           // did end
            self.isOneTime = true
            gif.removeFromSuperview()
            self.watermarketToShow.removeFromSuperview()
        }
    } */
    
    func configureGif(gifUrl : URL ) {
        self.gifURL = URL(string: gifUrl.absoluteString + ".mp4")
        self.videoURL = gifUrl.absoluteString + ".mp4"
        self.imageViewGif.sd_setImage(with: URL(string: gifUrl.absoluteString + ".jpg"), completed: nil)
        //self.imageViewGif.image = ASVideoPlayerController.sharedVideoPlayer.getThumbnailImage(forUrl: gifUrl)
       /* if self.currentFrame == nil {
        imageViewGif.sd_cacheFLAnimatedImage = false
        imageViewGif.setShouldResizeFrames(false)
        imageViewGif.contentMode = .scaleAspectFill
        imageViewGif.sd_setShowActivityIndicatorView(true)
        imageViewGif.sd_setIndicatorStyle(.gray)
        
       
           
        imageViewGif.sd_setImage(with: gifUrl) { (image, error, cacheType, url) in
            //print(image?.size)
            if self.currentFrame == nil {
            self.currentFrame = self.imageViewGif.currentFrame
            self.imageViewGif.image = self.currentFrame
            let frame =  CGRect(x: self.imageViewGif.frame.origin.x, y: self.imageViewGif.frame.origin.y, width: 181, height: 122)
                
            self.imageViewGif.frame = frame
            self.imageViewGif.layoutIfNeeded()
            }
            print(self.imageViewGif.frame.size)
        }
           
      
        
        }else{
            imageViewGif.sd_setImage(with: gifUrl) { (image, error, cacheType, url) in
                       //print(image?.size)
                       if self.currentFrame == nil {
                       self.currentFrame = self.imageViewGif.currentFrame
                       self.imageViewGif.image = self.currentFrame
                       let frame =  CGRect(x: self.imageViewGif.frame.origin.x, y: self.imageViewGif.frame.origin.y, width: 181, height: 122)
                           
                       self.imageViewGif.frame = frame
                       self.imageViewGif.layoutIfNeeded()
                       }
                       print(self.imageViewGif.frame.size)
            }
           
            
        } */
        
    }
    func startAnimating(){
      /*  if imageViewGif.isAnimatingGif() == false {
            imageViewGif.startAnimatingGIF()
        } */
    }
    func stopAnimating(){
       /* if imageViewGif.isAnimatingGIF {
            imageViewGif.stopAnimatingGIF()
        } */
    }
    
}
public class OwnGifPayedTableViewCell : UICollectionViewCell, ASAutoPlayVideoLayerContainerOwned {
    
    @IBOutlet weak var imageViewGif: UIImageView!
    @IBOutlet weak var watermarket : UIImageView!
    var playerController: ASVideoPlayerControllerOwned?
       var videoLayer: AVPlayerLayer = AVPlayerLayer()
    
       var videoURL: String? {
           didSet {
               if let videoURL = videoURL {
                    print("VideoURL: ",videoURL)
                   ASVideoPlayerControllerOwned.sharedVideoPlayer.setupVideoFor(url: videoURL)
               }
               videoLayer.isHidden = videoURL == nil
           }
       }
    override public func awakeFromNib() {
           super.awakeFromNib()
           imageViewGif.layer.cornerRadius = 5
           imageViewGif.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
           imageViewGif.clipsToBounds = true
           imageViewGif.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
           imageViewGif.layer.borderWidth = 0.5
           videoLayer.backgroundColor = UIColor.clear.cgColor
           videoLayer.videoGravity = AVLayerVideoGravity.resize
           imageViewGif.layer.addSublayer(videoLayer)
           
       }
    var gifIsReady = false
    var gifIsDisplayed = false
    var defaultFrame : CGRect = CGRect.zero
    var gifUrl : URL!
    
    public override func prepareForReuse() {
        imageViewGif.imageURL = nil
        super.prepareForReuse()
      /* imageViewGif.sd_cancelCurrentAnimationImagesLoad()
       imageViewGif.sd_cancelCurrentImageLoad()
       imageViewGif.sd_setImage(with: nil)
       imageViewGif.animatedImage = nil
       imageViewGif.image = nil */
    
        
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
        videoLayer.frame = CGRect(x: 0, y: 0, width: 181, height: 132)
    }
    func configureGif(gifUrl : URL ) {
       self.gifUrl = URL(string: gifUrl.absoluteString + ".mp4")
        self.videoURL = gifUrl.absoluteString + ".mp4"
        self.imageViewGif.sd_setImage(with: URL(string: gifUrl.absoluteString + ".jpg"), completed: nil)
       /* imageViewGif.sd_cacheFLAnimatedImage = false
        imageViewGif.sd_setShowActivityIndicatorView(true)
        imageViewGif.sd_setIndicatorStyle(.gray)
       
        
        imageViewGif.sd_setImage(with: gifUrl) { (image, error, cacheType, url) in
            //print(image?.size)
            if self.defaultFrame == CGRect.zero {
            let frame =  CGRect(x: self.imageViewGif.frame.origin.x, y: self.imageViewGif.frame.origin.y, width: 181, height: 132)
                
            self.imageViewGif.frame = frame
            self.imageViewGif.layoutIfNeeded()
            }else{
                self.imageViewGif.frame = self.defaultFrame
                self.imageViewGif.layoutIfNeeded()
            }
            print(self.imageViewGif.frame.size)
        }
        if self.defaultFrame != CGRect.zero {
            self.imageViewGif.frame = self.defaultFrame
            self.imageViewGif.layoutIfNeeded()
        } */
       
        
        
    }
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(imageViewGif.frame, from: imageViewGif)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    func visibleVideoWidth() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(imageViewGif.frame, from: imageViewGif)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.width
    }
    func startAnimating(){
        /*if imageViewGif.isAnimatingGif() == false {
            imageViewGif.startAnimatingGIF()
        } */
    }
    func stopAnimating(){
      /*  if imageViewGif.isAnimatingGIF {
            imageViewGif.stopAnimatingGIF()
        } */
    }
    
}
extension UIImageView: GIFAnimatable {
  private struct AssociatedKeys {
    static var AnimatorKey = "gifu.animator.key"
  }

  override open func display(_ layer: CALayer) {
    updateImageIfNeeded()
  }

  public var animator: Animator? {
    get {
      guard let animator = objc_getAssociatedObject(self, &AssociatedKeys.AnimatorKey) as? Animator else {
        let animator = Animator(withDelegate: self)
        self.animator = animator
        return animator
      }

      return animator
    }

    set {
      objc_setAssociatedObject(self, &AssociatedKeys.AnimatorKey, newValue as Animator?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
  }
}
